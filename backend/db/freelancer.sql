-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-06-2018 a las 21:10:28
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `freelancer`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autonomo`
--

CREATE TABLE `autonomo` (
  `id_user` varchar(150) NOT NULL,
  `dni` varchar(145) NOT NULL,
  `name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `date_up` varchar(150) NOT NULL,
  `idioma` varchar(150) NOT NULL,
  `categ_prof` varchar(150) NOT NULL,
  `especialista` varchar(150) NOT NULL,
  `materia` varchar(150) NOT NULL,
  `proyecto` varchar(150) NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` int(9) DEFAULT NULL,
  `community` varchar(150) DEFAULT NULL,
  `province` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `birthday` varchar(150) DEFAULT NULL,
  `price_h` int(5) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `autonomo`
--

INSERT INTO `autonomo` (`id_user`, `dni`, `name`, `last_name`, `date_up`, `idioma`, `categ_prof`, `especialista`, `materia`, `proyecto`, `avatar`, `email`, `phone`, `community`, `province`, `city`, `birthday`, `price_h`, `type`, `lat`, `lng`) VALUES
('facebook|1909274919107188', '01722926L', 'david', 'guetta', '11/01/2017', 'EspaÃ±ol:Ingles:Frances:', 'Developer web', 'PHP', 'Java:Jquery:PHP:Django:Node:Angular:', 'developer webbbbbb', '/freelancer/frontend/assets/media/dropzone/157156mYMKGh - copia.jpg', 'davidguetta@gmail.com', 789456123, 'ANDALUCÃA', 'CEUTA', 'ARMILLA', '12/01/2000', 20, NULL, 37.146, -3.62336),
('pepito', '21364537K', 'Luis', 'Cambra', '11/01/2017', 'EspaÃ±ol:Ingles:', 'Developer web', 'Java', 'Java:Jquery:PHP:Javascript:', 'ssssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/7696joan.jpg', 'luiscambra@cambra.es', 123465789, 'COMUNIDAD', 'VALENCIA', 'ONTINYENT', '02/01/2002', 15, 1, 38.8208, -0.610514),
('', '37274689S', 'pepito', 'moniato', '19/01/2017', 'Ingles:', 'Marketing', 'Director de relaciones pÃºblicas', 'Publicidad:Director de relaciones pÃºblicas:', 'wssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 123456789, 'MELILLA', 'MELILLA', 'MELILLA', '11/01/2017', 54, NULL, 35.3031, -2.92326),
('', '38736826L', 'Adrian', 'Montolla', '12/01/2017', 'EspaÃ±ol:Ingles:Frances:Italiano:', 'Developer web', 'Angular', 'Django:Angular:', 'ssssssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/157156mYMKGh - copia.jpg', 'adrian@gmail.com', 789456123, 'MURCIA', 'REGION DE MURCIA', 'CEUTI', '15/01/2014', 40, NULL, 38.0796, -1.27192),
('', '45122378C', 'provetajoan', 'montes', '27/01/2017', 'Ingles:', 'Designer', 'Photoshop', 'Bootstrap:Photoshop:', 'ssdsdsdsdsdsdsdsd', '/freelancer/frontend/assets/media/dropzone/288246mYMKGh - copia.jpg', 'proveta@gmail.com', 123456789, 'COMUNIDAD', 'VALENCIA', 'ONTINYENT', '17/01/2017', 40, NULL, 38.8208, -0.610514),
('', '45125684M', 'began', 'ultra', '24/01/2017', 'Ingles:Frances:', 'Advocate', 'Laboral', 'Laboral:Mercantil:', 'sdss advocateeee', '/freelancer/frontend/assets/media/dropzone/29345joan.jpg', 'begain@gmail.com', 123456789, 'COMUNIDAD', 'VALENCIA', 'BOCAIRENT', '23/01/2017', 18, NULL, 38.7679, -0.607424),
('', '45452369R', 'young', 'martinez', '13/01/2017', 'EspaÃ±ol:Ingles:Frances:', 'Marketing', 'Gestor de comunicaciÃ³n', 'Gestor de comunicaciÃ³n:', 'sdasdasdsdsadasd', '/freelancer/frontend/assets/media/dropzone/288246mYMKGh - copia.jpg', 'yoput@gmail.com', 123456789, 'ANDALUCÃA', 'ISLAS CANARIAS', 'ALCUDIA', '04/01/2017', 17, NULL, 39.8516, 3.12332),
('', '45454522B', 'david', 'bisbal', '12/01/2017', 'EspaÃ±ol:Ingles:Frances:', 'Advocate', 'Laboral', 'Administrativo:Laboral:Mercantil:', 'comentttttttttttttttttttt', '/freelancer/frontend/assets/media/dropzone/6391joan.jpg', 'bisbal@gmai.com', 123456789, 'CASTILLA', 'SALAMANCA', 'ALCONADA', NULL, 22, NULL, 40.9114, -5.36306),
('mascle', '45654512F', 'Joaquim', 'Fernandez', '26/05/1998', 'Ingles', 'Advocate', 'Laboral', 'Civil:Herencias:Laboral', 'test', '/freelancer/frontend/assets/media/dropzone/157156mYMKGh - copia.jpg', 'joaquim@gmail.com', 789456123, 'COMUNIDAD VALENCIANA', 'VALENCIA', 'BENICOLET', '26/07/1991', 54, 0, 38.9194, -0.344204),
('', '48454545S', 'Javier', 'Caballero', '02/01/2017', 'Ingles:Frances:Italiano:', 'Developer mobile', 'React native', 'Swift:Xamarin:React native:', 'sssssssssssssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/115136mYMKGh - copia.jpg', 'prueba@gmail.com', 789456123, 'CASTILLA', 'SALAMANCA', 'ALCONADA', NULL, 25, NULL, 0, 0),
('', '48601111X', 'provadiii', 'vdfdf', '24/01/2017', 'EspaÃ±ol:Ingles:Frances:', 'Developer mobile', 'Jquery mobile', 'Swift:Jquery mobile:Xamarin:', 'cccccccomentariooooooo', '/freelancer/frontend/assets/media/dropzone/6391joan.jpg', 'confirma@gmail.com', 789456123, 'MELILLA', 'MELILLA', 'MELILLA', NULL, 27, NULL, 35.3031, -2.92326),
('', '48604510J', 'proviua', 'tortosa', '26/01/2017', 'EspaÃ±ol:Ingles:Frances:', 'Developer mobile', 'Java Android', 'Java Android:Swift:Jquery mobile:', 'hi han uns quans', '/freelancer/frontend/assets/media/dropzone/29345joan.jpg', 'tortosa@gmail.com', 789456123, 'PAÃS', 'GUIPUZCOA', 'ARAMA', '24/01/2017', 50, NULL, 0, 0),
('', '48944447P', 'Jordi', 'Samaruc', '18/01/2017', 'Ingles:Frances:', 'Developer mobile', 'Jquery mobile', 'Array', 'sssssssssssssssssssssssssa', '/freelancer/frontend/assets/media/dropzone/img_default.jpg', 'asdf@asdf.com', 989898989, 'COMUNIDAD VALENCIANA', 'VALENCIA', 'BOCAIRENT', '', 23, NULL, 38.7679, -0.607424),
('', '51467811H', 'provafreelancer', 'frellancer', '09/01/2017', 'EspaÃ±ol:Italiano:', 'Developer mobile', 'Xamarin', 'Swift:Jquery mobile:Xamarin:', 'gggggggggggggggggggg', '/freelancer/frontend/assets/media/dropzone/28431joan.jpg', 'proveta@gmail.com', 789456123, 'ES', 'VALENCIA', 'ONTINYENT', NULL, 22, NULL, 38.8208, -0.610514),
('', '54545454D', 'Quique', 'Fernandez', '05/01/2017', 'Ingles:Frances:', 'Marketing', 'Ejecutivo de cuentas', 'Publicidad:Gestor de comunicaciÃ³n:Ejecutivo de cuentas:', 'sssssssssssssssssssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/140756mYMKGh - copia.jpg', 'quiquefernan@gmial.com', 123456789, NULL, 'ISLAS CANARIAS', 'ALCUDIA', NULL, 28, NULL, 39.8516, 3.12332),
('', '71257420Q', 'Victor', 'Robles', '21/01/2015', 'EspaÃ±ol:', 'Developer web', 'PHP', 'Jquery:PHP:Javascript:', 'ssssssssssssssssssssssssssssssss', '/freelancer/frontend/assets/media/dropzone/img_default.jpg', 'victorobles@gmail.com', 123456789, 'COMUNIDAD', 'VALENCIA', 'BENICOLET', '18/01/1996', 30, NULL, 0, 0),
('', '78787845F', 'Ricardo', 'Lopez', '04/01/2017', 'EspaÃ±ol:', 'Designer', 'Bootstrap', 'HTML5:CSS3:Bootstrap:Photoshop:', 'fgfgfgfgfgfgfgfgfg', '/freelancer/frontend/assets/media/dropzone/290896mYMKGh - copia.jpg', 'ricardolope@gmail.com', 789456123, NULL, 'VALENCIA', 'BENICOLET', NULL, 35, NULL, 0, 0),
('', '78787878F', 'Alicia', 'cabedo', '12/01/2017', 'EspaÃ±ol:Ingles:', 'Advocate', 'Laboral', 'Laboral:Mercantil:Herencias:', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '/freelancer/frontend/assets/media/dropzone/290896mYMKGh - copia.jpg', 'aliciacabedo@gmail.com', 123456789, NULL, 'GUIPUZCOA', 'CHIPIONA', '', 46, NULL, 36.7472, -6.41887),
('', '92471927J', 'Maria Amparo', 'Pla Vaello', '18/01/2017', 'EspaÃ±ol:Ingles:', 'Advocate', 'Civil', 'Civil:Penal:Laboral:Herencias:', 'aSDASDSADSSSaaaaaaaaa', '/freelancer/frontend/assets/media/dropzone/162926mYMKGh - copia.jpg', 'amparopvo@icav.es', 789456123, 'CO_UNIDAD', 'VALENCIA', 'ONTINYENT', '25/01/1995', 25, NULL, 38.8208, -0.610514);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `best_freelancer`
--

CREATE TABLE `best_freelancer` (
  `id` int(10) NOT NULL,
  `dni_freelancer` varchar(150) NOT NULL,
  `stars` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `best_freelancer`
--

INSERT INTO `best_freelancer` (`id`, `dni_freelancer`, `stars`) VALUES
(2, '21364537K', '7'),
(3, '37274689S', '6.1'),
(4, '38736826L', '6.6'),
(5, '45125684M', '11.8'),
(6, '45122378C', '2.8'),
(7, '92471927J', '8.6'),
(8, '71257420Q', '2.9'),
(9, '01722926L', '10.5'),
(10, '48601111X', '2.5'),
(11, '78787845F', '2.7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(5) NOT NULL,
  `name_catg` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `value` int(50) NOT NULL,
  `media` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name_catg`, `value`, `media`) VALUES
(1, 'Developer web', 35, 'https://localhost/freelancer/frontend/assets/media/developer_web.jpeg'),
(2, 'Developer mobile', 0, 'https://localhost/freelancer/frontend/assets/media/developer_mobile.png'),
(3, 'Designer', 4, 'https://localhost/freelancer/frontend/assets/media/designer_web.jpg'),
(4, 'Advocate', 39, 'https://localhost/freelancer/frontend/assets/media/advocate.jpg'),
(5, 'Marketing', 9, 'https://localhost/freelancer/frontend/assets/media/marketing.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_user` varchar(145) NOT NULL,
  `dni` varchar(145) NOT NULL,
  `name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `fech_nac` varchar(50) NOT NULL,
  `idioma` varchar(150) NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phone` int(15) NOT NULL,
  `community` varchar(150) DEFAULT NULL,
  `province` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `expiration_dni` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_user`, `dni`, `name`, `last_name`, `fech_nac`, `idioma`, `avatar`, `email`, `phone`, `community`, `province`, `city`, `expiration_dni`) VALUES
('', '09773635S', 'pepito', 'yujuuuuu', '24/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'A_DALUCÃA', 'IS_AS', 'ALMERÃA', '28/01/2017'),
('', '09870137D', 'pepito', 'demooo', '16/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'ARA_Ã“N', 'T_RUEL', 'ABEJUELA', '25/01/2017'),
('', '12121212D', 'otra prueba', 'Developer', '02/01/2017', 'EspaÃ±ol:Italiano:', '/freelancer/media/dropzone/9587joan.jpg', 'pruee@hotmail.com', 123456789, NULL, NULL, NULL, NULL),
('', '12121212P', 'asdasd', 'asdasd', '03/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'COMUNI_AD', 'VALENCIA', 'ONTINYENT', '11/01/2017'),
('', '12785633N', 'altra pprofga', 'asdasd', '24/01/2017', 'Ingles:Frances:', '/freelancer/media/dropzone/img_default.jpg', 'jadaw@gmail.com', 123456789, 'GALICIA', 'LU_O', 'BEGONTE', NULL),
('', '23147850C', 'ricarditooo', 'csdsd', '03/01/2017', 'Frances:Italiano:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'community', 'province', 'city', NULL),
('', '23555056C', 'pepita', 'buenasss', '13/01/2017', 'EspaÃ±ol:Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'CANARIAS', 'LAS', 'FIRGAS', '25/01/2017'),
('', '32146578D', 'sdfsdfsd', 'sdfsdfd', '02/01/2017', 'EspaÃ±ol:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'COMUNIDAD', 'VALENCIA', 'ONTINYENT', NULL),
('', '44444444K', 'yoquese', 'Developer', '02/01/2017', 'Ingles:Frances:Italiano:', '', 'example@asds.es', 787878787, NULL, NULL, NULL, NULL),
('', '45124545C', 'asasdasd', 'asdsds', '17/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'ISLAS', 'CASTILLA-LA', 'ALCÃšDIA', '25/01/2017'),
('', '45126741F', 'pruebaaaaa', 'asdsdasd', '16/01/2017', 'EspaÃ±ol:Frances:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, '', 'province', 'city', NULL),
('', '45454545F', 'sasds', 'asds', '14/01/2017', 'Ingles:Italiano:', '', 'xcss@asds.es', 555555555, NULL, NULL, NULL, NULL),
('', '45651249O', 'Joan', 'Montes doria', '12/01/2017', 'Ingles:Frances:', '/freelancer/media/dropzone/312816mYMKGh - copia.jpg', 'joanontinyent@hotmail.es', 789456321, NULL, NULL, NULL, NULL),
('', '48304510E', 'pruebita', 'apellidos', '16/01/2017', 'EspaÃ±ol:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, '', 'province', 'city', NULL),
('', '48562877S', 'javi', 'caballero ruiz', '03/01/2017', 'EspaÃ±ol:Ingles:Italiano:', '/freelancer/media/dropzone/img_default.jpg', 'joanPercu@gmail.com', 698745248, NULL, NULL, NULL, NULL),
('', '48602145D', 'quique', 'montada', '04/01/2017', 'Ingles:Italiano:', '/freelancer/media/dropzone/180296mYMKGh - copia.jpg', 'quique@gmail.com', 789456123, 'ES', '12', 'Alqueria, La', NULL),
('', '48604471F', 'kike', 'ortiz', '04/01/2017', 'Ingles:', '/freelancer/media/dropzone/243766mYMKGh - copia.jpg', 'balkan@gmail.com', 999966556, NULL, NULL, NULL, NULL),
('', '48604510D', 'joanet', 'monteset', '23/01/2017', 'EspaÃ±ol:Ingles:', '/freelancer/media/dropzone/29345joan.jpg', 'joanet@gmail.com', 123456789, 'CATALUÃ‘A', 'MU_CIA', 'BANYOLES', '25/01/2017'),
('', '48604510F', 'juanito', 'balderrama', '17/01/2017', 'Frances:', '/freelancer/media/dropzone/30965joan.jpg', 'juanin@gmail.es', 654789321, NULL, NULL, NULL, NULL),
('', '48604510G', 'joanet', 'montes dorito', '06/01/2017', 'Frances:Italiano:', '/freelancer/media/dropzone/30965joan.jpg', 'joanmodaw@gmail.com', 999999999, NULL, NULL, NULL, NULL),
('', '48604510J', 'diumenget', 'ofreneta', '24/01/2017', 'EspaÃ±ol:Ingles:Frances:Italiano:', '/freelancer/media/dropzone/img_default.jpg', 'ofreneta@gmail.com', 789456123, 'COMUNIDA_', 'G_LICIA', 'ALFONDEGUILLA', '27/01/2017'),
('', '48944447P', 'dfdf', 'dfdfd', '', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'asdf@asdf.com', 989898989, NULL, NULL, NULL, NULL),
('', '54128519C', 'fulanito', 'detal', '04/01/2017', 'EspaÃ±ol:Ingles:', '/freelancer/media/dropzone/22584joan - copia (6).jpg', 'fulanito@gmail.com', 789456123, 'ARAGÃ“N', 'ZARAG_ZA', 'ALFAJARÃN', '19/01/2017'),
('', '56613589D', 'quique', 'ortiz', '03/01/2017', 'Ingles:Frances:Italiano:', '/freelancer/media/dropzone/img_default.jpg', 'ortiz@gmail.com', 789456123, 'PAÃS', 'VIZCAYA', 'BALMASEDA', '12/01/2017'),
('', '59039638H', 'juan', 'vellebalzo', '17/01/2017', 'Frances:', '/freelancer/media/dropzone/22584joan - copia (6).jpg', 'juanvelle@gmail.com', 789456123, 'PAÃS', 'GUIPÃšZCOA', 'ARAMA', '28/01/2017'),
('', '64631212Q', 'fulanitos', 'detalita', '03/01/2017', 'EspaÃ±ol:', '/freelancer/media/dropzone/22134myAvatar400.png', 'fulanita@gmail.com', 789456123, 'CATALUÃ‘A', 'CASTILLA', 'ALELLA', '11/01/2017'),
('', '69442505P', 'pepito', 'lafinal', '03/01/2017', 'EspaÃ±ol:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'CANARIAS', 'LAS', 'ANTIGUA', '27/01/2017'),
('', '70955019L', 'alba', 'caÃ±ete', '17/01/2017', 'Ingles:Frances:', '/freelancer/media/dropzone/img_default.jpg', 'alabacane@gmail.com', 789456123, 'NAVARRA', 'COMUNIDAD', 'ANDOSILLA', '27/01/2017'),
('', '78486792M', 'Bernardo', 'Gutierrez', '18/01/1996', 'EspaÃ±ol:', '/freelancer/media/dropzone/4638joan.jpg', 'gutierrez@gmail.com', 789456123, 'MADRID', 'COMUNIDAD', 'ARANJUEZ', '11/01/2017'),
('', '78787878J', 'ricardo', 'Developer', '12/01/2017', 'Ingles:Frances:Italiano:', '/freelancer/media/dropzone/27390joan.jpg', 'joanmodaw@gmail.com', 999999999, NULL, NULL, NULL, NULL),
('', '79516997V', 'pepito', 'ijaaaa', '16/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'ARAGÃ“_', 'TERUEL', 'AGUAVIVA', '27/01/2017'),
('', '83132643V', 'provadivendres', 'divendresss', '11/01/2017', 'EspaÃ±ol:Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'diven@aasd.es', 789485612, 'COMUNIDAD', 'VALEN_IA', 'ADEMUZ', '21/01/2017'),
('', '87808446N', 'Oscar', 'Quintana', '04/01/2017', 'EspaÃ±ol:Ingles:', '/freelancer/media/dropzone/3310joan.jpg', 'oscarquin@hotmail.com', 123456789, 'ANDALUCÃA', 'IS_AS', 'ALCÃ“NTAR', '28/01/2017'),
('', '88393616Q', 'pepito', 'eldelcanto', '04/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'ANDALUCÃA', 'EXTREMADURA', 'CHIPIONA', '12/01/2017'),
('', '94666894K', 'pepito', 'yeahh', '02/01/2017', 'Ingles:', '/freelancer/media/dropzone/img_default.jpg', 'joanmodaw@gmail.com', 789456123, 'MADRID', 'COMUNIDAD', 'BATRES', '21/01/2017'),
('', '96600395G', 'oscar', 'maller', '04/01/2017', 'Ingles:', '/freelancer/media/dropzone/22584joan - copia (6).jpg', 'oscar@gmail.com', 789456123, 'EXTREMADURA', 'CAN_ABRIA', 'BADAJOZ', '18/01/2017'),
('', '98090536K', 'sonia', 'martinez', '04/01/2017', 'EspaÃ±ol:Ingles:Frances:', '/freelancer/media/dropzone/223656mYMKGh - copia.jpg', 'soniamar@gmail.com', 789456123, 'CATALUÃ‘A', 'MURCIA', 'CAMPRODON', '19/01/2017'),
('', '98986532W', 'santa', 'lucia', '24/01/2017', 'Ingles:Frances:', '/freelancer/media/dropzone/img_default.jpg', 'santaluc@gmail.com', 789456123, 'LA', 'LA', 'ANGUIANO', '31/01/2017'),
('', '99999999C', 'joaniu', 'monteset', '15/01/2017', 'EspaÃ±ol:Ingles:Frances:Italiano:', '/freelancer/media/dropzone/12420joan.jpg', 'yoqwuse@sesf.es', 999999999, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum`
--

CREATE TABLE `forum` (
  `id` int(10) NOT NULL,
  `item` varchar(150) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `id_user` varchar(150) DEFAULT NULL,
  `date` varchar(150) DEFAULT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `karma` varchar(150) DEFAULT NULL,
  `num_like` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `forum`
--

INSERT INTO `forum` (`id`, `item`, `comment`, `id_user`, `date`, `avatar`, `name`, `karma`, `num_like`) VALUES
(34, 'Presentacion', 'Hola me llamo Quique y soy un comentario prueba', '1939194526115227', '2018-06-05 21:47:03', '/freelancer/backend/media/dropzone/3283myAvatar200.png', 'Quique', 'aprendiz', 3),
(38, 'Proceso Freelancer', '1. Alta en el RETA (seguridad social); 2. Facturas; 3. Declararlo todo ante hacienda; Espero que os sirva de utilidad. Saludos.', '986158469488181254', '2018-06-06 10:20:01', 'https://localhost/freelancer/backend/media/gravatar/1.png', 'Joan', 'aprendiz', 1),
(42, 'Proceso Freelancer', 'Hola Joan, muchas gracias por tu aportacion, me ha sigo de gran ayuda. Gracias.', '100532158346388412174', '2018-06-06 11:19:17', 'https://lh4.googleusercontent.com/-4TSDBtyLCkE/AAAAAAAAAAI/AAAAAAAAANM/30_z1PX-rbc/photo.jpg', 'Jomondo', 'aprendiz', 0),
(103, 'Presentacion', 'Hola Quique, bienvendo al foro.', 'moderador', '2018-06-08 20:53:11', 'https://localhost/freelancer/backend/media/gravatar/6.png', 'moderador', 'aprendiz', 1),
(104, 'Presentacion', 'Bienvenido', 'joanet', '2018-06-09 12:17:09', '/freelancer/backend/media/dropzone/301256mYMKGh - copia.jpg', 'joanet', 'moderate freelance', 0),
(105, 'Proceso Freelancer', 'Gracias por comentar', 'joanet', '2018-06-09 12:17:57', '/freelancer/backend/media/dropzone/301256mYMKGh - copia.jpg', 'joanet', 'moderate freelance', 0),
(113, 'Presentacion', 'yeahaaaaaaaasdsdsdsdddfdf', 'sergi', '2018-06-09 12:31:51', 'https://localhost/freelancer/backend/media/gravatar/2.png', 'sergi', 'aprendiz', 2),
(114, 'Presentacion', 'testrererer', 'moderador', '2018-06-10 18:45:52', 'https://localhost/freelancer/backend/media/gravatar/6.png', 'moderador', 'aprendiz', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_likes`
--

CREATE TABLE `forum_likes` (
  `id_like` int(10) NOT NULL,
  `id_item` varchar(150) NOT NULL,
  `id_user` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `forum_likes`
--

INSERT INTO `forum_likes` (`id_like`, `id_item`, `id_user`) VALUES
(28, '34', 'joanet'),
(29, '103', 'joanet'),
(30, '34', 'moderador'),
(31, '34', 'sergi'),
(32, '113', 'sergi'),
(33, '113', 'moderador'),
(34, '38', 'moderador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `likes`
--

CREATE TABLE `likes` (
  `id` int(5) NOT NULL,
  `dni_freelancer` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `id_user` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `likes`
--

INSERT INTO `likes` (`id`, `dni_freelancer`, `id_user`) VALUES
(1, '01722926L', 'joanet'),
(4, '21364537K', 'joanet'),
(6, '54545454D', 'joanet'),
(9, '71257420Q', '100532158346388412174'),
(10, '45125684M', '100532158346388412174'),
(11, '92471927J', '100532158346388412174'),
(17, '51467811H', '100532158346388412174'),
(18, '01722926L', '100532158346388412174'),
(20, '37274689S', 'joanet'),
(23, '01722926L', 'sergi');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_register`
--

CREATE TABLE `user_register` (
  `id_user` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `type` int(5) NOT NULL,
  `token` varchar(2000) NOT NULL,
  `date` varchar(150) NOT NULL,
  `activate` int(4) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `birthday` varchar(150) DEFAULT NULL,
  `exp_dni` varchar(150) DEFAULT NULL,
  `language` varchar(150) DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `community` varchar(150) DEFAULT NULL,
  `province` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `points` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_register`
--

INSERT INTO `user_register` (`id_user`, `name`, `passwd`, `email`, `avatar`, `type`, `token`, `date`, `activate`, `last_name`, `birthday`, `exp_dni`, `language`, `phone`, `community`, `province`, `city`, `points`) VALUES
('100532158346388412174', 'Jomondo', '', 'joanmodaw@gmail.com', 'https://lh4.googleusercontent.com/-4TSDBtyLCkE/AAAAAAAAAAI/AAAAAAAAANM/30_z1PX-rbc/photo.jpg', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjEwMDUzMjE1ODM0NjM4ODQxMjE3NCI=.M1fM7kJJBXIvdkIbenf/oqbS/hodHe5Sod2SZlKA94E=', '2018-05-19 12:45:46', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6'),
('1939194526115227', 'Quique', '', 'joannyent@hotmail.es', '/freelancer/backend/media/dropzone/3283myAvatar200.png', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjE5MzkxOTQ1MjYxMTUyMjci.KzlFEy6oUd+gMe6VBi/iRicNBLoUPvujgy7uOy/2qVc=', '2018-05-19 13:18:57', 1, 'Caballero', '4/12/2017', '05/09/2018', 'Frances:Italiano:', 123456789, '11', '10', '009', '2'),
('986158469488181254', 'Joan', '', 'joanmodaw@gmail.com', 'https://localhost/freelancer/backend/media/gravatar/1.png', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.Ijk4NjE1ODQ2OTQ4ODE4MTI1NCI=.ynwg2beEXuJU8tfDgqNKMVWdt+/O3uWdLEeCqyDNG30=', '2018-05-19 13:08:03', 1, 'red social', '05/09/2018', '05/24/2018', 'EspaÃ±ol:Ingles:', 789456123, '06', '39', '018', '2'),
('joanet', 'joanet', '$2y$10$jpIDbdwNWkR3widh0.lgrObJJZ8N.eC9wRkunBWmXByVGz8l2vFN2', 'joanmodaw@gmail.com.', '/freelancer/backend/media/dropzone/301256mYMKGh - copia.jpg', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.ImpvYW5ldCI=.UwIVIbEHFDLfeCmQaoo5Nw0B02vQCKfs9w3FU5yV6Gs=', '2018-05-21 13:43:37', 1, 'testeroide', '11/21/2018', '12/18/2018', 'EspaÃ±ol:Ingles:Frances:Italiano:', 789456123, '10', '46', '184', '6'),
('moderador', 'moderador', '$2y$10$OOKs/mv128/MtoIjO/C7T.WDGp/pCIVs6Q4LqOVR7EASosUDx8tIK', 'joanmodaw@gmail.com', 'https://localhost/freelancer/backend/media/gravatar/6.png', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.Im1vZGVyYWRvciI=.5AN09gHXCtMedBe+XK5pIiFl4r0lxavMl2Nn/T/4c7g=', '2018-06-08 13:28:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2'),
('pepito', 'pepito', '$2y$10$pHJfwvPaEHqGu.gP81Ba1OTqPrcXsctOIqnXJz7TbCr6PS7lB8jMi', 'joanmodaw@gmail.com.', '/freelancer/backend/media/dropzone/23451myAvatar200.png', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.InBlcGl0byI=.jYOhhNwQFgF2rXi7q9sJ8E8WYmA5j/LiLgTh2/PGe60=', '2018-05-23 13:06:25', 2, 'Gutierrez', '03/12/1990', '09/12/1991', 'EspaÃ±ol:Italiano:', 789465123, '10', '46', '184', '1'),
('sergi', 'sergi', '$2y$10$/IgwWos31opYJhH4I.y5oO36WbURJKawssoScAxU07vjxHRTS6LGa', 'joanmodaw@gmail.com', 'https://localhost/freelancer/backend/media/gravatar/2.png', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.InNlcmdpIg==.oM3AA2GkZ1oWgf4BgELuBRZCPHjCty/cPF2e+eHMFLc=', '2018-06-07 11:48:03', 1, 'chafer', '12/05/2017', '06/19/2018', 'EspaÃ±ol:Ingles:', 789456123, '09', '08', '019', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `value_freelancer`
--

CREATE TABLE `value_freelancer` (
  `id` int(150) NOT NULL,
  `dni_freelancer` varchar(150) NOT NULL,
  `id_valorador` varchar(150) NOT NULL,
  `stars` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `value_freelancer`
--

INSERT INTO `value_freelancer` (`id`, `dni_freelancer`, `id_valorador`, `stars`) VALUES
(1, '01722926L', 'google-oauth2|100532158346388412174', '2'),
(2, '01722926L', 'facebook|1909274919107188', '4'),
(4, '01722926L', 'twitter|986158469488181254', '7.4'),
(5, '38736826L', 'pepito', '2'),
(7, '45125684M', 'pepito', '4.5'),
(10, '21364537K', 'pepito', '3'),
(11, '21364537K', 'twitter|986158469488181254', '5'),
(12, '37274689S', 'twitter|986158469488181254', '3.7'),
(13, '38736826L', 'twitter|986158469488181254', '6.6'),
(14, '45125684M', 'twitter|986158469488181254', '8.8'),
(15, '45122378C', 'twitter|986158469488181254', '2.8'),
(16, '92471927J', 'twitter|986158469488181254', '5'),
(17, '92471927J', 'google-oauth2|100532158346388412174', '8.6'),
(18, '71257420Q', 'facebook|1909274919107188', '2.9'),
(19, '45125684M', 'joanet', '11.8'),
(20, '92471927J', '1939194526115227', '8.6'),
(21, '37274689S', 'joanet', '6.1'),
(22, '01722926L', '1939194526115227', '8'),
(23, '48601111X', 'Adrian', '2.5'),
(24, '78787845F', 'Adrian', '2.7'),
(25, '21364537K', 'joanet', '7'),
(26, '01722926L', '100532158346388412174', '10.5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `value_website`
--

CREATE TABLE `value_website` (
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `stars` varchar(20) NOT NULL,
  `message` varchar(500) NOT NULL,
  `avatar` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `value_website`
--

INSERT INTO `value_website` (`name`, `category`, `stars`, `message`, `avatar`) VALUES
('Joan El Gran', '', '3.5', 'Es una plataforma web muy buena para encontrar a profesionales', 'https://lookaside.facebook.com/platform/profilepic/?asid=1909274919107188&height=50&width=50&ext=1525862229&hash=AeQFBdGgbboidFCH'),
('Jomondo', '', '3.7', 'Freelancer me ha ayudado a encontrar a profesionales', 'https://lh4.googleusercontent.com/-4TSDBtyLCkE/AAAAAAAAAAI/AAAAAAAAANM/30_z1PX-rbc/photo.jpg'),
('pepito', '', '4.7', 'Gracias a esta web pude hacer un proyecto que tenia entre manos', 'https://localhost/freelancer/media/gravatar/9.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitors`
--

CREATE TABLE `visitors` (
  `id` int(100) NOT NULL,
  `ip` varchar(150) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visitors`
--

INSERT INTO `visitors` (`id`, `ip`, `date`) VALUES
(2, '3571', '2018-06-01'),
(3, '20603', '2018-06-01'),
(4, '11616', '2018-06-01'),
(5, '10928', '2018-06-01'),
(6, '16055', '2018-06-01'),
(7, '20542', '2018-06-01'),
(8, '9764', '2018-06-01'),
(9, '25997', '2018-06-01'),
(10, '25410', '2018-06-01'),
(11, '31292', '2018-06-01'),
(12, '16031', '2018-06-01'),
(13, '783', '2018-06-02'),
(14, '21705', '2018-06-02'),
(15, '3996', '2018-06-02'),
(16, '23973', '2018-06-02'),
(17, '1998', '2018-06-02'),
(18, '18159', '2018-06-02'),
(19, '7530', '2018-06-02'),
(20, '4766', '2018-06-02'),
(21, '13820', '2018-06-02'),
(22, '16767', '2018-06-02'),
(23, '603', '2018-06-02'),
(24, '17740', '2018-06-02'),
(25, '18821', '2018-06-02'),
(26, '27384', '2018-06-02'),
(27, '19409', '2018-06-02'),
(28, '986', '2018-06-02'),
(29, '31773', '2018-06-02'),
(30, '29829', '2018-06-02'),
(31, '9875', '2018-06-02'),
(32, '26320', '2018-06-02'),
(33, '24210', '2018-06-02'),
(34, '21210', '2018-06-02'),
(35, '26935', '2018-06-02'),
(36, '25355', '2018-06-02'),
(37, '2022', '2018-06-02'),
(38, '28771', '2018-06-02'),
(39, '8487', '2018-06-02'),
(40, '16395', '2018-06-02'),
(41, '2440', '2018-06-02'),
(42, '12244', '2018-06-02'),
(43, '13408', '2018-06-02'),
(44, '9597', '2018-06-02'),
(45, '13800', '2018-06-02'),
(46, '22217', '2018-06-02'),
(47, '12065', '2018-06-02'),
(48, '15474', '2018-06-02'),
(49, '20200', '2018-06-02'),
(50, '20294', '2018-06-02'),
(51, '15641', '2018-06-02'),
(52, '21505', '2018-06-02'),
(53, '25038', '2018-06-02'),
(54, '22933', '2018-06-02'),
(55, '17062', '2018-06-02'),
(56, '17671', '2018-06-02'),
(57, '30977', '2018-06-02'),
(58, '15018', '2018-06-02'),
(59, '16036', '2018-06-02'),
(60, '934', '2018-06-02'),
(61, '13876', '2018-06-02'),
(62, '20958', '2018-06-02'),
(63, '7261', '2018-06-02'),
(64, '6383', '2018-06-02'),
(65, '20967', '2018-06-02'),
(66, '10435', '2018-06-02'),
(67, '8188', '2018-06-02'),
(68, '21517', '2018-06-02'),
(69, '28610', '2018-06-02'),
(70, '9447', '2018-06-02'),
(71, '6610', '2018-06-02'),
(72, '10915', '2018-06-02'),
(73, '8479', '2018-06-02'),
(74, '15468', '2018-06-02'),
(75, '17778', '2018-06-02'),
(76, '32195', '2018-06-02'),
(77, '8913', '2018-06-02'),
(78, '2939', '2018-06-02'),
(79, '1575', '2018-06-02'),
(80, '11198', '2018-06-02'),
(81, '26045', '2018-06-02'),
(82, '3480', '2018-06-02'),
(83, '12598', '2018-06-02'),
(84, '9792', '2018-06-02'),
(85, '21045', '2018-06-02'),
(86, '12500', '2018-06-02'),
(87, '13343', '2018-06-02'),
(88, '6066', '2018-06-02'),
(89, '771', '2018-06-02'),
(90, '6614', '2018-06-02'),
(91, '25216', '2018-06-02'),
(92, '21356', '2018-06-02'),
(93, '31097', '2018-06-02'),
(94, '9900', '2018-06-02'),
(95, '22218', '2018-06-02'),
(96, '18110', '2018-06-02'),
(97, '12347', '2018-06-02'),
(98, '7477', '2018-06-02'),
(99, '13144', '2018-06-02'),
(100, '17866', '2018-06-02'),
(101, '29499', '2018-06-02'),
(102, '4733', '2018-06-02'),
(103, '23729', '2018-06-02'),
(104, '14936', '2018-06-02'),
(105, '26117', '2018-06-02'),
(106, '1600', '2018-06-02'),
(107, '9918', '2018-06-02'),
(108, '29846', '2018-06-02'),
(109, '4895', '2018-06-02'),
(110, '13087', '2018-06-02'),
(111, '6199', '2018-06-02'),
(112, '27460', '2018-06-02'),
(113, '5165', '2018-06-03'),
(114, '9818', '2018-06-03'),
(115, '27402', '2018-06-03'),
(116, '32098', '2018-06-03'),
(117, '2071', '2018-06-03'),
(118, '28874', '2018-06-03'),
(119, '16955', '2018-06-03'),
(120, '20347', '2018-06-03'),
(121, '11524', '2018-06-03'),
(122, '23896', '2018-06-03'),
(123, '16049', '2018-06-03'),
(124, '29986', '2018-06-03'),
(125, '408', '2018-06-03'),
(126, '6925', '2018-06-03'),
(127, '27571', '2018-06-03'),
(128, '10946', '2018-06-03'),
(129, '11761', '2018-06-03'),
(130, '211', '2018-06-03'),
(131, '5488', '2018-06-03'),
(132, '28403', '2018-06-03'),
(133, '4298', '2018-06-03'),
(134, '28905', '2018-06-03'),
(135, '25147', '2018-06-03'),
(136, '11734', '2018-06-03'),
(137, '26711', '2018-06-03'),
(138, '30640', '2018-06-03'),
(139, '28811', '2018-06-03'),
(140, '21520', '2018-06-03'),
(141, '17961', '2018-06-03'),
(142, '9220', '2018-06-03'),
(143, '15853', '2018-06-03'),
(144, '3502', '2018-06-03'),
(145, '17442', '2018-06-03'),
(146, '32088', '2018-06-03'),
(147, '20374', '2018-06-03'),
(148, '16750', '2018-06-03'),
(149, '4111', '2018-06-03'),
(150, '23305', '2018-06-03'),
(151, '1871', '2018-06-03'),
(152, '18199', '2018-06-03'),
(153, '6300', '2018-06-03'),
(154, '30056', '2018-06-03'),
(155, '24284', '2018-06-03'),
(156, '23937', '2018-06-03'),
(157, '20006', '2018-06-03'),
(158, '7141', '2018-06-03'),
(159, '3763', '2018-06-03'),
(160, '12965', '2018-06-03'),
(161, '30468', '2018-06-03'),
(162, '24241', '2018-06-03'),
(163, '12558', '2018-06-03'),
(164, '2663', '2018-06-03'),
(165, '22202', '2018-06-03'),
(166, '18795', '2018-06-03'),
(167, '5357', '2018-06-03'),
(168, '22319', '2018-06-03'),
(169, '3272', '2018-06-03'),
(170, '8508', '2018-06-03'),
(171, '19433', '2018-06-03'),
(172, '20', '2018-06-03'),
(173, '1658', '2018-06-03'),
(174, '7877', '2018-06-03'),
(175, '4449', '2018-06-03'),
(176, '9350', '2018-06-03'),
(177, '20506', '2018-06-03'),
(178, '7723', '2018-06-03'),
(179, '8004', '2018-06-03'),
(180, '30301', '2018-06-03'),
(181, '19445', '2018-06-03'),
(182, '6086', '2018-06-03'),
(183, '25854', '2018-06-03'),
(184, '11658', '2018-06-03'),
(185, '23552', '2018-06-03'),
(186, '23609', '2018-06-03'),
(187, '7131', '2018-06-03'),
(188, '9120', '2018-06-03'),
(189, '18040', '2018-06-03'),
(190, '5215', '2018-06-03'),
(191, '8187', '2018-06-03'),
(192, '7001', '2018-06-03'),
(193, '29470', '2018-06-03'),
(194, '9095', '2018-06-03'),
(195, '20140', '2018-06-03'),
(196, '22527', '2018-06-03'),
(197, '18456', '2018-06-03'),
(198, '16721', '2018-06-03'),
(199, '21118', '2018-06-03'),
(200, '32283', '2018-06-03'),
(201, '6005', '2018-06-03'),
(202, '22988', '2018-06-03'),
(203, '21258', '2018-06-03'),
(204, '6011', '2018-06-03'),
(205, '11702', '2018-06-03'),
(206, '7096', '2018-06-03'),
(207, '3473', '2018-06-03'),
(208, '9002', '2018-06-03'),
(209, '10648', '2018-06-03'),
(210, '6839', '2018-06-03'),
(211, '29979', '2018-06-03'),
(212, '18825', '2018-06-03'),
(213, '5590', '2018-06-03'),
(214, '12913', '2018-06-03'),
(215, '6230', '2018-06-03'),
(216, '16599', '2018-06-03'),
(217, '29432', '2018-06-03'),
(218, '29530', '2018-06-03'),
(219, '13265', '2018-06-03'),
(220, '20507', '2018-06-03'),
(221, '13501', '2018-06-03'),
(222, '15026', '2018-06-03'),
(223, '14731', '2018-06-03'),
(224, '12951', '2018-06-03'),
(225, '19706', '2018-06-03'),
(226, '3336', '2018-06-03'),
(227, '2664', '2018-06-03'),
(228, '28214', '2018-06-03'),
(229, '4910', '2018-06-03'),
(230, '15492', '2018-06-03'),
(231, '4407', '2018-06-03'),
(232, '18084', '2018-06-03'),
(233, '2250', '2018-06-03'),
(234, '27139', '2018-06-03'),
(235, '20097', '2018-06-03'),
(236, '27430', '2018-06-03'),
(237, '12177', '2018-06-03'),
(238, '11425', '2018-06-03'),
(239, '11373', '2018-06-03'),
(240, '15744', '2018-06-03'),
(241, '20642', '2018-06-03'),
(242, '24505', '2018-06-03'),
(243, '7926', '2018-06-03'),
(244, '21239', '2018-06-03'),
(245, '25447', '2018-06-03'),
(246, '17764', '2018-06-03'),
(247, '15373', '2018-06-03'),
(248, '13261', '2018-06-03'),
(249, '14274', '2018-06-03'),
(250, '24271', '2018-06-03'),
(251, '20478', '2018-06-03'),
(252, '2515', '2018-06-03'),
(253, '9055', '2018-06-03'),
(254, '13442', '2018-06-03'),
(255, '3081', '2018-06-03'),
(256, '3598', '2018-06-03'),
(257, '10131', '2018-06-03'),
(258, '8284', '2018-06-03'),
(259, '5042', '2018-06-03'),
(260, '20227', '2018-06-03'),
(261, '31291', '2018-06-03'),
(262, '21456', '2018-06-03'),
(263, '24883', '2018-06-03'),
(264, '12716', '2018-06-03'),
(265, '32613', '2018-06-03'),
(266, '7408', '2018-06-03'),
(267, '13656', '2018-06-03'),
(268, '15989', '2018-06-03'),
(269, '13370', '2018-06-03'),
(270, '16329', '2018-06-03'),
(271, '5838', '2018-06-03'),
(272, '28170', '2018-06-03'),
(273, '16000', '2018-06-03'),
(274, '28719', '2018-06-03'),
(275, '10735', '2018-06-03'),
(276, '8809', '2018-06-03'),
(277, '14001', '2018-06-03'),
(278, '26518', '2018-06-03'),
(279, '19641', '2018-06-03'),
(280, '32535', '2018-06-03'),
(281, '23804', '2018-06-03'),
(282, '5755', '2018-06-03'),
(283, '20228', '2018-06-03'),
(284, '24517', '2018-06-03'),
(285, '6379', '2018-06-03'),
(286, '26184', '2018-06-03'),
(287, '32026', '2018-06-03'),
(288, '3309', '2018-06-03'),
(289, '24651', '2018-06-03'),
(290, '15512', '2018-06-03'),
(291, '26402', '2018-06-03'),
(292, '30131', '2018-06-03'),
(293, '19697', '2018-06-03'),
(294, '3973', '2018-06-03'),
(295, '9353', '2018-06-03'),
(296, '17372', '2018-06-03'),
(297, '7397', '2018-06-03'),
(298, '17338', '2018-06-03'),
(299, '11049', '2018-06-03'),
(300, '26143', '2018-06-03'),
(301, '3758', '2018-06-03'),
(302, '14532', '2018-06-03'),
(303, '5037', '2018-06-03'),
(304, '18146', '2018-06-03'),
(305, '7532', '2018-06-03'),
(306, '31886', '2018-06-03'),
(307, '22947', '2018-06-03'),
(308, '14963', '2018-06-03'),
(309, '7989', '2018-06-03'),
(310, '31919', '2018-06-03'),
(311, '7472', '2018-06-03'),
(312, '1184', '2018-06-03'),
(313, '8138', '2018-06-03'),
(314, '17723', '2018-06-03'),
(315, '6332', '2018-06-03'),
(316, '29775', '2018-06-03'),
(317, '9304', '2018-06-03'),
(318, '18396', '2018-06-03'),
(319, '12907', '2018-06-03'),
(320, '31153', '2018-06-03'),
(321, '26713', '2018-06-03'),
(322, '69', '2018-06-03'),
(323, '29594', '2018-06-03'),
(324, '15390', '2018-06-03'),
(325, '16840', '2018-06-03'),
(326, '21402', '2018-06-03'),
(327, '8801', '2018-06-03'),
(328, '15785', '2018-06-03'),
(329, '27903', '2018-06-03'),
(330, '13515', '2018-06-03'),
(331, '28310', '2018-06-03'),
(332, '20709', '2018-06-03'),
(333, '28462', '2018-06-03'),
(334, '1192', '2018-06-03'),
(335, '21707', '2018-06-04'),
(336, '24966', '2018-06-04'),
(337, '2767', '2018-06-04'),
(338, '2631', '2018-06-04'),
(339, '31255', '2018-06-04'),
(340, '26554', '2018-06-04'),
(341, '11780', '2018-06-04'),
(342, '11845', '2018-06-04'),
(343, '9289', '2018-06-04'),
(344, '17258', '2018-06-04'),
(345, '26049', '2018-06-04'),
(346, '20838', '2018-06-04'),
(347, '27034', '2018-06-04'),
(348, '27815', '2018-06-04'),
(349, '20315', '2018-06-04'),
(350, '13304', '2018-06-04'),
(351, '24910', '2018-06-04'),
(352, '25199', '2018-06-04'),
(353, '4811', '2018-06-04'),
(354, '2728', '2018-06-04'),
(355, '21844', '2018-06-04'),
(356, '21818', '2018-06-04'),
(357, '27073', '2018-06-04'),
(358, '7421', '2018-06-04'),
(359, '27516', '2018-06-04'),
(360, '7666', '2018-06-04'),
(361, '8782', '2018-06-04'),
(362, '1382', '2018-06-04'),
(363, '11331', '2018-06-04'),
(364, '15365', '2018-06-04'),
(365, '3951', '2018-06-04'),
(366, '17498', '2018-06-04'),
(367, '13963', '2018-06-04'),
(368, '5244', '2018-06-04'),
(369, '12549', '2018-06-04'),
(370, '4263', '2018-06-04'),
(371, '12379', '2018-06-04'),
(372, '248', '2018-06-04'),
(373, '10580', '2018-06-04'),
(374, '17256', '2018-06-04'),
(375, '1591', '2018-06-04'),
(376, '4993', '2018-06-04'),
(377, '6052', '2018-06-04'),
(378, '14605', '2018-06-04'),
(379, '5466', '2018-06-04'),
(380, '13195', '2018-06-04'),
(381, '4163', '2018-06-04'),
(382, '9707', '2018-06-04'),
(383, '26301', '2018-06-04'),
(384, '31848', '2018-06-04'),
(385, '28866', '2018-06-04'),
(386, '20179', '2018-06-04'),
(387, '2576', '2018-06-04'),
(388, '11318', '2018-06-04'),
(389, '29376', '2018-06-04'),
(390, '22793', '2018-06-04'),
(391, '27651', '2018-06-04'),
(392, '13767', '2018-06-04'),
(393, '13476', '2018-06-04'),
(394, '3065', '2018-06-04'),
(395, '4621', '2018-06-04'),
(396, '10046', '2018-06-04'),
(397, '18847', '2018-06-04'),
(398, '22286', '2018-06-04'),
(399, '1327', '2018-06-04'),
(400, '4089', '2018-06-04'),
(401, '21954', '2018-06-04'),
(402, '23358', '2018-06-04'),
(403, '28063', '2018-06-04'),
(404, '20435', '2018-06-04'),
(405, '6016', '2018-06-04'),
(406, '5149', '2018-06-04'),
(407, '5778', '2018-06-04'),
(408, '9434', '2018-06-04'),
(409, '19227', '2018-06-04'),
(410, '18531', '2018-06-04'),
(411, '11360', '2018-06-04'),
(412, '21785', '2018-06-04'),
(413, '2526', '2018-06-04'),
(414, '1983', '2018-06-04'),
(415, '23331', '2018-06-04'),
(416, '12312', '2018-06-04'),
(417, '25784', '2018-06-04'),
(418, '19723', '2018-06-04'),
(419, '31345', '2018-06-04'),
(420, '22504', '2018-06-04'),
(421, '19969', '2018-06-04'),
(422, '11916', '2018-06-04'),
(423, '23803', '2018-06-04'),
(424, '10709', '2018-06-04'),
(425, '13478', '2018-06-04'),
(426, '12007', '2018-06-04'),
(427, '30488', '2018-06-04'),
(428, '16470', '2018-06-04'),
(429, '13681', '2018-06-04'),
(430, '24406', '2018-06-04'),
(431, '18647', '2018-06-04'),
(432, '13764', '2018-06-04'),
(433, '26090', '2018-06-04'),
(434, '24795', '2018-06-04'),
(435, '31703', '2018-06-05'),
(436, '30002', '2018-06-05'),
(437, '8849', '2018-06-05'),
(438, '6882', '2018-06-05'),
(439, '10104', '2018-06-05'),
(440, '3665', '2018-06-05'),
(441, '5622', '2018-06-05'),
(442, '32247', '2018-06-05'),
(443, '14678', '2018-06-05'),
(444, '21123', '2018-06-05'),
(445, '15360', '2018-06-05'),
(446, '27184', '2018-06-05'),
(447, '19927', '2018-06-05'),
(448, '30390', '2018-06-05'),
(449, '12215', '2018-06-05'),
(450, '22212', '2018-06-05'),
(451, '5220', '2018-06-05'),
(452, '10272', '2018-06-05'),
(453, '15417', '2018-06-05'),
(454, '1497', '2018-06-05'),
(455, '32371', '2018-06-05'),
(456, '22957', '2018-06-05'),
(457, '5346', '2018-06-05'),
(458, '17188', '2018-06-05'),
(459, '22221', '2018-06-05'),
(460, '16013', '2018-06-05'),
(461, '19358', '2018-06-05'),
(462, '1663', '2018-06-05'),
(463, '4179', '2018-06-05'),
(464, '9877', '2018-06-05'),
(465, '938', '2018-06-05'),
(466, '28787', '2018-06-05'),
(467, '12926', '2018-06-05'),
(468, '5087', '2018-06-05'),
(469, '2971', '2018-06-05'),
(470, '6960', '2018-06-05'),
(471, '15635', '2018-06-05'),
(472, '6188', '2018-06-05'),
(473, '31568', '2018-06-05'),
(474, '4400', '2018-06-05'),
(475, '8617', '2018-06-05'),
(476, '7470', '2018-06-05'),
(477, '7042', '2018-06-05'),
(478, '28048', '2018-06-05'),
(479, '8399', '2018-06-05'),
(480, '13961', '2018-06-05'),
(481, '9870', '2018-06-05'),
(482, '24239', '2018-06-05'),
(483, '21180', '2018-06-05'),
(484, '12869', '2018-06-05'),
(485, '14940', '2018-06-05'),
(486, '4453', '2018-06-05'),
(487, '7578', '2018-06-05'),
(488, '24066', '2018-06-05'),
(489, '24122', '2018-06-05'),
(490, '21267', '2018-06-05'),
(491, '4188', '2018-06-05'),
(492, '24232', '2018-06-05'),
(493, '4187', '2018-06-05'),
(494, '15381', '2018-06-05'),
(495, '23617', '2018-06-05'),
(496, '26422', '2018-06-05'),
(497, '20920', '2018-06-05'),
(498, '24368', '2018-06-06'),
(499, '3133', '2018-06-06'),
(500, '9780', '2018-06-06'),
(501, '8702', '2018-06-06'),
(502, '3423', '2018-06-06'),
(503, '19843', '2018-06-06'),
(504, '5687', '2018-06-06'),
(505, '2514', '2018-06-06'),
(506, '21420', '2018-06-06'),
(507, '1831', '2018-06-06'),
(508, '26532', '2018-06-06'),
(509, '7636', '2018-06-06'),
(510, '18701', '2018-06-06'),
(511, '16578', '2018-06-06'),
(512, '14453', '2018-06-06'),
(513, '23203', '2018-06-06'),
(514, '31613', '2018-06-06'),
(515, '23154', '2018-06-06'),
(516, '29088', '2018-06-06'),
(517, '24586', '2018-06-06'),
(518, '20825', '2018-06-06'),
(519, '3831', '2018-06-06'),
(520, '28958', '2018-06-06'),
(521, '8315', '2018-06-06'),
(522, '6912', '2018-06-06'),
(523, '32408', '2018-06-06'),
(524, '12089', '2018-06-06'),
(525, '10622', '2018-06-06'),
(526, '8322', '2018-06-06'),
(527, '17216', '2018-06-06'),
(528, '9107', '2018-06-06'),
(529, '32255', '2018-06-06'),
(530, '26353', '2018-06-06'),
(531, '16095', '2018-06-06'),
(532, '1996', '2018-06-06'),
(533, '29398', '2018-06-06'),
(534, '15964', '2018-06-06'),
(535, '10773', '2018-06-06'),
(536, '18775', '2018-06-06'),
(537, '298', '2018-06-06'),
(538, '11198', '2018-06-06'),
(539, '8144', '2018-06-06'),
(540, '26889', '2018-06-06'),
(541, '13343', '2018-06-06'),
(542, '27716', '2018-06-06'),
(543, '26412', '2018-06-06'),
(544, '17765', '2018-06-06'),
(545, '570', '2018-06-06'),
(546, '301', '2018-06-06'),
(547, '26210', '2018-06-06'),
(548, '1525', '2018-06-06'),
(549, '9998', '2018-06-06'),
(550, '20203', '2018-06-06'),
(551, '21356', '2018-06-06'),
(552, '7477', '2018-06-06'),
(553, '24458', '2018-06-06'),
(554, '7113', '2018-06-06'),
(555, '17866', '2018-06-06'),
(556, '17102', '2018-06-06'),
(557, '9203', '2018-06-06'),
(558, '25672', '2018-06-06'),
(559, '31515', '2018-06-06'),
(560, '29499', '2018-06-06'),
(561, '14936', '2018-06-06'),
(562, '18968', '2018-06-06'),
(563, '31988', '2018-06-06'),
(564, '22449', '2018-06-06'),
(565, '5423', '2018-06-06'),
(566, '1852', '2018-06-06'),
(567, '26876', '2018-06-06'),
(568, '15389', '2018-06-06'),
(569, '29846', '2018-06-06'),
(570, '2071', '2018-06-06'),
(571, '9220', '2018-06-06'),
(572, '28612', '2018-06-06'),
(573, '2224', '2018-06-06'),
(574, '11141', '2018-06-06'),
(575, '21688', '2018-06-06'),
(576, '15834', '2018-06-06'),
(577, '29195', '2018-06-06'),
(578, '809', '2018-06-06'),
(579, '1710', '2018-06-06'),
(580, '21137', '2018-06-06'),
(581, '3151', '2018-06-06'),
(582, '32429', '2018-06-06'),
(583, '6376', '2018-06-06'),
(584, '15853', '2018-06-06'),
(585, '17442', '2018-06-06'),
(586, '31489', '2018-06-06'),
(587, '7826', '2018-06-06'),
(588, '3763', '2018-06-06'),
(589, '21986', '2018-06-06'),
(590, '31856', '2018-06-06'),
(591, '3269', '2018-06-06'),
(592, '23974', '2018-06-06'),
(593, '12387', '2018-06-06'),
(594, '19433', '2018-06-06'),
(595, '9754', '2018-06-06'),
(596, '110', '2018-06-06'),
(597, '9191', '2018-06-06'),
(598, '25359', '2018-06-06'),
(599, '23444', '2018-06-07'),
(600, '17981', '2018-06-07'),
(601, '1526', '2018-06-07'),
(602, '7539', '2018-06-07'),
(603, '29792', '2018-06-07'),
(604, '11826', '2018-06-07'),
(605, '1124', '2018-06-07'),
(606, '2361', '2018-06-07'),
(607, '1741', '2018-06-07'),
(608, '14637', '2018-06-07'),
(609, '15781', '2018-06-07'),
(610, '31614', '2018-06-07'),
(611, '13690', '2018-06-07'),
(612, '15970', '2018-06-07'),
(613, '2946', '2018-06-07'),
(614, '10834', '2018-06-07'),
(615, '31578', '2018-06-07'),
(616, '18652', '2018-06-07'),
(617, '32444', '2018-06-07'),
(618, '23770', '2018-06-07'),
(619, '9483', '2018-06-07'),
(620, '20456', '2018-06-07'),
(621, '32497', '2018-06-07'),
(622, '26113', '2018-06-07'),
(623, '10966', '2018-06-07'),
(624, '8535', '2018-06-07'),
(625, '27814', '2018-06-07'),
(626, '28229', '2018-06-07'),
(627, '16578', '2018-06-07'),
(628, '3431', '2018-06-07'),
(629, '18196', '2018-06-07'),
(630, '3005', '2018-06-07'),
(631, '24275', '2018-06-07'),
(632, '15464', '2018-06-07'),
(633, '10650', '2018-06-07'),
(634, '9998', '2018-06-07'),
(635, '5423', '2018-06-07'),
(636, '1852', '2018-06-07'),
(637, '27606', '2018-06-07'),
(638, '27573', '2018-06-07'),
(639, '15201', '2018-06-07'),
(640, '9754', '2018-06-07'),
(641, '18753', '2018-06-07'),
(642, '12006', '2018-06-07'),
(643, '10689', '2018-06-07'),
(644, '24893', '2018-06-07'),
(645, '3128', '2018-06-07'),
(646, '15845', '2018-06-07'),
(647, '20055', '2018-06-07'),
(648, '11570', '2018-06-07'),
(649, '3396', '2018-06-07'),
(650, '3629', '2018-06-07'),
(651, '341', '2018-06-07'),
(652, '15399', '2018-06-07'),
(653, '11988', '2018-06-07'),
(654, '10090', '2018-06-07'),
(655, '24436', '2018-06-07'),
(656, '17499', '2018-06-07'),
(657, '10883', '2018-06-07'),
(658, '28514', '2018-06-07'),
(659, '1272', '2018-06-07'),
(660, '13312', '2018-06-07'),
(661, '24017', '2018-06-07'),
(662, '20534', '2018-06-07'),
(663, '21561', '2018-06-07'),
(664, '3724', '2018-06-07'),
(665, '27262', '2018-06-07'),
(666, '11421', '2018-06-07'),
(667, '26741', '2018-06-07'),
(668, '2517', '2018-06-07'),
(669, '27615', '2018-06-07'),
(670, '20490', '2018-06-07'),
(671, '30963', '2018-06-07'),
(672, '1298', '2018-06-07'),
(673, '17898', '2018-06-07'),
(674, '16603', '2018-06-07'),
(675, '14768', '2018-06-07'),
(676, '4140', '2018-06-07'),
(677, '19255', '2018-06-07'),
(678, '24617', '2018-06-07'),
(679, '30884', '2018-06-07'),
(680, '8110', '2018-06-07'),
(681, '21731', '2018-06-07'),
(682, '12623', '2018-06-07'),
(683, '1912', '2018-06-07'),
(684, '1549', '2018-06-07'),
(685, '16604', '2018-06-07'),
(686, '28241', '2018-06-07'),
(687, '22198', '2018-06-07'),
(688, '32480', '2018-06-07'),
(689, '14786', '2018-06-07'),
(690, '14461', '2018-06-07'),
(691, '25555', '2018-06-07'),
(692, '21362', '2018-06-07'),
(693, '21989', '2018-06-07'),
(694, '2234', '2018-06-07'),
(695, '29635', '2018-06-07'),
(696, '20603', '2018-06-07'),
(697, '30105', '2018-06-07'),
(698, '4971', '2018-06-07'),
(699, '28312', '2018-06-07'),
(700, '24670', '2018-06-07'),
(701, '31477', '2018-06-07'),
(702, '4023', '2018-06-07'),
(703, '3784', '2018-06-07'),
(704, '12426', '2018-06-07'),
(705, '26363', '2018-06-07'),
(706, '27489', '2018-06-07'),
(707, '25664', '2018-06-07'),
(708, '23615', '2018-06-07'),
(709, '24441', '2018-06-07'),
(710, '5873', '2018-06-07'),
(711, '792', '2018-06-07'),
(712, '10508', '2018-06-07'),
(713, '13502', '2018-06-07'),
(714, '2335', '2018-06-07'),
(715, '1132', '2018-06-07'),
(716, '21077', '2018-06-07'),
(717, '20849', '2018-06-07'),
(718, '9322', '2018-06-07'),
(719, '25302', '2018-06-07'),
(720, '8996', '2018-06-07'),
(721, '19286', '2018-06-07'),
(722, '32091', '2018-06-07'),
(723, '31504', '2018-06-07'),
(724, '30342', '2018-06-07'),
(725, '7821', '2018-06-07'),
(726, '31559', '2018-06-07'),
(727, '31285', '2018-06-07'),
(728, '31063', '2018-06-07'),
(729, '24266', '2018-06-07'),
(730, '23620', '2018-06-07'),
(731, '30679', '2018-06-07'),
(732, '18936', '2018-06-07'),
(733, '10306', '2018-06-07'),
(734, '14532', '2018-06-07'),
(735, '6203', '2018-06-07'),
(736, '31576', '2018-06-07'),
(737, '16081', '2018-06-07'),
(738, '5037', '2018-06-07'),
(739, '18146', '2018-06-07'),
(740, '14963', '2018-06-07'),
(741, '28755', '2018-06-07'),
(742, '20340', '2018-06-07'),
(743, '19856', '2018-06-07'),
(744, '12589', '2018-06-07'),
(745, '15785', '2018-06-07'),
(746, '30217', '2018-06-07'),
(747, '5769', '2018-06-07'),
(748, '1201', '2018-06-07'),
(749, '1678', '2018-06-07'),
(750, '16047', '2018-06-07'),
(751, '12988', '2018-06-07'),
(752, '21491', '2018-06-07'),
(753, '5175', '2018-06-07'),
(754, '28462', '2018-06-07'),
(755, '4424', '2018-06-07'),
(756, '25022', '2018-06-07'),
(757, '12831', '2018-06-07'),
(758, '14250', '2018-06-07'),
(759, '31713', '2018-06-07'),
(760, '12088', '2018-06-07'),
(761, '23313', '2018-06-07'),
(762, '4170', '2018-06-07'),
(763, '9334', '2018-06-07'),
(764, '32006', '2018-06-07'),
(765, '19253', '2018-06-07'),
(766, '10871', '2018-06-07'),
(767, '1073', '2018-06-07'),
(768, '30483', '2018-06-07'),
(769, '11600', '2018-06-07'),
(770, '23498', '2018-06-07'),
(771, '29981', '2018-06-07'),
(772, '23369', '2018-06-07'),
(773, '26390', '2018-06-07'),
(774, '1441', '2018-06-07'),
(775, '18583', '2018-06-07'),
(776, '25490', '2018-06-07'),
(777, '8299', '2018-06-07'),
(778, '26482', '2018-06-08'),
(779, '30659', '2018-06-08'),
(780, '6409', '2018-06-08'),
(781, '9415', '2018-06-08'),
(782, '30524', '2018-06-08'),
(783, '15557', '2018-06-08'),
(784, '8396', '2018-06-08'),
(785, '3991', '2018-06-08'),
(786, '24930', '2018-06-08'),
(787, '10130', '2018-06-08'),
(788, '21892', '2018-06-08'),
(789, '20845', '2018-06-08'),
(790, '8832', '2018-06-08'),
(791, '9568', '2018-06-08'),
(792, '10267', '2018-06-08'),
(793, '32184', '2018-06-08'),
(794, '15614', '2018-06-08'),
(795, '755', '2018-06-08'),
(796, '1355', '2018-06-08'),
(797, '18321', '2018-06-08'),
(798, '22262', '2018-06-08'),
(799, '31664', '2018-06-08'),
(800, '20341', '2018-06-08'),
(801, '10999', '2018-06-08'),
(802, '31273', '2018-06-08'),
(803, '11018', '2018-06-08'),
(804, '3963', '2018-06-08'),
(805, '23190', '2018-06-08'),
(806, '12718', '2018-06-08'),
(807, '16792', '2018-06-08'),
(808, '32469', '2018-06-08'),
(809, '8924', '2018-06-08'),
(810, '28145', '2018-06-08'),
(811, '11047', '2018-06-08'),
(812, '30231', '2018-06-08'),
(813, '4069', '2018-06-08'),
(814, '15716', '2018-06-08'),
(815, '31236', '2018-06-08'),
(816, '23533', '2018-06-08'),
(817, '19405', '2018-06-08'),
(818, '2594', '2018-06-08'),
(819, '15835', '2018-06-08'),
(820, '18921', '2018-06-08'),
(821, '4514', '2018-06-08'),
(822, '27778', '2018-06-08'),
(823, '3635', '2018-06-08'),
(824, '15034', '2018-06-08'),
(825, '23915', '2018-06-08'),
(826, '32659', '2018-06-08'),
(827, '9838', '2018-06-08'),
(828, '4296', '2018-06-08'),
(829, '17761', '2018-06-08'),
(830, '16253', '2018-06-08'),
(831, '20082', '2018-06-08'),
(832, '19408', '2018-06-08'),
(833, '4367', '2018-06-08'),
(834, '22473', '2018-06-08'),
(835, '21187', '2018-06-08'),
(836, '20174', '2018-06-08'),
(837, '495', '2018-06-08'),
(838, '21756', '2018-06-08'),
(839, '10117', '2018-06-08'),
(840, '16504', '2018-06-08'),
(841, '17904', '2018-06-08'),
(842, '5993', '2018-06-08'),
(843, '22336', '2018-06-08'),
(844, '18566', '2018-06-08'),
(845, '2522', '2018-06-08'),
(846, '1436', '2018-06-08'),
(847, '13137', '2018-06-08'),
(848, '14262', '2018-06-08'),
(849, '19979', '2018-06-08'),
(850, '23735', '2018-06-08'),
(851, '27684', '2018-06-08'),
(852, '7077', '2018-06-08'),
(853, '13197', '2018-06-08'),
(854, '28118', '2018-06-08'),
(855, '9710', '2018-06-08'),
(856, '23183', '2018-06-08'),
(857, '17640', '2018-06-08'),
(858, '10327', '2018-06-08'),
(859, '20764', '2018-06-08'),
(860, '14081', '2018-06-08'),
(861, '6466', '2018-06-08'),
(862, '15226', '2018-06-08'),
(863, '27046', '2018-06-08'),
(864, '3411', '2018-06-08'),
(865, '9872', '2018-06-08'),
(866, '12075', '2018-06-08'),
(867, '16359', '2018-06-08'),
(868, '18324', '2018-06-08'),
(869, '24456', '2018-06-08'),
(870, '29044', '2018-06-08'),
(871, '22173', '2018-06-08'),
(872, '26386', '2018-06-08'),
(873, '16957', '2018-06-08'),
(874, '24388', '2018-06-08'),
(875, '31282', '2018-06-08'),
(876, '19745', '2018-06-08'),
(877, '5446', '2018-06-08'),
(878, '16677', '2018-06-08'),
(879, '29571', '2018-06-08'),
(880, '22137', '2018-06-08'),
(881, '3811', '2018-06-08'),
(882, '4871', '2018-06-08'),
(883, '18688', '2018-06-08'),
(884, '17721', '2018-06-08'),
(885, '12512', '2018-06-08'),
(886, '16281', '2018-06-08'),
(887, '13050', '2018-06-08'),
(888, '4267', '2018-06-08'),
(889, '24510', '2018-06-08'),
(890, '30765', '2018-06-08'),
(891, '23222', '2018-06-09'),
(892, '4336', '2018-06-09'),
(893, '1215', '2018-06-09'),
(894, '2817', '2018-06-09'),
(895, '26508', '2018-06-09'),
(896, '17320', '2018-06-09'),
(897, '28373', '2018-06-09'),
(898, '7058', '2018-06-09'),
(899, '11686', '2018-06-09'),
(900, '18154', '2018-06-09'),
(901, '26979', '2018-06-09'),
(902, '32146', '2018-06-09'),
(903, '25802', '2018-06-09'),
(904, '29256', '2018-06-09'),
(905, '22569', '2018-06-09'),
(906, '14254', '2018-06-09'),
(907, '9059', '2018-06-09'),
(908, '1641', '2018-06-09'),
(909, '18657', '2018-06-09'),
(910, '17335', '2018-06-09'),
(911, '26862', '2018-06-09'),
(912, '6364', '2018-06-09'),
(913, '29325', '2018-06-09'),
(914, '25683', '2018-06-09'),
(915, '27488', '2018-06-09'),
(916, '21703', '2018-06-09'),
(917, '12688', '2018-06-09'),
(918, '24975', '2018-06-09'),
(919, '10265', '2018-06-09'),
(920, '9787', '2018-06-09'),
(921, '24798', '2018-06-09'),
(922, '4799', '2018-06-09'),
(923, '27380', '2018-06-09'),
(924, '4637', '2018-06-09'),
(925, '13287', '2018-06-09'),
(926, '29016', '2018-06-09'),
(927, '11156', '2018-06-09'),
(928, '8378', '2018-06-09'),
(929, '17073', '2018-06-09'),
(930, '11477', '2018-06-09'),
(931, '19204', '2018-06-09'),
(932, '22077', '2018-06-09'),
(933, '19307', '2018-06-09'),
(934, '11498', '2018-06-09'),
(935, '26312', '2018-06-09'),
(936, '11997', '2018-06-09'),
(937, '32306', '2018-06-09'),
(938, '22765', '2018-06-09'),
(939, '2702', '2018-06-09'),
(940, '17186', '2018-06-09'),
(941, '16675', '2018-06-09'),
(942, '15506', '2018-06-09'),
(943, '30307', '2018-06-09'),
(944, '29359', '2018-06-09'),
(945, '22204', '2018-06-09'),
(946, '25441', '2018-06-09'),
(947, '5664', '2018-06-09'),
(948, '23513', '2018-06-09'),
(949, '13817', '2018-06-09'),
(950, '21555', '2018-06-09'),
(951, '16973', '2018-06-09'),
(952, '13217', '2018-06-09'),
(953, '11730', '2018-06-09'),
(954, '14790', '2018-06-09'),
(955, '9596', '2018-06-09'),
(956, '19817', '2018-06-09'),
(957, '9198', '2018-06-09'),
(958, '143', '2018-06-09'),
(959, '27989', '2018-06-09'),
(960, '24839', '2018-06-09'),
(961, '8756', '2018-06-09'),
(962, '13728', '2018-06-09'),
(963, '17753', '2018-06-09'),
(964, '4187', '2018-06-09'),
(965, '14728', '2018-06-09'),
(966, '2366', '2018-06-09'),
(967, '927', '2018-06-09'),
(968, '18895', '2018-06-09'),
(969, '32582', '2018-06-09'),
(970, '11346', '2018-06-09'),
(971, '13575', '2018-06-09'),
(972, '26272', '2018-06-09'),
(973, '23354', '2018-06-09'),
(974, '6176', '2018-06-09'),
(975, '17094', '2018-06-09'),
(976, '29319', '2018-06-09'),
(977, '8031', '2018-06-09'),
(978, '13291', '2018-06-09'),
(979, '13785', '2018-06-09'),
(980, '14580', '2018-06-10'),
(981, '20130', '2018-06-10'),
(982, '18461', '2018-06-10'),
(983, '14994', '2018-06-10'),
(984, '18097', '2018-06-10'),
(985, '29713', '2018-06-10'),
(986, '14230', '2018-06-10'),
(987, '3863', '2018-06-10'),
(988, '27762', '2018-06-10'),
(989, '27404', '2018-06-10'),
(990, '3466', '2018-06-10'),
(991, '27733', '2018-06-10'),
(992, '7267', '2018-06-10'),
(993, '28768', '2018-06-10'),
(994, '6339', '2018-06-10'),
(995, '1843', '2018-06-10'),
(996, '19184', '2018-06-10'),
(997, '18806', '2018-06-10'),
(998, '32763', '2018-06-10'),
(999, '27511', '2018-06-10'),
(1000, '10264', '2018-06-10'),
(1001, '4721', '2018-06-10'),
(1002, '13930', '2018-06-10'),
(1003, '9536', '2018-06-10'),
(1004, '13284', '2018-06-10'),
(1005, '18713', '2018-06-10'),
(1006, '16461', '2018-06-10'),
(1007, '30806', '2018-06-10'),
(1008, '1794', '2018-06-10'),
(1009, '28126', '2018-06-10'),
(1010, '8407', '2018-06-10'),
(1011, '10075', '2018-06-10'),
(1012, '3193', '2018-06-10'),
(1013, '23998', '2018-06-10'),
(1014, '7103', '2018-06-10'),
(1015, '12940', '2018-06-10'),
(1016, '24021', '2018-06-10'),
(1017, '18451', '2018-06-10'),
(1018, '10832', '2018-06-10'),
(1019, '14441', '2018-06-10'),
(1020, '32287', '2018-06-10'),
(1021, '11716', '2018-06-10'),
(1022, '13038', '2018-06-10'),
(1023, '5193', '2018-06-10'),
(1024, '25773', '2018-06-10'),
(1025, '7940', '2018-06-10'),
(1026, '11256', '2018-06-10'),
(1027, '25487', '2018-06-10'),
(1028, '9756', '2018-06-10'),
(1029, '7405', '2018-06-10'),
(1030, '4430', '2018-06-10'),
(1031, '14114', '2018-06-10'),
(1032, '30929', '2018-06-10'),
(1033, '20278', '2018-06-10'),
(1034, '24119', '2018-06-10'),
(1035, '17378', '2018-06-10'),
(1036, '4004', '2018-06-10'),
(1037, '1459', '2018-06-10'),
(1038, '20749', '2018-06-10'),
(1039, '21103', '2018-06-10'),
(1040, '30517', '2018-06-10'),
(1041, '29555', '2018-06-10'),
(1042, '10096', '2018-06-10'),
(1043, '6889', '2018-06-10'),
(1044, '23867', '2018-06-10'),
(1045, '7428', '2018-06-10'),
(1046, '7036', '2018-06-10'),
(1047, '17133', '2018-06-10'),
(1048, '9972', '2018-06-10'),
(1049, '20658', '2018-06-10'),
(1050, '18435', '2018-06-10'),
(1051, '25890', '2018-06-10'),
(1052, '7709', '2018-06-10'),
(1053, '9070', '2018-06-10'),
(1054, '7091', '2018-06-10'),
(1055, '23640', '2018-06-10'),
(1056, '11269', '2018-06-10'),
(1057, '18959', '2018-06-10'),
(1058, '10705', '2018-06-10'),
(1059, '1392', '2018-06-10'),
(1060, '17280', '2018-06-10'),
(1061, '8425', '2018-06-10'),
(1062, '12654', '2018-06-10'),
(1063, '22674', '2018-06-10'),
(1064, '25187', '2018-06-10'),
(1065, '29786', '2018-06-10'),
(1066, '9867', '2018-06-10'),
(1067, '19100', '2018-06-10'),
(1068, '15968', '2018-06-10'),
(1069, '16399', '2018-06-10'),
(1070, '11318', '2018-06-10'),
(1071, '28089', '2018-06-10'),
(1072, '2204', '2018-06-10'),
(1073, '29544', '2018-06-10'),
(1074, '25253', '2018-06-10'),
(1075, '30519', '2018-06-10'),
(1076, '13476', '2018-06-10'),
(1077, '897', '2018-06-10'),
(1078, '31782', '2018-06-10'),
(1079, '31909', '2018-06-10'),
(1080, '17918', '2018-06-10'),
(1081, '2170', '2018-06-10'),
(1082, '24927', '2018-06-10'),
(1083, '4621', '2018-06-10'),
(1084, '20913', '2018-06-10'),
(1085, '24084', '2018-06-10'),
(1086, '22205', '2018-06-10'),
(1087, '20011', '2018-06-10'),
(1088, '30763', '2018-06-10'),
(1089, '11381', '2018-06-10'),
(1090, '32537', '2018-06-10'),
(1091, '21954', '2018-06-10'),
(1092, '13490', '2018-06-10'),
(1093, '27614', '2018-06-10'),
(1094, '23555', '2018-06-10'),
(1095, '18304', '2018-06-10');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autonomo`
--
ALTER TABLE `autonomo`
  ADD PRIMARY KEY (`dni`,`id_user`);

--
-- Indices de la tabla `best_freelancer`
--
ALTER TABLE `best_freelancer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`dni`,`id_user`);

--
-- Indices de la tabla `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `forum_likes`
--
ALTER TABLE `forum_likes`
  ADD PRIMARY KEY (`id_like`);

--
-- Indices de la tabla `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_register`
--
ALTER TABLE `user_register`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `value_freelancer`
--
ALTER TABLE `value_freelancer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `value_website`
--
ALTER TABLE `value_website`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `best_freelancer`
--
ALTER TABLE `best_freelancer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT de la tabla `forum_likes`
--
ALTER TABLE `forum_likes`
  MODIFY `id_like` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `value_freelancer`
--
ALTER TABLE `value_freelancer`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1096;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
