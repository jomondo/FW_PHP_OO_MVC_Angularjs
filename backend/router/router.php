<?php
require_once("paths.php");
require 'autoload.php';

include(UTILS_PATH . "utils.inc.php");
include(UTILS . "response_code.inc.php");
include(UTILS . "common.inc.php");
include(UTILS . "email.inc.php");
include(UTILS . "validatePHP.php");
include(UTILS . "functions.inc.php");
//print_r("router");
//exit;
if (PRODUCTION) { //estamos en producción
    ini_set('display_errors', '1');
    ini_set('error_reporting', E_ERROR | E_WARNING); //error_reporting(E_ALL) ;
    //error_reporting(E_ALL) ; | E_NOTICE --> commit E_NOTICE to use timeout userdao_country
} else {
    ini_set('display_errors', '0');
    ini_set('error_reporting', '0'); //error_reporting(0); 
}

ob_start(); 
session_start();

function handlerRouter() {
    //print_r("handlerRouter");
    //exit;
    if (!empty($_GET['module'])) {
        //print_r("module");
        //exit;
        $URI_module = $_GET['module'];
    } else {
        //print_r("no exist module");
        //exit;
        die('<script>window.location.href="https://localhost/freelancer/home/"</script>');

    }

    if (!empty($_GET['function'])) {
        //$cont++;
        //print_r($cont);
        //print_r($_GET['function']);
        //exit;
        $URI_function = $_GET['function'];
    } else {
        $URI_function = 'begin';
    }
    handlerModule($URI_module, $URI_function);
}

function handlerModule($URI_module, $URI_function) {
    $modules = simplexml_load_file('resources/modules.xml');
    $exist = false;
    //print_r("handlerModule");
    //exit;
    foreach ($modules->module as $module) {
        if (($URI_module === (String) $module->uri)) {
            //print_r("URI_module");
            //exit;
            $exist = true;

            $path = MODULE_PATH . $URI_module . "/controller/controller_" . $URI_module . ".class.php";
            if (file_exists($path)) {
                //print_r($path);
                //exit;
                $path_utils = MODULE_PATH . $URI_module . "/utils/functions_" . $URI_module . ".php";
                include_once($path);//controller

                if (file_exists($path_utils)) {
                    include_once($path_utils);

                }
                
                $controllerClass = "controller_" . $URI_module;
                $obj = new $controllerClass;

                ////////////////
                handlerfunction(((String) $module->name), $obj, $URI_function);
                break;

            }else {
                if (PRODUCTION) {
                    showErrorPage(0, "ERROR - 400 not found controller", 'HTTP/1.0 400 Bad Request', 400);

                }else {
                    handlerModule('home', 'begin');

                }

            }
            
        }
    }
    if (!$exist) {
        if (PRODUCTION) {
            showErrorPage(0, "ERROR - 400 not found module", 'HTTP/1.0 400 Bad Request', 400);
                    
        }else {
            handlerModule('home', 'begin');

        }

    }
}

function handlerFunction($module, $obj, $URI_function) {
    $functions = simplexml_load_file(MODULE_PATH . $module . "/resources/functions.xml");
    $exist = false;

    foreach ($functions->function as $function) {
        if (($URI_function === (String) $function->uri)) {
            $exist = true;
            $event = (String) $function->name;
            break;
        }
    }
    if (!$exist) {
        if (PRODUCTION) {
            showErrorPage(0, "ERROR - 400 not found function", 'HTTP/1.0 400 Bad Request', 400);
                    
        }else {
            handlerModule('home', 'begin');

        }

    } else {
        //$obj->$event();
        call_user_func(array($obj, $event));
    }
}

handlerRouter();
ob_flush();
