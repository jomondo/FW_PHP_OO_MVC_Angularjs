<?php
	class controller_contact{
		function __construct(){
				
		}

		function begin(){
			loadView("contact","contact");
		}

		function process_consulting(){
			$dataJSON = json_decode($_POST["data"], true);
			//echo json_encode($dataJSON);
			//exit;
			if (isset($dataJSON['token'])=="contact_form") {
				/* envio email user */
				$arrValues = array(
					'type' => 'contact',
					'name' => $dataJSON['inputName'],
					'email' => $dataJSON['inputEmail'],
					'subject' => $dataJSON['inputSubject'],
					'mgs' => $dataJSON['inputMessage']
				);

				try{
                	$email_cli = send_email($arrValues);

				}catch (Exception $e) {
					showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);

				}
				//echo json_encode($email_cli);
				//exit;
				/* envio email admin */
				$arrValues = array(
					'type' => 'admin',
					'name' => $dataJSON['inputName'],
					'email' => $dataJSON['inputEmail'],
					'subject' => $dataJSON['inputSubject'],
					'mgs' => $dataJSON['inputMessage']
				);

				try{
					sleep(5);
                	$email_adm = send_email($arrValues);
                	//echo "TRUE|";
				}catch (Exception $e) {
					showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);

				}

				if ($email_cli && $email_adm) {
					echo "TRUE";
				}else{
					echo "FALSE";
				}

			}else{
				showErrorPage(0, "ERROR - 400 not found data", 'HTTP/1.0 400 Bad Request', 400);

			}
		}

	}