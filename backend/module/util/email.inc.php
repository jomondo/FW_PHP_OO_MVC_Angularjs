<?php
    function send_email($arr) {
        $html = '';
        $subject = '';
        $body = '';
        $ruta = '';
        $return = '';
        //echo json_encode($arr);
        //exit;
        // amigable1("?module=login&function=verify&aux=" . $arr['token'], true)
        //amigable1("?module=login&function=verify_change_passwd&aux=" . $arr['token'], true)
        switch ($arr['type']) {
            case 'alta':
                $subject = 'Tu Alta en Freelancer';
                $ruta = "<a href='https://localhost/freelancer/#/user/activar/" . $arr['token'] . "'>aqu&iacute;</a>";
                $body = 'Gracias por unirte a nuestra aplicaci&oacute;n<br> Para finalizar el registro, pulsa ' . $ruta;
                break;
    
            case 'modificacion':
                $subject = 'Tu Nuevo Password en Freelancer<br>';
                $ruta = '<a href="https://localhost/freelancer/#/user/changePassword/'. $arr['token'] .'">aqu&iacute;</a>';
                $body = 'Para cambiar tu password pulsa ' . $ruta;
                break;
                
            case 'contact':
                $subject = 'Tu Peticion a freelancer ha sido enviada';
                $ruta = '<a href=' . 'https://localhost/freelancer/home/'. '>aqu&iacute;</a>';
                $body = 'Su consulta: <p>'.$arr['mgs'].'</p><br>Gracias por consultarnos. En breve le responderemos.<br> Para visitar nuestra web, pulsa ' . $ruta;
                break;
    
            case 'admin':
                $subject = $arr['subject'];
                $body = 'name: ' . $arr['name']. '<br>' .
                'email: ' . $arr['email']. '<br>' .
                'subject: ' . $arr['subject']. '<br>' .
                'mgs: ' . $arr['mgs'];
                break;
        }
        
        $html .= "<html>";
        $html .= "<body>";
	       $html .= "<h4>". $subject ."</h4>";
	       $html .= $body;
	       $html .= "<br><br>";
	       $html .= "<p>Sent by freelancer</p>";
		$html .= "</body>";
		$html .= "</html>";

        //set_error_handler('ErrorHandler');
        try{
            if ($arr['type'] === 'admin')
                $address = 'joanmodaw@gmail.com';//joandeveloperweb@gmail.com (admin)
            else if($arr['type'] === 'contact')
                $address = $arr['email'];
            else if($arr['type'] === 'alta')
                $address = $arr['inputEmail'];
            else if($arr['type'] === 'modificacion')
                $address = $arr['inputEmail'];

            $result = send_mailgun('joandeveloperweb@gmail.com', $address, $subject, $html);    
        } catch (Exception $e) {
			$return = 0;
		}
		//restore_error_handler();
        return $result;
    }
    
    function send_mailgun($from, $to, $subject, $html){
        $config = array();
        $config['api_key'] = "key-a788c1ea902012296c863d751532b2cc"; //API Key
        $config['api_url'] = "https://api.mailgun.net/v3/sandbox7602616a189143929b4b41eaa3b6aef4.mailgun.org/messages"; //API Base URL
    
        $message = array();
        $message['from'] = $from;
        $message['to'] = $to;
        $message['h:Reply-To'] = "joandeveloperweb@gmail.com";
        $message['subject'] = $subject;
        $message['html'] = $html;
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $config['api_url']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }
