<?php
    class login_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = login_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function count($arrArgument) {
            return $this->bll->count_BLL($arrArgument);
        }

        public function count_activate($arrArgument) {
            return $this->bll->count_activate_BLL($arrArgument);
        }

        public function create_user($arrArgument) {
            return $this->bll->create_user_BLL($arrArgument);
        }

        public function select_freelancer($arrArgument) {
            return $this->bll->select_freelancer_BLL($arrArgument);
        }

        public function update_activate($arrArgument) {
            return $this->bll->update_activate_BLL($arrArgument);
        }

        public function update_token($arrArgument) {
            return $this->bll->update_token_BLL($arrArgument);
        }

        public function select_user_token($arrArgument) {
            return $this->bll->select_user_token_BLL($arrArgument);
        }

        public function check_email($arrArgument) {
            return $this->bll->check_email_BLL($arrArgument);
        }

        public function check_token($arrArgument) {
            return $this->bll->check_token_BLL($arrArgument);
        }

        public function updt_tokn_chge_passwd($arrArgument) {
            return $this->bll->updt_tokn_chge_passwd_BLL($arrArgument);
        }

        public function update_passwd($arrArgument) {
            return $this->bll->update_passwd_BLL($arrArgument);
        }

        public function check_lock($arrArgument) {
            return $this->bll->check_lock_BLL($arrArgument);
        }


    }