<?php
    class login_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = login_dao::getInstance();
            $this->db = db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function count_BLL($arrArgument) {
            return $this->dao->count_DAO($this->db, $arrArgument);
        }

        public function count_activate_BLL($arrArgument) {
            return $this->dao->count_activate_DAO($this->db, $arrArgument);
        }

        public function create_user_BLL($arrArgument) {
            return $this->dao->create_user_DAO($this->db, $arrArgument);
        }

        public function select_freelancer_BLL($arrArgument) {
            return $this->dao->select_freelancer_DAO($this->db, $arrArgument);
        }

        public function update_activate_BLL($arrArgument) {
            return $this->dao->update_activate_DAO($this->db, $arrArgument);
        }

        public function update_token_BLL($arrArgument) {
            return $this->dao->update_token_DAO($this->db, $arrArgument);
        }

        public function select_user_token_BLL($arrArgument) {
            return $this->dao->select_user_token_DAO($this->db, $arrArgument);
        }

        public function check_email_BLL($arrArgument) {
            return $this->dao->check_email_DAO($this->db, $arrArgument);
        }

        public function check_token_BLL($arrArgument) {
            return $this->dao->check_token_DAO($this->db, $arrArgument);
        }

        public function updt_tokn_chge_passwd_BLL($arrArgument) {
            return $this->dao->updt_tokn_chge_passwd_DAO($this->db, $arrArgument);
        }

        public function update_passwd_BLL($arrArgument) {
            return $this->dao->update_passwd_DAO($this->db, $arrArgument);
        }

        public function check_lock_BLL($arrArgument) {
            return $this->dao->check_lock_DAO($this->db, $arrArgument);
        }

        
    }