<?php
class login_dao {
    static $_instance;

    private function __construct() {
        
    }
// var data = {"id":json[0].id_user,"name":json[0].name, "email":json[0].email,"avatar":json[0].avatar, "type":json[0].type};
    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function count_DAO($db, $value) {

        $sql = "SELECT count(*) as count FROM user_register WHERE id_user = '".$value['id']."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function count_activate_DAO($db, $value) {

        $sql = "SELECT count(*) as count FROM user_register WHERE id_user = '".$value['id']."' and activate=1";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function check_lock_DAO($db, $value) {

        $sql = "SELECT count(*) as count FROM user_register WHERE id_user = '".$value['id']."' and activate=2";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function create_user_DAO($db, $value) {

        $sql = "INSERT INTO user_register ( id_user, name, passwd, email, avatar, type, token, date, activate ) VALUES ('$value[id]', '$value[name]', '$value[passwd]', '$value[email]','$value[avatar]', '$value[type]', '$value[token]', now(), '$value[activate]' )";
            
        return $db->ejecutar($sql);
        
    }

    public function select_freelancer_DAO($db, $id) {

        $sql = "SELECT * FROM user_register WHERE id_user = '".$id."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function update_activate_DAO($db, $value) {

        $sql = "UPDATE user_register SET activate = 1 WHERE token = '".$value['token']."'";
            
        return $db->ejecutar($sql);
        
    }

    public function update_token_DAO($db, $value) {

        $sql = "UPDATE user_register SET token = '".$value['token']."' WHERE id_user = '".$value['id']."'";
            
        return $db->ejecutar($sql);
        
    }

    public function select_user_token_DAO($db, $token) {
        //print_r($token);
        //exit;
        $sql = "SELECT * FROM user_register WHERE token = '".$token."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function check_email_DAO($db, $email) {
        
        $sql = "SELECT count(*) as count FROM user_register WHERE email = '".$email."' and passwd!='' ";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function updt_tokn_chge_passwd_DAO($db, $value) {

        $sql = "UPDATE user_register SET token = '".$value['token']."' WHERE email = '".$value['email']."' and passwd!=''";
            
        return $db->ejecutar($sql);
        
    }

    public function update_passwd_DAO($db, $value) {
        //print_r($value);
        //exit;

        $sql = "UPDATE user_register SET passwd = '".$value['password']."' WHERE token = '".$value['token']."' and passwd!=''";
            
        return $db->ejecutar($sql);
        
    }

    public function check_token_DAO($db, $value) {

        $sql = "SELECT count(*) as count FROM user_register WHERE token = '".$value['token']."' and activate=1 and passwd!='' ";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

}
