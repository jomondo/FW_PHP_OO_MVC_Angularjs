$('document').ready(function() {
	$(document).on('click','#btn_submit_login',function(){
		var rdo = true;
        
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var passwdreg = /^[A-Za-z0-9ñÑ]{5,20}$/;
            
        var name = $("#name_login").val();
        var passwd = $("#passwd_login").val();
        
        if( name == ""){
            var message = "Introduce el nombre";
            amaran(message);
            rdo=false;
            return false;
        }else if (!namereg.test(name)){
            var message = "El nombre tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( passwd == ""){
            var message = "Introduce su password";
            amaran(message);
            rdo=false;
            return false;
        }else if (!passwdreg.test(passwd)){
            var message = "El password permite alfanumerico de 5-20 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }

        if (rdo) {
            var data = { "id":name, "password":passwd };
            var data_login_JSON = JSON.stringify(data);
            console.log("json debug: "+data_login_JSON);

            $.post(amigable('?module=login&function=login'),{alta_login_json: data_login_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);

                if (json.success) {
                    var payload = {"id":json.user[0].id_user};
                    var jwt = create_jwt(payload);
                    var data = {"id":json.user[0].id_user, "token":jwt};
                    var token = JSON.stringify(data);
                    
                    load_data_ajax("?module=login&function=update_token","POST",token,jwt,"update_token_manual");
                    /// /////////////////////////////////////
                   /* $.ajax({
                      url: amigable('?module=login&function=update_token'),
                      type: 'POST',
                      data:{data:token},
                      success:function(response){
                        alert(response);
                        var json = JSON.parse(response);
                        if (json.success) {
                          alert("user updated");
                          var tokenInit = {"token":jwt, "log_man":true};
                          var tokenToInit = JSON.stringify(tokenInit);
                          localStorage.setItem("datos_user",tokenToInit);
                          window.location.href = amigable("?module=home");

                        }
                        return false;     
                      },
                      error:function(xhr){
                        alert(xhr.responseText);
                      },

                    });*/

                }
                
               }).fail(function(xhr){
                	//alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }

           });//end fail

        }//end rdo

	});

	//logout-manual
	$( "button#logout-manual" ).on( "click", function() {
	    //alert("entra");
	    localStorage.removeItem('datos_user');
	    $("div#btn-logout-manual").css("display","none");
	    window.location.href = amigable("?module=home");
	});


});