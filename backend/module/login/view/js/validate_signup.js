$('document').ready(function() {
	$(document).on('click','#btn_submit_signup',function(){
		var rdo = true;
        
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var passwdreg = /^[A-Za-z0-9ñÑ]{5,20}$/;
            
        var name = $("#name_signup").val();
        var email = $("#email_signup").val();
        var passwd = $("#passwd_signup").val();
        var re_passwd = $("#rePasswd_signup").val();
        
        if( name == ""){
            var message = "Introduce el nombre";
            amaran(message);
            rdo=false;
            return false;
        }else if (!namereg.test(name)){
            var message = "El nombre tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( email == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;
        }else if (!emailreg.test(email)){
            var message = "Tiene que ser un email valido. ejemplo@gmail.com";
            amaran(message);
            rdo=false;
            return false;
        }if( passwd == ""){
            var message = "Introduce su password";
            amaran(message);
            rdo=false;
            return false;
        }else if (!passwdreg.test(passwd)){
            var message = "El password permite alfanumerico de 5-20 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( passwd !== re_passwd){
            var message = "El password no coincide";
            amaran(message);
            rdo=false;
            return false;
        }

        if (rdo) {
            var data = { "id":name, "email":email, "password":passwd, "re_passwd":re_passwd };
            var data_signup_JSON = JSON.stringify(data);
            console.log("json debug: "+data_signup_JSON);

            $.post(amigable('?module=login&function=alta_signup'),{alta_signup_json: data_signup_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                localStorage.setItem("aux","reg");
                if (json.success) {
                    window.location.href = json.redirect;

                }
                
               }).fail(function(xhr){
                //alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    //alert(xhr.responseJSON);
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }

                    if (xhr.responseJSON.error.validaDni){
                        amaran(xhr.responseJSON.error.validaDni);
                        
                    }

                    if (xhr.responseJSON.error.name){
                        amaran(xhr.responseJSON.error.name);
                     
                    }
                    

           });//end fail

        }//end rdo

        
	});//end click
});