$('document').ready(function() {
  var userProfile;
  var content = $('.content');
  var loadingSpinner = $('#loading');
  content.css('display', 'block');
  loadingSpinner.css('display', 'none');

  var webAuth = new auth0.WebAuth({
    domain: 'jomondo.eu.auth0.com',
    clientID: 'uJOaYrymNUnqjVmTCA70iufikU9O2Z85',
    redirectUri: 'https://localhost/freelancer/home/',
    audience: 'https://' + 'jomondo.eu.auth0.com' + '/userinfo',
    responseType: 'token id_token',
    scope: '',
    leeway: 60
  });

  //var loginStatus = $('.container h4');
  //var loginView = $('#login-view');
  //var homeView = $('#home-view');
  //var profileView = $('#profile-view');
  

  // buttons and event listeners
  var loginBtn = $('#btn-login');
  var logoutBtn = $('#btn-logout');

  loginBtn.click(function(e) {
    e.preventDefault();
    webAuth.authorize();
    localStorage.setItem('access_controller', true);
    localStorage.setItem('register',true);


  });

  logoutBtn.click(logout);

  function setSession(authResult) {
    // Set the time that the access token will expire at
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  function logout() {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('access_controller');
    localStorage.removeItem('datos_user');
    //$('div.test_consultant').remove();
    displayButtons();
    window.location.href = amigable("?module=home");
  }

  function isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  function displayButtons() {
    //var loginStatus = $('.container h4');
    if (isAuthenticated()) {
      loginBtn.css('display', 'none');
      logoutBtn.css('display', 'inline-block');
      /*loginStatus.text(
        'You are logged in! You can now view your profile area.'
      );*/
    } else {
      loginBtn.css('display', 'inline-block');
      logoutBtn.css('display', 'none');
      //loginStatus.text('You are not logged in! Please log in to continue.');
    }
  }

  function getProfile() {
    if (!userProfile) {
      var accessToken = localStorage.getItem('access_token');
      if (!accessToken) {
        console.log('Access token must exist to fetch profile');
      }

      webAuth.client.userInfo(accessToken, function(err, profile) {
        console.log(profile);
        if (profile) {
          userProfile = profile;
          displayProfile();
        }
      });
    } else {
      displayProfile();
    }
  }

  function displayProfile() {
    alert("entra en displayProfile");
    var data = {"id": userProfile.sub, "name": userProfile.name, "picture": userProfile.picture, "email": userProfile.email};
    var datos_social = JSON.stringify(data);
    //console.log(datos_social);

    load_data_ajax("?module=login&function=social_signup","POST",datos_social,"","displayProfile_social");
    
  }

  function handleAuthentication() {
    webAuth.parseHash(function(err, authResult) {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        setSession(authResult);
        loginBtn.css('display', 'none');
      } else if (err) {
        console.log(err);
        alert('Error: ' + err.error + '. Check the console for further details.');
      }
      displayButtons();
    });
  }

  handleAuthentication();

  //////////// access peticio al server per a pillar al usuari //////////
  var item_controller = localStorage.getItem('access_controller');
  alert(item_controller);
  if (item_controller=="true") {
    //alert("entra first");
    handleAuthentication();
    getProfile();
    
  }

});