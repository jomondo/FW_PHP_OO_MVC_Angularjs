<?php
	class controller_login{
		function __construct(){
				
		}

		function social_signup(){
			//print_r($_POST['data']);
			//exit;
			$jsondata = array();
			$data = json_decode($_POST['data'],true);
			
			try {
				$value = loadModel(MODEL_LOGIN, "login_model", "count", $data);

			} catch (Exception $e) {
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}
			
			if ($value[0]['count']==0) {
				//print_r("entra en count=0");
				//exit;
				if ($data['email']) {
					$avatar = $data['avatar'];
					
				}else{
					$avatar = MEDIA_GRAVATAR . "media/gravatar/".get_gravatar().".png";

				}

				$arrArgument = array(
	                'id' => $data['id'],
	                'name' => $data['name'],
	                'passwd' => $data['passwd'],
	                'email' => $data['email'],
	                'type' => 0,
	                'activate' => 1,
	                'avatar' => $avatar
	            );

	            try {
					$value = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);

				} catch (Exception $e) {
					showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

				}

				if ($value) {
					$jsondata['success'] = true;
					echo json_encode($jsondata);
					exit;

				}else{
					showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

				}
				

			}else{
				//existeix l'usuari, per tant pillarem les seues dades per almacenar-és en localstorage
				try {
					$value = loadModel(MODEL_LOGIN, "login_model", "select_freelancer", $data['id']);

				} catch (Exception $e) {
					showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

				}

				if ($value) {
					echo json_encode($value);
					exit;

				}else{
					showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

				}
			}
			
		}

		function alta_signup(){
			//print_r($_POST['data']);
			//exit;
			$jsondata = array();
        	//$userJSON = json_decode($_POST['alta_signup_json'], true);
        	$userJSON = json_decode($_POST['data'], true);

        	$rdo = validate_signup($userJSON);

        	if ($rdo['result']) {
        		//echo json_encode("correct validation php");
        		//exit;
        		try {
        			$value = loadModel(MODEL_LOGIN, "login_model", "count", $rdo['datos']);
        			
        		} catch (Exception $e) {
        			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        		}
        		
        		if ($value) {
        			$rdo_id = check_user($value);//utils login
        			//$rdo_email = check_email($value);
        			if ($rdo_id['resultName']) {
        				$avatar = MEDIA_GRAVATAR . get_gravatar() . ".png";

			            $arrArgument = array(
			                'id' => $rdo['datos']['id'],
			                'name' => $rdo['datos']['id'],
			                'passwd' => password_hash($rdo['datos']['password'], PASSWORD_BCRYPT),
			                'email' => $rdo['datos']['email'],
			                'type' => 0,
			                'activate' => 0,
			                'avatar' => $avatar,
			                'token' => ""
			            );
			            //create user
			            try {
			            	$arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
			            	$value = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);

			            } catch (Exception $e) {
			            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			            }
			            
			            if ($value) {
			            	sendtoken($arrArgument, "alta");

			            	$jsondata['success'] = true;
			            	//$jsondata['redirect'] = "../home/";
			        		echo json_encode($jsondata);
			        		exit;

			            }else{
			            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			            }

        			}else{
        				$jsondata['error'] = $rdo_id['error'];
        				echo json_encode($jsondata);
        				exit;

        			}

        		}else{
        			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        		}
        		
        	}else{
        		$jsondata['error'] = $rdo['error'];
        		echo json_encode($jsondata);
        		exit;

        	}
		}

		function verify() {
			//print_r($_GET['aux']);
			//exit;
			$jsondata = array();
			$token = $_GET['aux'];
	        if (substr($_GET['aux'], 0, 3) == "Ver") {
	            $arrArgument = array('token' => $token);

	            try {
	                $value = loadModel(MODEL_LOGIN, "login_model", "update_activate", $arrArgument);
	            } catch (Exception $e) {
	                $value = false;
	            }

	            if ($value) {
	                //die('<script>localStorage.setItem("activate",true);window.location.href="https://localhost/freelancer/home/"</script>');
	                $jsondata['success'] = true;
	                echo json_encode($jsondata);
					exit;

	            } else {
	                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
	            }
	        }
	    }

	    function verify_change_passwd() {
			//print_r($_GET['aux']);
			//print_r($_GET['aux1']);
			//exit;
			$token = $_GET['aux'];

	        if (substr($_GET['aux'], 0, 3) == "Ver") {
	            $arrArgument = array('token' => $token);

	            try {
	                $value = loadModel(MODEL_LOGIN, "login_model", "check_token", $arrArgument);

	            } catch (Exception $e) {
	            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

	            }

	            if ($value[0]['count']==1) {//si token existeix en db //canviar a ==1
	                //loadView('home', 'home');
	                //die('<script>localStorage.setItem("change",'.$id.');</script>');
	            	loadView("login","change_passwd");

	            } else {
	                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

	            }
	        }
	    }
	    function change_passwd (){
	    	$data = json_decode($_POST['data'], true);
			//echo json_encode($data);
	    	//exit;
	    	/*try {
	    		$value = loadModel(MODEL_LOGIN, "login_model", "check_email", $data['email']);

	    	} catch (Exception $e) {
	    		showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

	    	}*/

	    	//if ($value[0]['count']==1) {
	    		$arrArgument = array('token' => $data['token'], 'password' => password_hash($data['password'], PASSWORD_BCRYPT));

	    		try {
	    			$value = loadModel(MODEL_LOGIN, "login_model", "update_passwd", $arrArgument);

	    		} catch (Exception $e) {
	    			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

	    		}
	    		
	    		if ($value) {
	    			$jsondata['success'] = true;
	    			//$jsondata['redirect'] = "../../home/";
					echo json_encode($jsondata);
					exit;

	    		}else{
	    			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

	    		}
	    	/*}else{
	    		$jsondata['error'] = "Este email no existe en nuestra base de datos";
				echo json_encode($jsondata);
				exit;

	    	}*/
	    }
	    function login(){
			//print_r("entra en login");
			//exit;
			$jsondata = array();
			$data = json_decode($_POST['data'],true);
			//print_r($data);
			//exit;
			try {
				$value = loadModel(MODEL_LOGIN, "login_model", "count", $data);

			} catch (Exception $e) {
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}
			
			$rdo_name_log = check_user_login($value);

			if ($rdo_name_log['resultName']) {
				/*$jsondata['success'] = "your name si existe";
        		echo json_encode($jsondata);
        		exit;*/
        		try {
        			$value = loadModel(MODEL_LOGIN, "login_model", "select_freelancer", $data['id']);

        		}catch (Exception $e) {
        			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        		}
        		if ($value) {
        			//echo json_encode($value[0]['passwd']);
        			//exit;
        			
        			$rdo_passwd = check_passwd_login($data['password'],$value[0]['passwd']);//check passwd encript with passwd dencript
        			//echo json_encode($rdo_passwd);
        			//exit;
        			if ($rdo_passwd['resultPsswd']) {
        				// $value = loadModel(MODEL_LOGIN, "login_model", "count_activate", $data);
        				$check_lock = loadModel(MODEL_LOGIN, "login_model", "check_lock", $data);

        				if (!$check_lock[0]['count']==1) {
        					// $check_lock = loadModel(MODEL_LOGIN, "login_model", "check_lock", $data);
        					$value = loadModel(MODEL_LOGIN, "login_model", "count_activate", $data);

        					if ($value[0]['count']==1) {
        						$value = loadModel(MODEL_LOGIN, "login_model", "select_freelancer", $data['id']);

        						if ($value) {
	        						$jsondata['success'] = true;
		        					$jsondata['user'] = $value;
						        	echo json_encode($jsondata);
						        	exit;

					        	}else{
		        					showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

		        				}

        					}else{
        						// $jsondata['error'] = "Este usuario ha sido bloqueado, ponte en contacto con el administrador.";
					        	// echo json_encode($jsondata);
					        	// exit;
					        	$jsondata['error'] = "Este usuario aun no esta activado, verifica su correo para terminar el registro completo.";
			        		echo json_encode($jsondata);
			        		exit;
        					}

        				}else{
							// $jsondata['error'] = "Este usuario aun no esta activado, verifica su correo para terminar el registro completo.";
			    //     		echo json_encode($jsondata);
			    //     		exit;
			        		$jsondata['error'] = "Este usuario ha sido bloqueado, ponte en contacto con el administrador.";
					        	echo json_encode($jsondata);
					        	exit;

        				}

        			}else{
        				$jsondata['error'] = $rdo_passwd['error'];
		        		echo json_encode($jsondata);
		        		exit;

        			}
        		}else{
        			showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        		}
        		
			}else{
				$jsondata['error'] = $rdo_name_log['error'];
        		echo json_encode($jsondata);
        		exit;
			}
			
			
		}

		function update_token(){
			//echo json_encode($_POST['data']);
        	//exit;
			$jsondata = array();
			$data = json_decode($_POST['data'],true);

			try {
				$value = loadModel(MODEL_LOGIN, "login_model", "update_token", $data);

			} catch (Exception $e) {
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}

			if ($value) {
				$jsondata['success']=true;
				echo json_encode($jsondata);
        		exit;

			}else{
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}
		}

		function select_user_token(){
			//echo json_encode($_POST['data']);
        	//exit;
			$jsondata = array();
			$data = json_decode($_POST['data'],true);

			try {
				$value = loadModel(MODEL_LOGIN, "login_model", "select_user_token", $data['token']);

			} catch (Exception $e) {
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}

			if ($value) {
				$jsondata['success']=true;
				$jsondata['user']=$value;
				echo json_encode($jsondata);
        		exit;

			}else{
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}
		}

		function check_email(){
			//echo json_encode($_POST['data']);
        	//exit;
			$jsondata = array();
			$data = json_decode($_POST['data'],true);

			try {
				$value = loadModel(MODEL_LOGIN, "login_model", "check_email", $data['email']);

			} catch (Exception $e) {
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}

			if ($value[0]['count']>=1) {
				/*$jsondata['success']=true;
				echo json_encode($jsondata);
        		exit;*/
        		$arrArgument = array('email' => $data['email']);
        		$arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
			    
			    try {
			    	$update_token = loadModel(MODEL_LOGIN, "login_model", "updt_tokn_chge_passwd", $arrArgument);

			    } catch (Exception $e) {
			    	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			    }

			    if($update_token){
			    	sendtoken($arrArgument, "modificacion");

			    	$jsondata['success']=true;
			    	//$jsondata['redirect']="../home/";
					echo json_encode($jsondata);
	        		exit;

			    }else{
			    	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			    }

			}else if($value[0]['count']==0){
				$jsondata['msg']="Este email no se encuentra en nuestra base de datos";
				echo json_encode($jsondata);
        		exit;

			}else{
				showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

			}
		}

	}