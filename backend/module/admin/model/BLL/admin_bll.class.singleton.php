<?php    
    class admin_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = admin_dao::getInstance();
            $this->db = db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function insert_visitor_BLL($arrayValue) {
            return $this->dao->insert_visitor_DAO($this->db, $arrayValue);
        }

        public function get_total_visitors_BLL() {
            return $this->dao->get_total_visitors_DAO($this->db);
        }

        public function get_total_freelancer_BLL() {
            return $this->dao->get_total_freelancer_DAO($this->db);
        }

        public function get_total_clients_BLL() {
            return $this->dao->get_total_clients_DAO($this->db);
        }

        public function get_users_register_BLL() {
            return $this->dao->get_users_register_DAO($this->db);
        }

        public function lock_user_BLL($valArray) {
            return $this->dao->lock_user_DAO($this->db, $valArray);
        }

        public function unlock_user_BLL($valArray) {
            return $this->dao->unlock_user_DAO($this->db, $valArray);
        }

        public function delete_user_BLL($valArray) {
            return $this->dao->delete_user_DAO($this->db, $valArray);
        }

        public function get_visitors_last_7days_BLL() {
            return $this->dao->get_visitors_last_7days_DAO($this->db);
        }

        
    }