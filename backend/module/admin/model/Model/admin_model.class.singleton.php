<?php
    class admin_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = admin_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function insert_visitor($arrayValue) {
            return $this->bll->insert_visitor_BLL($arrayValue);
        }

        public function get_total_visitors() {
            return $this->bll->get_total_visitors_BLL();
        }

        public function get_total_freelancer() {
            return $this->bll->get_total_freelancer_BLL();
        }

        public function get_total_clients() {
            return $this->bll->get_total_clients_BLL();
        }

        public function get_users_register() {
            return $this->bll->get_users_register_BLL();
        }

        public function lock_user($valArray) {
            return $this->bll->lock_user_BLL($valArray);
        }

        public function unlock_user($valArray) {
            return $this->bll->unlock_user_BLL($valArray);
        }

        public function delete_user($valArray) {
            return $this->bll->delete_user_BLL($valArray);
        }

        public function get_visitors_last_7days() {
            return $this->bll->get_visitors_last_7days_BLL();
        }
        
    }