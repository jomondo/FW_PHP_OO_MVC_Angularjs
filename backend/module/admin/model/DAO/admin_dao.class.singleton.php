<?php
class admin_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function insert_visitor_DAO($db, $ip) {
        
        // $date = new DateTime();
        // $date_format =  $date->format('d-m-Y');
        

        $sql = "INSERT INTO visitors ( ip, date ) VALUES ('$ip', CURRENT_DATE)";

        return $stmt = $db->ejecutar($sql);
        
    
    }

    public function get_total_visitors_DAO($db) {

        $sql = "SELECT count(*) as count_visitors FROM visitors";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function get_total_freelancer_DAO($db) {

        $sql = "SELECT count(*) as count_freelancer FROM autonomo";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function get_total_clients_DAO($db) {

        $sql = "SELECT count(*) as count_clients FROM cliente";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function get_users_register_DAO($db) {

        $sql = "SELECT * FROM user_register";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function lock_user_DAO($db, $valArray) {
        //triggers
        $sql = "UPDATE user_register SET activate = 2 WHERE id_user = '".$valArray['id_user']."' ";

        return $stmt = $db->ejecutar($sql);
        
    
    }

    public function unlock_user_DAO($db, $valArray) {

        $sql = "UPDATE user_register SET activate = 1 WHERE id_user = '".$valArray['id_user']."' ";

        return $stmt = $db->ejecutar($sql);
        
    
    }

    public function delete_user_DAO($db, $valArray) {

        $sql = "DELETE FROM user_register WHERE id_user = '".$valArray['id_user']."' ";

        return $stmt = $db->ejecutar($sql);
        
    
    }

    public function get_visitors_last_7days_DAO($db) {
        
        $sql = "SELECT count(*) as visitors_last7days FROM visitors WHERE date BETWEEN CURRENT_DATE()-7 AND CURRENT_DATE() GROUP BY date ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }



   

}
