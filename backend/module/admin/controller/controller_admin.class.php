<?php
	class controller_admin{
		function __construct(){
				
		}

		function control_visitas(){
			//write_visita();
			/*--> simulacion ip <-- */
			$ip = rand();

			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "insert_visitor", $ip);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function get_total_visitors(){
			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "get_total_visitors");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	echo json_encode($value);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function get_total_freelancer(){
			
			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "get_total_freelancer");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	echo json_encode($value);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function get_total_clients(){
			
			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "get_total_clients");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	echo json_encode($value);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function get_users_register(){
			
			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "get_users_register");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	echo json_encode($value);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function lock_user(){
			$jsondata = array();

			$data = json_decode($_POST["data"], true);

			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "lock_user", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	$jsondata['success']=true;
            	echo json_encode($jsondata);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

		function unlock_user(){
			$jsondata = array();

			$data = json_decode($_POST["data"], true);

			try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "unlock_user", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
            	$jsondata['success']=true;
            	echo json_encode($jsondata);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


		}

        function delete_user(){
            $jsondata = array();

            $data = json_decode($_POST["data"], true);

            try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "delete_user", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                $jsondata['success']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


        }

        function get_visitors_last_7days(){

            try{
                $value = loadModel(MODEL_ADMIN, "admin_model", "get_visitors_last_7days");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                echo json_encode($value);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }


        }

	}