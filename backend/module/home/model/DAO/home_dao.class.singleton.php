<?php
class home_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_freelancer_DAO($db) {
        $sql = "CALL get_freelancers()";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function obtain_price_h_DAO($db) {
        //print_r("entra en obtain_price_h_DAO");
        //exit;
        $sql = "SELECT DISTINCT price_h FROM autonomo";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }
    
    public function details_freelancer_DAO($db,$id) {
        $sql = "CALL details_freelancer('".$id."')";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_client_DAO($db) {
        $sql = "SELECT count(*) as count FROM cliente";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_free_DAO($db) {
        $sql = "SELECT count(*) as count FROM autonomo";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    /*public function pagination_freelancer_DAO($arrayValues,$db) {
        $pos = $arrayValues['position'];
        $user_per_page = $arrayValues['user_per_page'];
        $name_categ = $arrayValues['category'];
        
        $sql = "SELECT * FROM autonomo WHERE categ_prof='".$name_categ."' LIMIT ".$pos." , ".$user_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }*/

    public function count_by_category_DAO($id_category,$db) {

        $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof='".$id_category['categ_prof']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_val_category_DAO($id_category,$db) {
        $sql = "CALL get_cont_category('".$id_category['categ_prof']."')";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function update_val_category_DAO($val,$db) {

        $sql = "UPDATE category SET value='".$val['val_categ']."' WHERE name_catg='".$val['categ_prof']."'";

        return $stmt = $db->ejecutar($sql);

    }

    public function get_category_DAO($db) {
        $sql = "CALL get_categories()";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_category_DAO($db) {
        $sql = "CALL obtain_category()";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_especialist_DAO($db) {

        $sql = "CALL get_especialist()";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_province_DAO($db) {

        $sql = "CALL get_province()";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function paintAutocomplete_DAO($db) {

        $sql = "SELECT DISTINCT especialista as label, categ_prof as category FROM autonomo";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function dataSearch_DAO($arrayValues,$db) {
        $especialista = $arrayValues['especialista'];
        $name_categ = $arrayValues['categ_prof'];
        
        $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof='".$name_categ."' and especialista='".$especialista."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function list_Search_DAO($arrayValues,$db) {
        $name_categ = $arrayValues['categ_prof'];

        $sql = "SELECT * FROM autonomo WHERE categ_prof='".$name_categ."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    /*public function count_searchKey_DAO($arrayValues,$db) {

        $sql = "SELECT count(*) as count FROM autonomo WHERE especialista LIKE '%".$arrayValues['name_key']."%' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }*/

    public function submitValueWeb_DAO($arrayValues,$db) {
        
        $sql = "INSERT INTO value_website ( name, stars, message, avatar ) VALUES ('$arrayValues[name]', '$arrayValues[stars]','$arrayValues[mgs]', '$arrayValues[avatar]')";

        return $stmt = $db->ejecutar($sql);

    }

    public function insert_best_freelancer_DAO($arrayValues,$db) {
        
        $sql = "INSERT INTO best_freelancer ( dni_freelancer,stars ) VALUES ('$arrayValues[dni_valorado]', '$arrayValues[stars]')";

        return $stmt = $db->ejecutar($sql);

    }

    public function update_stars_DAO($arrayValues,$db) {
        
        $sql = "UPDATE best_freelancer SET stars = '".$arrayValues['stars']."' WHERE dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        return $stmt = $db->ejecutar($sql);

    }

    public function verify_rating_DAO($arrayValues,$db) {
        
        $sql = "SELECT count(*) as count FROM value_freelancer WHERE id_valorador = '".$arrayValues['id_valorador']."' and dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_best_freelancer_DAO($arrayValues,$db) {
        
        $sql = "SELECT count(*) as count FROM best_freelancer WHERE dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function select_stars_DAO($dni,$db) {
        
        $sql = "SELECT stars FROM value_freelancer WHERE dni_freelancer = '".$dni."' ORDER BY stars DESC LIMIT 1";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_votaciones_DAO($db, $arrayVal) {
        $sql = "CALL count_votaciones('".$arrayVal['dni']."') ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_votaciones_by_user_DAO($db, $arrayVal) {
        
        $sql = "SELECT * FROM value_freelancer ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function submitValueFreelancer_DAO($arrayValues,$db) {
    
        $sql = "INSERT INTO value_freelancer ( dni_freelancer, id_valorador, stars ) VALUES ('$arrayValues[dni_valorado]', '$arrayValues[id_valorador]','$arrayValues[stars]')";

        return $db->ejecutar($sql);

    }

    public function best_clients_DAO($db) {

        $sql = "SELECT * FROM value_website ORDER BY value_website.stars DESC LIMIT 3";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_best_freelancer_DAO($db) {

        $sql = "SELECT * FROM best_freelancer ORDER BY stars DESC LIMIT 6";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_data_freelancer_DAO($db,$arrayValues) {

        $sql = "SELECT * FROM autonomo WHERE dni = '".$arrayValues['dni1']."' or dni = '".$arrayValues['dni2']."' or dni = '".$arrayValues['dni3']."' or dni = '".$arrayValues['dni4']."' or dni = '".$arrayValues['dni5']."' or dni = '".$arrayValues['dni6']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_markets_DAO($db,$arrayValues) {

        $sql = "SELECT * FROM city_coordenadas WHERE city = '".$arrayValues['city']."' and dni = '".$arrayValues['dni']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function data_geolocation_DAO($db,$arrayValues) {

        $sql = "SELECT name, categ_prof, especialista, materia, price_h FROM city_coordenadas INNER JOIN autonomo ON city_coordenadas.dni = autonomo.dni WHERE autonomo.dni = '".$arrayValues['dni']."';";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function like_freelancer_DAO($db, $arrayValues) {
        
        //echo json_encode($arrayValues);
        //exit;
        $sql = "INSERT INTO likes ( dni_freelancer,id_user ) VALUES ('$arrayValues[dni_like]', '$arrayValues[id_user]')";

        return $stmt = $db->ejecutar($sql);

    }

    public function check_like_DAO($db,$arrayValues) {
        //SELECT count(*) as count FROM likes WHERE id_user = '".$arrayValues['id_user']."' and dni_freelancer = '".$arrayValues['dni_like'].
        $sql = "SELECT count(*) as count FROM likes WHERE id_user = '".$arrayValues['id_user']."' and dni_freelancer = '".$arrayValues['dni_like']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_likes_DAO($db,$arrayValues) {
        
        $sql = "CALL get_likes('".$arrayValues['dni']."')";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function stop_likes_DAO($db, $arrayValues) {
        $sql = "DELETE FROM likes WHERE dni_freelancer = '".$arrayValues['dni_like']."' and id_user = '".$arrayValues['id_user']."' ";

        return $stmt = $db->ejecutar($sql);

    }

    public function update_points_user_DAO($db, $val) {

        $sql = "UPDATE user_register SET points='".$val['points']."' WHERE id_user='".$val['id_user']."' ";

        return $stmt = $db->ejecutar($sql);

    }

    public function get_users_register_DAO($db) {
        
        $sql = "SELECT * FROM user_register ORDER BY points DESC";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

}
