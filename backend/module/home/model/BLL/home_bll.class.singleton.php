<?php    
    class home_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = home_dao::getInstance();
            $this->db = db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function list_freelancer_BLL() {
            return $this->dao->list_freelancer_DAO($this->db);
        }

        public function details_freelancer_BLL($id) {
            return $this->dao->details_freelancer_DAO($this->db,$id);
        }
        
        public function count_client_BLL() {
            return $this->dao->count_client_DAO($this->db);
        }

        public function count_free_BLL() {
            return $this->dao->count_free_DAO($this->db);
        }

        public function pagination_freelancer_BLL($arrayValues) {
            return $this->dao->pagination_freelancer_DAO($arrayValues,$this->db);
        }
        
        public function get_category_BLL() {
            return $this->dao->get_category_DAO($this->db);
        }

        public function obtain_category_BLL() {
            return $this->dao->obtain_category_DAO($this->db);
        }

        public function obtain_especialist_BLL() {
            return $this->dao->obtain_especialist_DAO($this->db);
        }

        public function obtain_province_BLL() {
            return $this->dao->obtain_province_DAO($this->db);
        }

        public function get_val_category_BLL($id_category) {
            return $this->dao->get_val_category_DAO($id_category,$this->db);
        }

        public function update_val_category_BLL($val) {
            return $this->dao->update_val_category_DAO($val,$this->db);
        }

        public function count_by_category_BLL($id_category) {
            return $this->dao->count_by_category_DAO($id_category,$this->db);
        }

        public function paintAutocomplete_BLL() {
            return $this->dao->paintAutocomplete_DAO($this->db);
        }

        public function dataSearch_BLL($arrayValues) {
            return $this->dao->dataSearch_DAO($arrayValues,$this->db);
        }

        public function list_Search_BLL($arrayValues) {
            return $this->dao->list_Search_DAO($arrayValues,$this->db);
        }

        public function count_searchKey_BLL($arrayValues) {
            return $this->dao->count_searchKey_DAO($arrayValues,$this->db);
        }

        public function list_KeySearch_BLL($arrayValues) {
            return $this->dao->list_KeySearch_DAO($arrayValues,$this->db);
        }

        public function apply_filters_BLL($arrayValues) {
            return $this->dao->apply_filters_DAO($arrayValues,$this->db);
        }

        public function list_filter_BLL($arrayValues) {
            return $this->dao->list_filter_DAO($arrayValues,$this->db);
        }

        public function obtain_price_h_BLL() {
            return $this->dao->obtain_price_h_DAO($this->db);
        }

        public function submitValueWeb_BLL($arrayValues) {
            return $this->dao->submitValueWeb_DAO($arrayValues,$this->db);
        }

        public function select_stars_BLL($arrayValues) {
            return $this->dao->select_stars_DAO($arrayValues,$this->db);
        }

        public function submitValueFreelancer_BLL($arrayValues) {
            return $this->dao->submitValueFreelancer_DAO($arrayValues,$this->db);
        }

        public function verify_rating_BLL($arrayValues) {
            return $this->dao->verify_rating_DAO($arrayValues,$this->db);
        }

        public function update_stars_BLL($arrayValues) {
            return $this->dao->update_stars_DAO($arrayValues,$this->db);
        }

        public function insert_best_freelancer_BLL($arrayValues) {
            return $this->dao->insert_best_freelancer_DAO($arrayValues,$this->db);
        }

        public function count_best_freelancer_BLL($arrayValues) {
            return $this->dao->count_best_freelancer_DAO($arrayValues,$this->db);
        }

        public function best_clients_BLL() {
            return $this->dao->best_clients_DAO($this->db);
        }

        public function get_markets_BLL($arrayValues) {
            return $this->dao->get_markets_DAO($this->db,$arrayValues);
        }

        public function data_geolocation_BLL($arrayValues) {
            return $this->dao->data_geolocation_DAO($this->db,$arrayValues);
        }

        public function get_best_freelancer_BLL($arrayValues) {
            return $this->dao->get_best_freelancer_DAO($this->db,$arrayValues);
        }

        public function get_data_freelancer_BLL($arrayValues) {
            return $this->dao->get_data_freelancer_DAO($this->db,$arrayValues);
        }

        public function like_freelancer_BLL($arrayValues) {
            return $this->dao->like_freelancer_DAO($this->db,$arrayValues);
        }

        public function check_like_BLL($arrayValues) {
            return $this->dao->check_like_DAO($this->db,$arrayValues);
        }

        public function get_likes_BLL($arrayValues) {
            return $this->dao->get_likes_DAO($this->db,$arrayValues);
        }

        public function stop_likes_BLL($arrayValues) {
            return $this->dao->stop_likes_DAO($this->db,$arrayValues);
        }

        public function update_points_user_BLL($arrayValues) {
            return $this->dao->update_points_user_DAO($this->db,$arrayValues);
        }

        public function get_users_register_BLL() {
            return $this->dao->get_users_register_DAO($this->db);
        }

        public function get_votaciones_BLL($arrayValues) {
            return $this->dao->get_votaciones_DAO($this->db, $arrayValues);
        }

        public function get_votaciones_by_user_BLL($arrayValues) {
            return $this->dao->get_votaciones_by_user_DAO($this->db, $arrayValues);
        }

        
    }