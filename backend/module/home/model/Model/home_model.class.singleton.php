<?php
    class home_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = home_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function list_freelancer() {
            return $this->bll->list_freelancer_BLL();
        }
        
        public function details_freelancer($id) {
            return $this->bll->details_freelancer_BLL($id);
        }

        public function count_client() {
            return $this->bll->count_client_BLL();
        }

        public function count_free() {
            return $this->bll->count_free_BLL();
        }

        public function pagination_freelancer($arrayValues) {
            return $this->bll->pagination_freelancer_BLL($arrayValues);
        }

        public function get_category() {
            return $this->bll->get_category_BLL();
        }

        public function obtain_category() {
            return $this->bll->obtain_category_BLL();
        }

        public function obtain_especialist() {
            return $this->bll->obtain_especialist_BLL();
        }

        public function obtain_province() {
            return $this->bll->obtain_province_BLL();
        }

        public function get_val_category($id_category) {
            return $this->bll->get_val_category_BLL($id_category);
        }

        public function update_val_category($val) {
            return $this->bll->update_val_category_BLL($val);
        }

        public function count_by_category($id_category) {
            return $this->bll->count_by_category_BLL($id_category);
        }

        public function paintAutocomplete() {
            return $this->bll->paintAutocomplete_BLL();
        }

        public function dataSearch($arrayValues) {
            return $this->bll->dataSearch_BLL($arrayValues);
        }

        public function list_Search($arrayValues) {
            return $this->bll->list_Search_BLL($arrayValues);
        }

        public function count_searchKey($arrayValues) {
            return $this->bll->count_searchKey_BLL($arrayValues);
        }

        public function list_KeySearch($arrayValues) {
            return $this->bll->list_KeySearch_BLL($arrayValues);
        }

        public function apply_filters($arrayValues) {
            return $this->bll->apply_filters_BLL($arrayValues);
        }

        public function list_filter($arrayValues) {
            return $this->bll->list_filter_BLL($arrayValues);
        }

        public function obtain_price_h(){
            return $this->bll->obtain_price_h_BLL();
        }

        public function submitValueWeb($arrayValues){
            return $this->bll->submitValueWeb_BLL($arrayValues);
        }

        public function submitValueFreelancer($arrayValues){
            return $this->bll->submitValueFreelancer_BLL($arrayValues);
        }

        public function select_stars($arrayValues){
            return $this->bll->select_stars_BLL($arrayValues);
        }

        public function update_stars($arrayValues){
            return $this->bll->update_stars_BLL($arrayValues);
        }

        public function insert_best_freelancer($arrayValues){
            return $this->bll->insert_best_freelancer_BLL($arrayValues);
        }
        
        public function verify_rating($arrayValues){
            return $this->bll->verify_rating_BLL($arrayValues);
        }

        public function count_best_freelancer($arrayValues){
            return $this->bll->count_best_freelancer_BLL($arrayValues);
        }

        public function best_clients(){
            return $this->bll->best_clients_BLL();
        }

        public function get_markets($arrayValues){
            return $this->bll->get_markets_BLL($arrayValues);
        }

        public function data_geolocation($arrayValues){
            return $this->bll->data_geolocation_BLL($arrayValues);
        }

        public function get_best_freelancer($arrayValues){
            return $this->bll->get_best_freelancer_BLL($arrayValues);
        }

        public function get_data_freelancer($arrayValues){
            return $this->bll->get_data_freelancer_BLL($arrayValues);
        }

        public function like_freelancer($arrayValues){
            return $this->bll->like_freelancer_BLL($arrayValues);
        }

        public function check_like($arrayValues){
            return $this->bll->check_like_BLL($arrayValues);
        }

        public function get_likes($arrayValues){
            return $this->bll->get_likes_BLL($arrayValues);
        }

        public function stop_likes($arrayValues){
            return $this->bll->stop_likes_BLL($arrayValues);
        }

        public function update_points_user($arrayValues){
            return $this->bll->update_points_user_BLL($arrayValues);
        }

        public function get_users_register(){
            return $this->bll->get_users_register_BLL();
        }

        public function get_votaciones($arrayValues){
            return $this->bll->get_votaciones_BLL($arrayValues);
        }

        public function get_votaciones_by_user($arrayValues){
            return $this->bll->get_votaciones_by_user_BLL($arrayValues);
        }

    }