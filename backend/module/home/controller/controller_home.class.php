<?php
    class controller_home {
        function __construct() {

        }

        function getAutocomplete(){
            //print_r("getAutocompleteeeiosssss");
            //exit;
            getData("paintAutocomplete");

        }

        function list_freelancer(){
           // print_r("list_freelancer");
            //exit;
            getData("list_freelancer");


        }

        function get_best_freelancer(){
            //print_r("get_best_freelancer");
            //exit;
            data_best_freelancer("get_best_freelancer","get_data_freelancer");

        }

        function search(){
            //print_r("entra en search");
            //exit;
            count_freelancer("dataSearch","listSearchHome","countSearch","datosSearch");


        }

        function begin() {
            //print_r("begin");
            //exit;
            loadView("home","home");
    
        }

        function developer() {
            loadView("home","student_web_developer");
    
        }

        function listUser() {
            loadView("home","listUser");
            
        }

        function listUserPaint() {
            if (isset($_SESSION['countSearch']) && isset($_SESSION['datosSearch'])) {
                //print_r("si existe countSearch");
                //exit;
                $data = array();
                $data['count']=$_SESSION['countSearch'];
                $data['data']=$_SESSION['datosSearch'];
                echo json_encode($data);
                exit;

            }
        }

        function paintList(){
            //print_r("entra en paintList");
            //exit;
            paintList("list_Search");


        }

        function details(){
            //print_r("entra en details");
            //exit;
            details_freelancer();


        }

        function details_list(){
            //print_r("entra en details");
            //exit;
            details_freelancer_list();


        }

        function viewDetails(){
            //print_r("entra en viewDetails");
            //exit;
            loadView("home","detailsUser");
            
        }

        function paintDetails(){
            //print_r("entra en paintDetails");
            //exit;
            if (isset($_SESSION['detailsFree'])) {
                $data = array();
                $data=$_SESSION['detailsFree'];
                echo json_encode($data);
                exit;

            }else{
                $jsondata = array();
                showErrorPage(0, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);

            }
        }

        function countStatisticFree(){
            getData("count_free");

        }

        function countStatisticCli(){
            getData("count_client");

        }

        function nameCategory(){
            getData("get_category");

        }

        function countCategory(){
            count_freelancer("count_by_category","listUser","countUserByCateg","nameByCateg");

        }

        function paintListFreelancer(){
            //print_r("entra en paintListFreelancer");
            //exit;
            if (isset($_SESSION['countUserByCateg']) && isset($_SESSION['nameByCateg'])) {
                $data = array();
                $data['count']=$_SESSION['countUserByCateg'];
                $data['data']=$_SESSION['nameByCateg'];
                echo json_encode($data);
                exit;

            }else{
                $jsondata = array();
                showErrorPage(0, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);

            }

        }

        function num_page(){
            paintList("pagination_freelancer");

        }

        function countListSearch(){
            if (isset($_SESSION['countSearch']) && isset($_SESSION['datosSearch'])) {
                $data = array();
                $data['count']=$_SESSION['countSearch'];
                $data['data']=$_SESSION['datosSearch'];
                echo json_encode($data);
                exit;

            }else{
                $jsondata = array();
                showErrorPage(0, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);

            }
        }

        function paintListSearch(){
            paintList("list_Search");

        }

        function countSearchKey(){
            count_freelancer("count_searchKey","listKeySearch","countkeySearch","namekeySearch");

        }

        function ListSearchKey(){
            if (isset($_SESSION['countkeySearch']) && isset($_SESSION['namekeySearch'])) {
                $data = array();
                $data['count']=$_SESSION['countkeySearch'];
                $data['data']=$_SESSION['namekeySearch'];
                echo json_encode($data);
                exit;

            }else{
                $jsondata = array();
                showErrorPage(0, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);

            }

        }

        function paintListKeySearch(){
            paintList("list_KeySearch");

        }

        function valCateg(){
            if (isset($_POST['data'])) {
                getData_specific("get_val_category");

            }
        
        }

        function updateValCateg(){
            //print_r("entra en updateValCateg");
            //exit;
            updateData("update_val_category");

        }

        function obtainCateg(){
            //print_r("entra en obtainCateg");
            //exit;
            getData("obtain_category");

        }

        function obtainEspecialist(){
            getData("obtain_especialist");

        }

        function obtainProvince(){
            getData("obtain_province");

        }

        function apply_filters(){
            //print_r($_POST['data']);
            //exit;
            //filters("apply_filters");
            count_freelancer("apply_filters","","count_filt_categ","data_filt_categ");

        }

        function ListFilter(){
            if (isset($_SESSION['count_filt_categ']) && isset($_SESSION['data_filt_categ'])) {
                $data = array();
                $data['count']=$_SESSION['count_filt_categ'];
                $data['data']=$_SESSION['data_filt_categ'];
                echo json_encode($data);
                exit;

            }else{
                $jsondata = array();
                showErrorPage(0, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);

            }

        }

        function paintListFilter(){
            //print_r("entra en paintListFilter");
            //exit;
            paintList("list_filter");

        }

        function obtain_price_h(){
            //print_r("entra en obtain_price_h");
            //exit;
            getData("obtain_price_h");
            
        }

        function data_val_web(){
            getData_specific("submitValueWeb");
            
        }

        function submitValueFreelancer(){
            insert_value_freelancer("verify_rating","select_stars","submitValueFreelancer","insert_best_freelancer","update_stars","count_best_freelancer");
            
        }

        function paintBestClient(){
            getData("best_clients");
            
        }

        function get_markets(){
            //print_r("entra en get_markets");
            //exit;
            getData_specific("get_markets");
            
        }

        function data_geolocation(){
            getData_specific("data_geolocation");
            
        }

        function submit_like(){
            insert_like("check_like", "like_freelancer");
            
        }

        function stop_like(){
            stop_like("stop_likes");
            
        }

        function get_likes_details(){
            getData_specific("get_likes");
            
        }

        function update_points_user(){
            updateData("update_points_user");
            
        }

        function get_users_register(){
            getData("get_users_register");
            
        }

        function get_votaciones(){
            getData_specific("get_votaciones");
            
        }

        function get_votaciones_by_user(){
            // getData_specific("get_votaciones");
             getData("get_votaciones_by_user");
            
        }

  
}

    