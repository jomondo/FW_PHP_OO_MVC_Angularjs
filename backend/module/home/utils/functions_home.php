<?php
	function getData($model){
        $jsondata = array();
            
        $value = false;

        try{
            $value = loadModel(MODEL_HOME, "home_model", $model);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            echo json_encode($value);
            exit;
                
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        } 
            
    }

    function data_best_freelancer($model1,$model2){
        $jsondata = array();
            
        $value = false;

        try{
            $value = loadModel(MODEL_HOME, "home_model", $model1);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
           $arrayValues = array(
                'dni1' => $value[0]['dni_freelancer'],
                'dni2' => $value[1]['dni_freelancer'],
                'dni3' => $value[2]['dni_freelancer'],
                'dni4' => $value[3]['dni_freelancer'],
                'dni5' => $value[4]['dni_freelancer'],
                'dni6' => $value[5]['dni_freelancer']
                    
            );
            //echo json_encode($arrayValues);
            //exit;
            try{
                $value_best = loadModel(MODEL_HOME, "home_model", $model2, $arrayValues);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }
            
            if ($value_best) {
                echo json_encode($value_best);
                exit;
            }
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        } 
            
    }

    function getData_specific($model){
        
        $jsondata = array();
            
        $value = false;

        $getData= json_decode($_POST['data'],true);
        // print_r($getData);
        // exit;
        @$val1 = $getData['categ_prof'];
        @$val2 = $getData['name'];
        @$val3 = $getData['categ'];
        @$val4 = $getData['stars'];
        @$val5 = $getData['mgs'];
        @$val6 = $getData['city'];
        @$val7 = $getData['dni'];
        @$val8 = $getData['avatar'];
        @$val9 = $getData['token'];
        //@$val9 = $getData['id_valorador'];
        //@$val10 = $getData['dni_valorado'];

        $arrayValues = array(
            'categ_prof' => $val1,
            'name' => $val2,
            'categ' => $val3,
            'stars' => $val4,
            'mgs' => $val5,
            'city' => $val6,
            'dni' => $val7,
            'avatar' => $val8,
            'token' => $val9
                
        );
        //print_r($arrayValues);
        //exit;
        try{
            $value = loadModel(MODEL_HOME, "home_model", $model, $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            $jsondata['success']=true;
            $jsondata['value']=$value;
            echo json_encode($jsondata);
            exit;
                
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        } 
            
    }

    function insert_value_freelancer($model1,$model2,$model3,$model4,$model5,$model6){
        $jsondata = array();
            
        $value = false;

        $getData= json_decode($_POST['data'],true);
        $val1 = $getData['stars'];
        $val2 = $getData['id_valorador'];
        $val3 = $getData['dni_valorado'];

        $arrayValues = array(
            'id_valorador' => $val2,
            'dni_valorado' => $val3,
            'stars' => $val1
                
        );
        //print_r($arrayValues);
        //exit;

        //comprovem si el valorador ja ha valorat al freelancer
        try{
            $value = loadModel(MODEL_HOME, "home_model", $model1, $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        //pillem les estrelles que conte el freelancer valorat
        if ($value[0]['count']==0){
            try{
                $value = loadModel(MODEL_HOME, "home_model", $model2, $arrayValues['dni_valorado']);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }
            //echo json_encode($value[0]['stars']);
            //exit;
            if ($value[0]['stars']!=null){
                //sumem les que tenia + les que l'usuari a clicat
                $total_stars = $value[0]['stars'] + $arrayValues['stars'];
                $arrayValues['stars'] = $total_stars;

                $value = loadModel(MODEL_HOME, "home_model", $model3, $arrayValues);

                if ($value){
                    //altre contra DAO -> comprobar si el user existeix en la taula best_freelancer (count_best_freelancer)
                    //si ja existeix fer update, si no fer insert
                    $value_count = loadModel(MODEL_HOME, "home_model", $model6, $arrayValues);//count
                    if ($value_count[0]['count']==0) {
                        $value = loadModel(MODEL_HOME, "home_model", $model4, $arrayValues);//insert table best_freelancer
                        if ($value) {
                            $jsondata['success']=true;
                            echo json_encode($jsondata);
                            exit;
                        }else{
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                        }
                    }else{
                        $value = loadModel(MODEL_HOME, "home_model", $model5, $arrayValues);//update
                        if ($value) {
                            $jsondata['success']=true;
                            echo json_encode($jsondata);
                            exit;
                        }else{
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                        }

                    }
                        
                }else{
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                }
                    
            }else{
                //si no te estrelles, insertem directament
                $value = loadModel(MODEL_HOME, "home_model", $model3, $arrayValues);
                
                if ($value){
                    $value_count = loadModel(MODEL_HOME, "home_model", $model6, $arrayValues);//count

                    if ($value_count[0]['count']==0) {
                        $value = loadModel(MODEL_HOME, "home_model", $model4, $arrayValues);//insert table best_freelancer

                        if ($value) {
                            $jsondata['success']=true;
                            echo json_encode($jsondata);
                            exit;
                        }else{
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                        }
                    }else{
                        $value = loadModel(MODEL_HOME, "home_model", $model5, $arrayValues);//update
                        if ($value) {
                            $jsondata['success']=true;
                            echo json_encode($jsondata);
                            exit;
                        }else{
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                        }

                    }
                        
                }else{
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                }

            }
                
        }else if($value[0]['count']==1){
            $jsondata['mgs']="Ya has valorado a este usuario";
            echo json_encode($jsondata);
            exit;

        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
            
    }

    function updateData($model){
        //print_r("entra en updateData");
        //exit;
        $jsondata = array();
            
        $value = false;

        $getData= json_decode($_POST['data'],true);
        //print_r($_POST['data']);
        //exit;
        @$val1 = $getData['categ_prof'];
        @$val2 = $getData['val_categ'];
        @$val3 = $getData['id_user'];
        @$val4 = $getData['points'];

        $arrayValues = array(
            'categ_prof' => $val1,
            'val_categ' => $val2,
            'id_user' => $val3,
            'points' => $val4,
                
        );

        try{
            $value = loadModel(MODEL_HOME, "home_model", $model, $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            echo json_encode($value);
            exit;
                
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        } 
            
    }

    function count_freelancer($listSearch,$list,$name_count,$data_name){
        $jsondata = array();
        
        if (isset($_POST['data']) && ($_POST['data']==true)) {
            $value = false;

            $dataSearch = json_decode($_POST['data'],true);

            @$especialista = $dataSearch['especialista'];
            @$categ_prof = $dataSearch['categ_prof'];
            @$province = $dataSearch['province'];
            @$price_min = $dataSearch['price_min'];
            @$price_max = $dataSearch['price_max'];
            @$keyword = $dataSearch['name_key'];
            
            $arrayValues = array(
                'especialista' => $especialista,
                'categ_prof' => $categ_prof,
                'province' => $province,
                'price_min' => $price_min,
                'price_max' => $price_max,
                'name_key' => $keyword,
                
            );
            //echo json_encode($arrayValues);
            //exit;
            try{
                $value = loadModel(MODEL_HOME, "home_model", $listSearch, $arrayValues);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }
            //echo json_encode($value);
            //exit;
            if ($value){
                $user_per_page=2;

                $rows=$value[0]['count'];
                $pages = ceil($rows / $user_per_page);

                $_SESSION[$name_count]=$pages;
                $_SESSION[$data_name]=$arrayValues;
                $jsondata['ref']=$list;
                //$callback="index.php?module=home&function=listUser";
                $callback="../home/listUser/";
                $jsondata['success']=true;
                $jsondata['redirect']=$callback;
                echo json_encode($jsondata);
                exit;
                
            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            } 
        }
        
        
    }

    function paintList($list){
        //echo json_encode($_POST['data']);
        //exit;
        $jsondata = array();
        
        $name_categ = $_POST['data'];
        $province = $num['province'];
        $price_min = $num['price_min'];
        $price_max = $num['price_max'];
        $especialista = $num['especialista'];
        @$keyword = $num['name']['name_key'];
        //echo json_encode($keyword);
        //exit;
        $arrayValues = array(
            'categ_prof' => $name_categ,
            'especialista' => $especialista,
            'name_key' => $keyword,
            'price_min' => $price_min,
            'price_max' => $price_max,
            'province' => $province
        );
        //echo json_encode($arrayValues);
        //exit;
        $value = false;

        try{
            $value = loadModel(MODEL_HOME, "home_model", $list, $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        }
        //echo json_encode($value);
        //exit;
        if ($value) {
            //$jsondata['count']=$page;
            $jsondata=$value;
            echo json_encode($jsondata);
            exit;

        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
    }

    function details_freelancer (){
        $jsondata = array();
        
        $id_dni = json_decode($_POST['id'],true);
        $dni = $id_dni['dni'];

        $value = false;

        try{
            $value = loadModel(MODEL_HOME, "home_model", "details_freelancer",$dni);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //print_r($value);
        //exit;
        if ($value){
            $_SESSION['detailsFree']=$value;
            //index.php?module=home&view=detailsUser
            $jsondata['redirect']="../home/viewDetails/";
            echo json_encode($jsondata);
            exit;

        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }  
    }

    function details_freelancer_list (){
        $jsondata = array();
        //print_r("php ".$_GET['aux']);
        //exit;

        $value = false;

        try{
            $value = loadModel(MODEL_HOME, "home_model", "details_freelancer", $_GET['aux']);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //print_r($value);
        //exit;
        if ($value){
            $jsondata['val']=$value;
            echo json_encode($jsondata);
            exit;

        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }  
    }

    function insert_like ($model1, $model2){
        $jsondata = array();
        
        $data = json_decode($_POST['data'],true);
        //echo json_encode($data);
        //exit;
        $value = false;

        try {
            $value = loadModel(MODEL_HOME, "home_model", $model1, $data);

        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
            //print_r($value[0]['count']);
            //exit;
        if ($value[0]['count']==0) {
            try{
                $value = loadModel(MODEL_HOME, "home_model", $model2, $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }
            //echo json_encode($value);
            //exit;
            if ($value){
                $jsondata['success']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }else{
            $jsondata['msg']="No puedes dar me gusta dos veces al mismo usuario";
            echo json_encode($jsondata);
            exit;

        }

        
    }

    function stop_like ($model){
        $jsondata = array();
        
        $data = json_decode($_POST['data'],true);
        //echo json_encode($data);
        //exit;
        $value = false;
        
        try {
            $value = loadModel(MODEL_HOME, "home_model", $model, $data);

        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }

        if ($value) {
            $jsondata['success']=true;
            echo json_encode($jsondata);
            exit;

        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }

        
    }