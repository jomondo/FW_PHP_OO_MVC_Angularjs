function paintCategories(i, j, data){
    if (data[i]==undefined) {
		return false;

	}
    $("div#categories").append('<div class="col-md-5 ser_grid1 category" id="'+data[i].categ_prof+'" style="margin-left:100px;"><img src="https://localhost/freelancer/media/service1.jpg" alt="" id="clickCategory"/><div class="ser_info">'
		+'<i class="fa icon fa-suitcase"></i><h4 class="nameCategory'+i+'" id="'+data[i].categ_prof+'">'+data[i].categ_prof+'</h4><i class="fa fa-plus"></i><div class="clearfix"></div></div>'
		+'<p>Lorem ipsum dolor sit amet augueit, consectet adipiscing elit. Quisque utrisus eget libero finibus sollicitudin quisi mauris.</p></div>');
    if (data[j]==undefined) {
		return false;

	}
    $("div#categories").append('<div class="col-md-5 ser_grid1 category" id="'+data[j].categ_prof+'" style="margin-left:100px;"><img src="https://localhost/freelancer/media/service1.jpg" alt="" id="clickCategory"/><div class="ser_info">'
		+'<i class="fa icon fa-suitcase"></i><h4 class="nameCategory'+j+'" id="'+data[j].categ_prof+'">'+data[j].categ_prof	+'</h4><i class="fa fa-plus"></i><div class="clearfix"></div></div>'
		+'<p>Lorem ipsum dolor sit amet augueit, consectet adipiscing elit. Quisque utrisus eget libero finibus sollicitudin quisi mauris.</p></div>');

}

function paintListFreelancer(json,name_list){
    $(".list-group-item").remove();
	json.data.forEach(function(elemento){           
		//alert(elemento.dni);
		$("div#"+name_list).append('<a href="#" class="list-group-item"><div class="media col-md-3"><figure class=""><img class="media-object img-rounded img-responsive" src="'+elemento.avatar+'" alt="" height="150" width="150"></figure>'
			+'</div><div class="col-md-6"><h4 class="list-group-item-heading list_cat" id="'+elemento.categ_prof+'">'+elemento.categ_prof+'</h4><p class="list-group-item-text list_esp" id="'+elemento.especialista+'">Especialista: '+elemento.especialista+'</p></div>'
			+'<div class="col-md-3 "><button type="button" class="btn btn-default btn-lg btn-block buttonDetails" id="'+elemento.dni+'">Details </button></div></a>');

	});

}

function paintListHome(json,name_list){
    $("#block1").append('<div class="w3l-info1" ><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[0].avatar+'" alt=""/></div><div class="col-md-7 team-grid-2">'
		+'<h4>'+json[0].name+'</h4><h5>'+json[0].categ_prof+'</h5><p>'+json[0].especialista+'</p>'
		+'<button id="'+json[0].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
		+'<div class="clearfix"></div></div><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[2].avatar+'" alt=""/></div><div class="col-md-7 team-grid-2">'
		+'<h4>'+json[2].name+'</h4><h5>'+json[2].categ_prof+'</h5><p>'+json[2].especialista+'</p>'
		+'<button id="'+json[2].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
		+'<div class="clearfix"></div></div><div class="clearfix"></div></div>');
    $("#block2").append('<div class="w3l-info1" ><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[1].avatar+'" alt="" /></div><div class="col-md-7 team-grid-2">'
		+'<h4>'+json[1].name+'</h4><h5>'+json[1].categ_prof+'</h5><p>'+json[1].especialista+'</p>'
		+'<button id="'+json[1].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
		+'<div class="clearfix"></div></div><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[4].avatar+'" alt="" /></div><div class="col-md-7 team-grid-2">'
		+'<h4>'+json[4].name+'</h4><h5>'+json[4].categ_prof+'</h5><p>'+json[4].especialista+'</p>'
		+'<button id="'+json[4].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
		+'<div class="clearfix"></div></div><div class="clearfix"></div></div>');
    $("li#block3").append('<div class="w3l-info1" ><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[3].avatar+'" alt="" /></div><div class="col-md-7 team-grid-2">'
        +'<h4>'+json[3].name+'</h4><h5>'+json[3].categ_prof+'</h5><p>'+json[3].especialista+'</p>'
        +'<button id="'+json[3].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
        +'<div class="clearfix"></div></div><div class="col-md-6"><div class="col-md-5 team-grid-1"><img src="'+json[5].avatar+'" alt="" /></div><div class="col-md-7 team-grid-2">'
        +'<h4>'+json[5].name+'</h4><h5>'+json[5].categ_prof+'</h5><p>'+json[5].especialista+'</p>'
        +'<button id="'+json[5].dni+'" class="btn btn-info buttonDetails">Details</button></div>'
        +'<div class="clearfix"></div></div><div class="clearfix"></div></div>');
}

function paintStat_Home(count,name_list){
	if (name_list=="countClient") {
		$("#countClient").append(count);
		
	}

	if (name_list=="countFreelancer") {
		$("#countFreelancer").append(count);
		
	}
	

}

function paintBestClient(json){
	$("img#client1").attr("src",json[0].avatar);
	$("h4#client1").append(json[0].name);
	$("p#client1").append(json[0].message);

	$("img#client2").attr("src",json[1].avatar);
	$("h4#client2").append(json[1].name);
	$("p#client2").append(json[1].message);

	$("img#client3").attr("src",json[2].avatar);
	$("h4#client3").append(json[2].name);
	$("p#client3").append(json[2].message);

}
