$(document).ready(function() {
	$.ajax({
		url: '../../home/paintDetails/',
		type: 'GET',
		success:function(response){
 			//alert(response);
 			var json = JSON.parse(response);
 			//alert(json[0].avatar);
 			$("div#imgAvatar").append('<img alt="Avatar" src="'+json[0].avatar+'" class="img-circle img-responsive" width=200 height=200><div style="color:#999;" >Avatar</div>');
 			$("div#nombreUsu").append('<h4 style="color:#00b1b1;">'+json[0].name+' '+ json[0].last_name +'</h4></span><span><p>'+ json[0].categ_prof+'</p></span>');
 			$("div#firstName").append('<p>'+json[0].name+'</p>');
 			$("div#last_name").append('<p>'+json[0].last_name+'</p>');
 			$("div#date_up").append('<p>'+json[0].date_up+'</p>');
 			$("div#specialist").append('<p>'+json[0].especialista+'</p>');
 			
        	json.forEach(function(elemento){
        		var f = elemento.materia.split(":");
        		for(var i=0;i<f.length;i++){
				    if (f[i]!=="") {
				    	$("div#matters").append('<button style="float:left;" class="btn btn-success">'+f[i]+'</button>');
				    }
				    
				}
        		
        	});
 			
 			$("div#email").append('<p>'+json[0].email+'</p>');
 			$("div#phone").append('<p>'+json[0].phone+'</p>');
 			$("div#city").append('<p>'+json[0].city+'</p>');
 			$("div#project").append('<p>'+json[0].proyecto+'</p>');
 			var data = {"city":json[0].city,"dni":json[0].dni}
 			geolocation ("../../home/get_markets/",data, "staticMap");
 			
 			rating_stars_freelancer(json[0].dni);
		},
		error:function(xhr){
			/*alert(xhr.responseText);*/
			if (xhr.responseText.error) {
				amaran(xhr.responseText.error);

			}
			
		},


	});

	
	
});		

function rating_stars_freelancer(dni){
	$( ".submitRating_stars" ).on( "click", function() {
		//alert(dni);
		var rdo = true;

        var stars = $('#rating_details').val();

        if( stars == ""){
            var message = "Seleccione su puntuacion con las estrellas. Minima puntuacion 0.1";
            amaran(message);
            rdo=false;
            return false;

        }

        if (rdo) {
            var data = localStorage.getItem("datos_user");
            var json = JSON.parse(data);
            if (data!==null) {
                if (json.token) {
                    var tokenInit = {"token":json.token};
                    var tokenToInit = JSON.stringify(tokenInit);
                    var data = {"dni_valorado":dni,"stars":stars};
                    var data_rating = JSON.stringify(data);
                    
                    load_data_ajax("?module=login&function=select_user_token","POST",tokenToInit,data_rating,"rating_stars_free");
                    
                }
                
            }

        }//end rdo

	});
}