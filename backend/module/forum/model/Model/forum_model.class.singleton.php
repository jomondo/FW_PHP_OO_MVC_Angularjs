<?php
    class forum_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = forum_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function submit_comment($arrayValue) {
            return $this->bll->submit_comment_BLL($arrayValue);
        }

        public function get_comments() {
            return $this->bll->get_comments_BLL();
        }

        public function comments_grupBy() {
            return $this->bll->comments_grupBy_BLL();
        }

        public function submit_answer($arrayValue) {
            return $this->bll->submit_answer_BLL($arrayValue);
        }

        public function update_karma($arrayValue) {
            return $this->bll->update_karma_BLL($arrayValue);
        }

        public function delete_comment($arrayValue) {
            return $this->bll->delete_comment_BLL($arrayValue);
        }

        public function check_like($arrayValue) {
            return $this->bll->check_like_BLL($arrayValue);
        }

        public function insert_like($arrayValue) {
            return $this->bll->insert_like_BLL($arrayValue);
        }

        public function update_like($arrayValue) {
            return $this->bll->update_like_BLL($arrayValue);
        }

        public function get_likes($arrayValue) {
            return $this->bll->get_likes_BLL($arrayValue);
        }
        
    }