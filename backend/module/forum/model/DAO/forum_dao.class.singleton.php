<?php
class forum_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function submit_comment_DAO($db, $arrayValue) {

        $sql = "INSERT INTO forum ( item, comment, id_user, date, avatar, name, num_like) VALUES ('$arrayValue[item]', '$arrayValue[comment]', '$arrayValue[id_user]', now(), '$arrayValue[avatar]','$arrayValue[name]', 0 )";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function get_comments_DAO($db) {

        $sql = "SELECT * FROM forum";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function comments_grupBy_DAO($db) {

        $sql = "SELECT * FROM forum GROUP BY item";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function submit_answer_DAO($db, $arrayValue) {
        // print_r($arrayValue);
        // exit;

        $sql = "INSERT INTO forum ( item, comment, id_user, date, avatar, name, num_like) VALUES ('$arrayValue[item]','$arrayValue[comment]', '$arrayValue[id_user]', now(), '$arrayValue[avatar]', '$arrayValue[name]', 0 )";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function update_karma_DAO($db, $arrayValue) {
        // print_r($arrayValue);
        // exit;

        $sql = "UPDATE forum SET karma = '".$arrayValue['karma']."' WHERE id_user = '".$arrayValue['id_user']."' ";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function delete_comment_DAO($db, $arrayValue) {
        // print_r($arrayValue);
        // exit;

        $sql = "DELETE FROM forum WHERE id = '".$arrayValue['id_item']."' ";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function check_like_DAO($db, $arrayValue) {

        $sql = "SELECT count(*) as count_item FROM forum_likes WHERE id_item = '".$arrayValue['id_item']."' and id_user = '".$arrayValue['id_user']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt); 
    
    }

    public function insert_like_DAO($db, $arrayValue) {
        // print_r($arrayValue['id_user']);
        // exit;

        $sql = "INSERT INTO forum_likes ( id_item, id_user ) VALUES ( '$arrayValue[id_item]', '$arrayValue[id_user]' )";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function update_like_DAO($db, $arrayValue) {
        // print_r($arrayValue);
        // exit;

        $sql = "UPDATE forum SET num_like = '".$arrayValue['num_like']."' WHERE id = '".$arrayValue['id_item']."' ";

        return $stmt = $db->ejecutar($sql);
        
    }

    public function get_likes_DAO($db, $arrayValue) {
        // print_r($arrayValue);
        // exit;
        $sql = "SELECT * FROM forum WHERE id = '".$arrayValue['id_item']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt); 
    
    }

}
