<?php    
    class forum_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = forum_dao::getInstance();
            $this->db = db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function submit_comment_BLL($arrayValue) {
            return $this->dao->submit_comment_DAO($this->db, $arrayValue);
        }

        public function get_comments_BLL() {
            return $this->dao->get_comments_DAO($this->db);
        }

        public function submit_answer_BLL($arrayValue) {
            return $this->dao->submit_answer_DAO($this->db, $arrayValue);
        }

        public function comments_grupBy_BLL() {
            return $this->dao->comments_grupBy_DAO($this->db);
        }

        public function update_karma_BLL($arrayValue) {
            return $this->dao->update_karma_DAO($this->db,$arrayValue);
        }

        public function delete_comment_BLL($arrayValue) {
            return $this->dao->delete_comment_DAO($this->db,$arrayValue);
        }

        public function check_like_BLL($arrayValue) {
            return $this->dao->check_like_DAO($this->db,$arrayValue);
        }

        public function insert_like_BLL($arrayValue) {
            return $this->dao->insert_like_DAO($this->db,$arrayValue);
        }

        public function update_like_BLL($arrayValue) {
            return $this->dao->update_like_DAO($this->db,$arrayValue);
        }

        public function get_likes_BLL($arrayValue) {
            return $this->dao->get_likes_DAO($this->db,$arrayValue);
        }

        
    }