<?php
	class controller_forum{
		function __construct(){
				
		}

		function submit_comment(){
            $jsondata = array();
            $data = json_decode($_POST["data"], true);
            // echo json_encode($data);
            // exit;
			try{
                $value = loadModel(MODEL_FORUM, "forum_model", "submit_comment", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                $jsondata['success']=true;
            	echo json_encode($jsondata);
            	exit;

            }else{
            	showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

		}

        function get_comments(){
            
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "get_comments");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                echo json_encode($value);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }

        function comments_grupBy(){
            
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "comments_grupBy");

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                echo json_encode($value);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }

        function submit_answer(){
            $jsondata = array();
            $data = json_decode($_POST["data"], true);
            // echo json_encode($data);
            // exit;
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "submit_answer", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                $jsondata['success']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }

        function update_karma(){
            $jsondata = array();
            $data = json_decode($_POST["data"], true);
            // echo json_encode($data);
            // exit;
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "update_karma", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                $jsondata['success']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }

        function delete_comment(){
            $jsondata = array();
            $data = json_decode($_POST["data"], true);
            // echo json_encode($data);
            // exit;
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "delete_comment", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value) {
                $jsondata['success']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }

        function check_like(){
            $jsondata = array();
            $data = json_decode($_POST["data"], true);
            // echo json_encode($data);
            // exit;
            try{
                $value = loadModel(MODEL_FORUM, "forum_model", "check_like", $data);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value[0]['count_item']==0) {

                try{
                    $value_insert = loadModel(MODEL_FORUM, "forum_model", "insert_like", $data);

                }catch(Exception $e){
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                }

                if ($value_insert) {
                    try{
                        $value_like = loadModel(MODEL_FORUM, "forum_model", "get_likes", $data);

                    }catch(Exception $e){
                        showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                    }
                        
                    if ($value_like) {
                        $data['num_like'] = $value_like[0]['num_like'] + 1;
                        
                        try{
                            $value_update = loadModel(MODEL_FORUM, "forum_model", "update_like", $data);

                        }catch(Exception $e){
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                        }

                        if ($value_update) {
                            $jsondata['success']=true;
                            echo json_encode($jsondata);
                            exit;

                        }else{
                            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                        }

                    }else{
                        showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                    }

                    

                }else{
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                }

            }else{
                $jsondata['mgs']= "Ya has dado me gusta al comentario";
                echo json_encode($jsondata);
                exit;

            }

        }
		

	}