<?php
    function validateProfileFreelancerPHP($value) {
        $error = array();
        $rdo = true;
        $filtro = array(
            'dni' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{8}[A-Z]{1}$/')
            ),
            'name' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ]{4,35}$/')
            ),
            'last_name' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ]{4,35}$/')
            ),
            'birthday' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/')
            ),
            'date_up' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/')
            ),
            'email' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{4,12}[.]{1}[A-Za-z]{2,4}$/')
            ),
            'phone' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{9}$/')
            ),
            'price_h' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{1,3}$/')
            ),
            'proyect' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ0-9]{15,500}$/')
            ),
        );


        $result = filter_var_array($value, $filtro);

        if ($result != "" && $result) {
            $result['language']=$value['language'];
            $result['category']=$value['category'];
            $result['matters']=$value['matters'];
            $result['specialist']=$value['specialist'];
            $result['community']=$value['community'];
            $result['province']=$value['province'];
            $result['city']=$value['city'];
            $result['id']=$value['id'];

            if (!$result['dni']) {
                $error['dni'] = 'El dni tiene que tener 8 numeros y una letra';
                $rdo = false;
                
            }
            if (!$result['name']) {
                $error['name'] = 'El nombre tiene que tener entre 4-35 caracteres';
                $rdo = false;
            }
            if (!$result['last_name']) {
                $error['last_name'] = 'Los apellidos tiene que tener entre 4-35 caracteres';
                $rdo = false;
            }

            if (!$result['birthday']) {
                $error['birthday'] = 'La fecha tiene que tener el siguente formato dd/mm/aaaa';
                $rdo = false;
            }

            if (!$result['date_up']) {
                $error['date_up'] = 'La fecha tiene que tener el siguente formato dd/mm/aaaa';
                $rdo = false;
            }

            if ( $result['birthday'] && $result['date_up'] ) {
                $rdoDate = compararFechas($result['birthday'],$result['date_up']);
                if ($rdoDate==0 || $rdoDate>0) {
                    $error['errorCompare'] = "Error, la fecha dni es inferior o igual a la fecha de nacimiento";
                    $rdo = false;

                }else{
                    $rdo = true;

                }
            }
            
            if (!$result['email']) {
                $error['email'] = 'Tiene que ser un email valido. ejemplo@gmail.com';
                $rdo = false;
            }
            if (!$result['phone']) {
                $error['phone'] = 'El telefono tiene que tener 9 numeros';
                $rdo = false;
            }

            if (!$result['price_h']) {
                $error['price_h'] = 'precio/h tiene que tener entre 1 y 3 numeros';
                $rdo = false;
            }

            if ($result['language']=="") {
                $error['language'] = 'Selecciona un idioma';
                $rdo = false;
            }
            if ($result['category']=="") {
                $error['category'] = 'Selecciona una categoria';
                $rdo = false;
            }
            if ($result['matters']=="") {
                $error['matters'] = 'Selecciona un materia como minimo';
                $rdo = false;
            }
            if ($result['specialist']=="") {
                $error['specialist'] = 'Selecciona una especialidad';
                $rdo = false;
            }
            if (!$result['proyect']) {
                $error['proyect'] = 'Esta caja de proyecto tiene que tener entre 15-500 caracteres';
                $rdo = false;
            }
            
            if ($result['community']==='Select community'){
                $error['community']="Select a community";
                $rdo = false;
            }

            if ($result['province']==='Select province'){
                $error['province']="Select a province";
                $rdo = false;
            }

            if ($result['city']==='Select city'){
                $error['city']="Select a city";
                $rdo = false;
            }
            
            if ($result['id']===""){
                $error['id']="id empty";
                $rdo = false;
            }

            if ($result['dni']) {
               $rdoDni = validarDniFreelancer($result['dni']);
               if ($rdoDni==false) {
                    $error['validaDni'] = "El dni introducido no es correcto";
                    $rdo = false;

               }

            }

        }else {
            $rdo = false;
        }

        return $return = array('result' => $rdo, 'error' => $error, 'datos' => $result);
    }

    /* https://www.jose-aguilar.com/blog/comparar-fechas-con-php/ */
    function compararFechas($primera, $segunda){
        $valoresPrimera = explode ("/", $primera);   
        $valoresSegunda = explode ("/", $segunda); 

        $diaPrimera    = $valoresPrimera[0];  
        $mesPrimera  = $valoresPrimera[1];  
        $anyoPrimera   = $valoresPrimera[2]; 

        $diaSegunda   = $valoresSegunda[0];  
        $mesSegunda = $valoresSegunda[1];  
        $anyoSegunda  = $valoresSegunda[2];

        $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
        $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     

        if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
            echo json_encode("La fecha ".$primera." no es v&aacute;lida");
            return false;
        }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
            echo json_encode("La fecha ".$segunda." no es v&aacute;lida");
            return false;
        }else{
            return $diasPrimeraJuliano - $diasSegundaJuliano;
            
        } 

    }

    function validarDniFreelancer($dni){
        $letra = substr($dni, -1);
        $numeros = substr($dni, 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
            return $rdo=true;

        }else{
            return $rdo=false;

        }
    }