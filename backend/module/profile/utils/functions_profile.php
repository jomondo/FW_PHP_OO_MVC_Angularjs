<?php
	require_once(UTILS_PROFILE."validatePHP.php");
    require_once(UTILS_PROFILE."validateFreelancerPHP.php");
    require_once(UTILS_PROFILE."upload.php");

    function getData($model){
        $jsondata = array();
            
        $value = false;

        try{
            $value = loadModel(MODEL_PROFILE, "profile_model", $model);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            echo json_encode($value);
            exit;
                
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        } 
            
    }