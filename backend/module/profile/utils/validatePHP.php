<?php
    function validateProfileClientPHP($value) {
        $error = array();
        $rdo = true;
        $filtro = array(
            'name' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ]{4,35}$/')
            ),
            'last_name' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ]{4,35}$/')
            ),
            'birthday' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{1,2}(\/)[0-9]{1,2}(\/)[0-9]{4}$/')
            ),
            'exp_dni' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{1,2}(\/)[0-9]{1,2}(\/)[0-9]{4}$/')
            ),
            'email' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{4,12}[.]{1}[A-Za-z]{2,4}$/')
            ),
            'phone' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[0-9]{9}$/')
            ),
        );


        $result = filter_var_array($value, $filtro);

        if ($result != "" && $result) {
            $result['language']=$value['language'];
            $result['community']=$value['community'];
            $result['province']=$value['province'];
            $result['city']=$value['city'];


            if (!$result['name']) {
                $error['name'] = 'El nombre tiene que tener entre 4-35 caracteres';
                $rdo = false;
            }
            if (!$result['last_name']) {
                $error['last_name'] = 'Los apellidos tiene que tener entre 4-35 caracteres';
                $rdo = false;
            }
            
            if (!$result['birthday']) {
                $error['birthday'] = 'La fecha tiene que tener el siguente formato dd/mm/aaaa';
                $rdo = false;
            }

            if (!$result['exp_dni']) {
                $error['exp_dni'] = 'La fecha de expiracion tiene que tener el siguente formato dd/mm/aaaa';
                $rdo = false;
            }

            if ( $result['birthday'] && $result['exp_dni'] ) {
                $rdoDate = compararFechasFreelancer($result['birthday'],$result['exp_dni']);
                if ($rdoDate==0 || $rdoDate>0) {
                    $error['errorCompare'] = "Error, la fecha dni es inferior o igual a la fecha de nacimiento";
                    $rdo = false;

                }else{
                    $rdo = true;

                }
            }
            
            if (!$result['email']) {
                $error['email'] = 'Tiene que ser un email valido. ejemplo@gmail.com';
                $rdo = false;
            }
            if (!$result['phone']) {
                $error['phone'] = 'El telefono tiene que tener 9 numeros';
                $rdo = false;
            }

            if ($result['language']=="") {
                $error['language'] = 'Selecciona un idioma';
                $rdo = false;
            }

            if ($result['community']==='Select community'){
                $error['community']="Select a community";
                $rdo = false;
            }

            if ($result['province']==='Select province'){
                $error['province']="Select a province";
                $rdo = false;
            }

            if ($result['city']==='Select city'){
                $error['city']="Select a city";
                $rdo = false;
            }


        }else {
            $rdo = false;

        }

        return $return = array('result' => $rdo, 'error' => $error, 'datos' => $result);
    }


/* https://www.jose-aguilar.com/blog/comparar-fechas-con-php/ */
    function compararFechasFreelancer($primera, $segunda){
        $valoresPrimera = explode ("/", $primera);  
        $valoresSegunda = explode ("/", $segunda); 

        $diaPrimera    = $valoresPrimera[1];  
        $mesPrimera  = $valoresPrimera[0];  
        $anyoPrimera   = $valoresPrimera[2]; 

        $diaSegunda   = $valoresSegunda[1];  
        $mesSegunda = $valoresSegunda[0];  
        $anyoSegunda  = $valoresSegunda[2];

        $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
        $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     

        if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
            echo json_encode("La fecha ".$primera." no es v&aacute;lida");
            return false;
        }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
            echo json_encode("La fecha ".$segunda." no es v&aacute;lida");
            return false;
        }else{
            return $diasPrimeraJuliano - $diasSegundaJuliano;
            
        } 

    }

   /* function validarDni($dni){
        $letra = substr($dni, -1);
        $numeros = substr($dni, 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
            return $rdo=true;

        }else{
            return $rdo=false;

        }
    }*/

