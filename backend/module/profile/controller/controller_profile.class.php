<?php
class controller_profile{
    function __construct() {
        
    }

    function begin() {
        //print_r("begin");
        //exit;
        loadView("profile","profile");
        
    }

    function resultUser() {
        //print_r("resultUser");
        //exit;
        loadView("profile","resultUser");
        
    }

    function paintUsers() {
        //print_r("paintUsers");
        //exit;
        $jsondata = array();
        if (isset($_SESSION['users'])) {
            $jsondata["users"] = $_SESSION['users'];
        }
        
        close_session();
        echo json_encode($jsondata);
        exit;
    }

    function security() {
        //print_r("security");
        //exit;
        $jsondata = array();
        if (isset($_SESSION['user'])) {
            $jsondata["user"] = $_SESSION['user'];
            echo json_encode($jsondata);
            exit;
        }else{
            $jsondata["user"] = null;
            echo json_encode($jsondata);
            exit;
        }

    }

    function alta_profile_client(){
        $jsondata = array();
        $usersJSON = json_decode($_POST["alta_profile_json"], true);
        $rdo = validateProfileClientPHP($usersJSON);
        //echo json_encode ($rdo);
        //exit;

        if (empty($_SESSION['result_avatar'])) {//si l'usuari no ha escollit ninguna imtage
            $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => '/freelancer/media/dropzone/img_default.jpg');
        }
        $result_avatar = $_SESSION['result_avatar'];

        if ($rdo['result'] &&  $result_avatar['resultado'] ) {
            //comprovar si ja existeix en db
            /* li passem l'adreça, obtinguem les coordenades i se les guardem en db */
            $content=array();

            $cadena = str_replace("_","",$rdo['datos']['city']);
            $cad = normaliza($cadena);
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA7pK8OLDa9EXwpugryUfoX1-yKoaoJBiE&address='.strtolower($cad).'&sensor=true';

            $content = loadModel(MODEL_PROFILE, "profile_model", "get_community", $url);//obtain coordenadas
            $decode = json_decode($content, true);
            //print_r($decode['results'][0]['geometry']['bounds']['northeast']['lat'].$decode['results'][0]['geometry']['bounds']['northeast']['lng']);
            //exit;
            if ($content) {
                $lat = $decode['results'][0]['geometry']['bounds']['northeast']['lat'];
                $lng = $decode['results'][0]['geometry']['bounds']['northeast']['lng'];
                $arrayVal = array('dni' =>$rdo['datos']['dni'], 'city' =>$rdo['datos']['city'], 'lat' =>$lat,'lng' =>$lng);

                $content = loadModel(MODEL_PROFILE, "profile_model", "insert_coordenadas", $arrayVal);
            }

            $arrayData = array(
                'id' =>$rdo['datos']['id'],
                'dni' =>$rdo['datos']['dni'],
                'name' =>$rdo['datos']['name'],
                'last_name' => $rdo['datos']['last_name'],
                'birthday' => $rdo['datos']['birthday'],
                'exp_dni' => $rdo['datos']['exp_dni'],
                'language' => $rdo['datos']['language'],
                'email' => $rdo['datos']['email'],
                'phone' =>$rdo['datos']['phone'],
                'avatar' =>$result_avatar['datos'],
                'community' =>$rdo['datos']['community'],
                'province' =>$rdo['datos']['province'],
                'city' =>$rdo['datos']['city'],
                'type' =>$usersJSON['type']
            );

            ///////////////// Insert to database ////////////////////////
            $value = false;
            //print_r($arrayData);
            //exit;
            $value = loadModel(MODEL_PROFILE, "profile_model", "create_profile", $arrayData);
            
            if ($value){
                $_SESSION['users'] = $arrayData;

                $jsondata["success"] = true;//index.php?module=profile&view=resultUser
                $jsondata["redirect"] = "../profile/resultUser/";
                echo json_encode($jsondata);
                exit;
            }else{
                $jsondata["msg"] = "Ha ocurrido un error inesperado en nuestra base de datos. Perdone las molestias.";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

            }
            
        }else{
            $jsondata["error"] = $rdo['error'];
            header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit;

        }

    }

    function update_profile_client(){
        //echo json_encode ("entra");
        //exit;
        $jsondata = array();
        $usersJSON = json_decode($_POST["data"], true);
        $rdo = validateProfileClientPHP($usersJSON);
        //echo json_encode ($rdo);
        //exit;

        if (empty($_SESSION['result_avatar'])) {//si l'usuari no ha escollit ninguna imtage
            //$_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => '/freelancer/media/dropzone/img_default.jpg');
            $content = loadModel(MODEL_PROFILE, "profile_model", "get_avatar", $usersJSON['id']);
            //echo json_encode ($content);
            //exit;
            if ($content) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => $content[0]['avatar']);

            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

        }
        $result_avatar = $_SESSION['result_avatar'];
        //echo json_encode ($result_avatar);
        //exit;
        if ($rdo['result'] &&  $result_avatar['resultado'] ) {
            //echo json_encode ("entra resultado");
            //exit;
            //comprovar si ja existeix en db
            /* li passem l'adreça, obtinguem les coordenades i se les guardem en db */
            //echo json_encode ("entra en rdo");
            //exit;
            /*$content=array();

            $cadena = str_replace("_","",$rdo['datos']['city']);
            $cad = normaliza($cadena);
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA7pK8OLDa9EXwpugryUfoX1-yKoaoJBiE&address='.strtolower($cad).'&sensor=true';

            $content = loadModel(MODEL_PROFILE, "profile_model", "get_community", $url);//obtain coordenadas
            $decode = json_decode($content, true);
            //print_r($decode['results'][0]['geometry']['bounds']['northeast']['lat'].$decode['results'][0]['geometry']['bounds']['northeast']['lng']);
            //exit;
            if ($content) {
                $lat = $decode['results'][0]['geometry']['bounds']['northeast']['lat'];
                $lng = $decode['results'][0]['geometry']['bounds']['northeast']['lng'];
                $arrayVal = array('dni' =>$rdo['datos']['dni'], 'city' =>$rdo['datos']['city'], 'lat' =>$lat,'lng' =>$lng);

                $content = loadModel(MODEL_PROFILE, "profile_model", "insert_coordenadas", $arrayVal);
            }*/

            $arrayData = array(
                'id' =>$usersJSON['id'],
                'name' =>$rdo['datos']['name'],
                'last_name' => $rdo['datos']['last_name'],
                'birthday' => $rdo['datos']['birthday'],
                'exp_dni' => $rdo['datos']['exp_dni'],
                'language' => $rdo['datos']['language'],
                'email' => $rdo['datos']['email'],
                'phone' =>$rdo['datos']['phone'],
                'avatar' =>$result_avatar['datos'],
                'community' =>$rdo['datos']['community'],
                'province' =>$rdo['datos']['province'],
                'city' =>$rdo['datos']['city'],
                'table' =>"user_register"
            );

            ///////////////// Insert to database ////////////////////////
            $value = false;
            //print_r($arrayData);
            //exit;
            $value = loadModel(MODEL_PROFILE, "profile_model", "update_profile_client", $arrayData);
            //print_r($value);
            //exit;
            if ($value){
                close_session();
                $jsondata["success"] = true;
                $jsondata["msg"] = "Sus datos han sido actualizados";
                echo json_encode($jsondata);
                exit;
            }else{
                $jsondata["msg"] = "Ha ocurrido un error inesperado en nuestra base de datos. Perdone las molestias.";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

            }
            
        }else{
            $jsondata["error"] = $rdo['error'];
            header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit;

        }

    }

    function alta_profile_freelancer(){
        $jsondata = array();
        $usersJSON = json_decode($_POST["alta_profileFreelancer_json"], true);
        //echo json_encode($usersJSON);
        //exit;
        $rdo = validateProfileFreelancerPHP($usersJSON);

        if (empty($_SESSION['result_avatar'])) {//si l'usuari no ha escollit ninguna imtage
            $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => '/freelancer/media/dropzone/img_default.jpg');
        }
        $result_avatar = $_SESSION['result_avatar'];

        if ($rdo['result'] &&  $result_avatar['resultado'] ) {

            $content=array();

            $cadena = str_replace("_","",$rdo['datos']['city']);
            $cad = normaliza($cadena);
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA7pK8OLDa9EXwpugryUfoX1-yKoaoJBiE&address='.strtolower($cad).'&sensor=true';

            $content = loadModel(MODEL_PROFILE, "profile_model", "get_community", $url);//obtain coordenadas
            $decode = json_decode($content, true);
            //print_r($decode['results'][0]['geometry']['bounds']['northeast']['lat'].$decode['results'][0]['geometry']['bounds']['northeast']['lng']);
            //exit;
            if ($content) {
                $lat = $decode['results'][0]['geometry']['bounds']['northeast']['lat'];
                $lng = $decode['results'][0]['geometry']['bounds']['northeast']['lng'];
                $arrayVal = array('dni' =>$rdo['datos']['dni'],'city' =>$rdo['datos']['city'], 'lat' =>$lat,'lng' =>$lng);

                $content = loadModel(MODEL_PROFILE, "profile_model", "insert_coordenadas", $arrayVal);
            }
            
            $arrayData = array(
                'id' =>$rdo['datos']['id'],
                'dni' =>$rdo['datos']['dni'],
                'name' =>$rdo['datos']['name'],
                'last_name' => $rdo['datos']['last_name'],
                'birthday' => $rdo['datos']['birthday'],
                'date_up' => $rdo['datos']['date_up'],
                'language' => $rdo['datos']['language'],
                'category' => $rdo['datos']['category'],
                'matters' => $rdo['datos']['matters'],
                'specialist' => $rdo['datos']['specialist'],
                'proyect' => $rdo['datos']['proyect'],
                'email' => $rdo['datos']['email'],
                'phone' =>$rdo['datos']['phone'],
                'price_h' =>$rdo['datos']['price_h'],
                'community' =>$rdo['datos']['community'],
                'province' =>$rdo['datos']['province'],
                'city' =>$rdo['datos']['city'],
                'avatar' =>$result_avatar['datos'],
                'type' =>$usersJSON['type']
            );

            ///////////////// Insert to database ////////////////////////
            $value = false;
            //print_r($arrayData);
            //exit;
            $value = loadModel(MODEL_PROFILE, "profile_model", "create_profile", $arrayData);

            if ($value){
                $_SESSION['users'] = $arrayData;
                //$callback = "index.php?module=secondHandProducts&view=resultProducts";

                $jsondata["success"] = true;
                $jsondata["redirect"] = "../profile/resultUser/";//->>>>>>>>>>>> !!!
                echo json_encode($jsondata);
                exit;
            }else{
                $jsondata["msg"] = "Ha ocurrido un error inesperado en nuestra base de datos. Perdone las molestias.";
                header('HTTP/1.0 400 Bad error');//peta i se'n va al fail
                echo json_encode($jsondata);
                exit;

            }
            
        }else{
            $jsondata["error"] = $rdo['error'];
            header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit;

        }

    }



    function load_community(){
        $content=array();

        $url = 'http://apiv1.geoapi.es/comunidades?type=JSON&key=d6fecb835906853c9987261e9ecf51594cb6f43500b134d83e26cb9bd2dc11f3&sandbox=1';
        $content = loadModel(MODEL_PROFILE, "profile_model", "get_community", $url);
        //print_r($content);
        //exit;
        if ($content) {
            echo $content;
            exit;
        }else{
            $content="error";
            echo $content;
            exit;
        }

    }

    function load_provinces(){
        $community_json = json_decode($_POST["data"], true);
        //print_r($community_json);
        //exit;
        $content=array();
        $content = loadModel(MODEL_PROFILE, "profile_model", "get_provinces", $community_json);

        if($content){
            echo $content;
            exit;
        }else{
            $content="error";
            echo $content;
            exit;
        }

    }

    function load_cities(){
        $province_json = json_decode($_POST["data"], true);
        $content=array();
        
        $content = loadModel(MODEL_PROFILE, "profile_model", "get_cities", $province_json);

        if($content){
            echo $content;
            exit;
        }else{
            $content="error";
            echo $content;
            exit;
        }
        
    }

    function upload(){
        $jsondata = array();
        $result_avatar = upload_files();
        //print_r($result_avatar);
        //exit;
        if (!$result_avatar['resultado']) {
            $jsondata['error']=$result_avatar['error'];
            header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit;
        }
        $_SESSION['result_avatar'] = $result_avatar;

    }

    function delete(){
        $result = remove_file();
        //print_r($result);
        //exit;
        if($result === true){
            $_SESSION['result_avatar'] = array();
            echo json_encode(array("res" => true));
        }else{
            echo json_encode(array("res" => false));
        }

    }

    function check_type(){
        //echo json_encode($_POST['data']);
        //exit;
        $jsondata = array();

        $usersJSON = json_decode($_POST["data"], true);
        $arrayValues = array("id"=>$usersJSON['id'],"table"=>"cliente");

        try {
            $content = loadModel(MODEL_PROFILE, "profile_model", "count_user", $arrayValues);

        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        if ($content) {
            if ($content[0]['count']==1) {
                $jsondata['users']=true;
                echo json_encode($jsondata);
                exit;

            }else{
                //$usersJSON = json_decode($_POST["data"], true);
                $arrayValues = array("id"=>$usersJSON['id'],"table"=>"autonomo");

                try {
                    $content = loadModel(MODEL_PROFILE, "profile_model", "count_user", $arrayValues);

                } catch (Exception $e) {
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

                }

                if ($content) {
                    if ($content[0]['count']==1) {
                        $jsondata['users']=true;
                        echo json_encode($jsondata);
                        exit;

                    }else{
                        $jsondata['users']=false;
                        echo json_encode($jsondata);
                        exit;

                    }
                }else{
                    showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                }
            }
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        }
        
    }

    function get_data_user(){
        //echo json_encode($_POST['data']);
        //exit;
        $jsondata = array();

        $usersJSON = json_decode($_POST["data"], true);
        $arrayValues = array("id"=>$usersJSON['id'],"table"=>"autonomo");

        try{
            $value = loadModel(MODEL_PROFILE, "profile_model", "get_data_user", $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            $jsondata['success']=true;
            $jsondata['value']=$value;
            echo json_encode($jsondata);
            exit;
                
        }else{
            $arrayValues = array("id"=>$usersJSON['id'],"table"=>"cliente");

            try{
                $value = loadModel(MODEL_PROFILE, "profile_model", "get_data_user", $arrayValues);

            }catch(Exception $e){
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

            }

            if ($value){
                $jsondata['success']=true;
                $jsondata['value']=$value;
                echo json_encode($jsondata);
                exit;
                
            }else{
                showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                
            }

        } 

    }

    function get_like(){
        //echo json_encode("get_like");
        //exit;
        getData("get_likes");
            
        }

   /* function get_data_client(){
        //echo json_encode($_POST['data']);
        //exit;
        $jsondata = array();

        $arrayValues = array("id"=>$_GET["aux"],"table"=>"user_register");

        try{
            $value = loadModel(MODEL_PROFILE, "profile_model", "get_data_client", $arrayValues);

        }catch(Exception $e){
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

        }
        //echo json_encode($value);
        //exit;
        if ($value){
            $jsondata['success']=true;
            $jsondata['value']=$value;
            echo json_encode($jsondata);
            exit;
                
        }else{
            showErrorPage(0, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            


        } 

    }*/

}