<?php
    class profile_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = profile_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function create_profile($arrArgument) {
            return $this->bll->create_profile_BLL($arrArgument);
        }
        
        public function list_profile() {
            return $this->bll->list_profile_BLL();
        }

        public function get_community($url){
            return $this->bll->get_community_BLL($url);
        }

        public function get_provinces($id){
            return $this->bll->get_provinces_BLL($id);
        }

        public function get_cities($value){
            return $this->bll->get_cities_BLL($value);
        }
        
        public function insert_coordenadas($value){
            return $this->bll->insert_coordenadas_BLL($value);
        }

        public function count_user($value){
            return $this->bll->count_user_BLL($value);
        }

        public function get_data_user($value){
            return $this->bll->get_data_user_BLL($value);
        }

        public function get_avatar($value){
            return $this->bll->get_avatar_BLL($value);
        }

        public function update_profile_client($value){
            return $this->bll->update_profile_client_BLL($value);
        }

        public function get_data_client($value){
            return $this->bll->get_data_client_BLL($value);
        }

        public function get_likes(){
            return $this->bll->get_likes_BLL();
        }


    }