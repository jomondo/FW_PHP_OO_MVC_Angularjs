<?php
    class profile_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = profile_dao::getInstance();
            $this->db = db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function create_profile_BLL($arrArgument) {
            return $this->dao->create_profile_DAO($this->db, $arrArgument);
        }
        
        public function list_profile_BLL() {
            return $this->dao->select_profile_DAO($this->db);
        }

        public function get_community_BLL($url) {
            return $this->dao->get_community_DAO($url);
        }
        public function get_provinces_BLL($id) {
            return $this->dao->get_provinces_DAO($id);
        }
        public function get_cities_BLL($value) {
            return $this->dao->get_cities_DAO($value);
        }

        public function insert_coordenadas_BLL($value) {
            return $this->dao->insert_coordenadas_DAO($this->db, $value);
        }

        public function count_user_BLL($value) {
            return $this->dao->count_user_DAO($this->db, $value);
        }

        public function get_avatar_BLL($value) {
            return $this->dao->get_avatar_DAO($this->db, $value);
        }

        public function get_data_user_BLL($value) {
            return $this->dao->get_data_user_DAO($this->db, $value);
        }

        public function update_profile_client_BLL($value) {
            return $this->dao->update_profile_client_DAO($this->db, $value);
        }

        public function get_data_client_BLL($value) {
            return $this->dao->get_data_client_DAO($this->db, $value);
        }

        public function get_likes_BLL() {
            return $this->dao->get_likes_DAO($this->db);
        }

        
    }