<?php
class profile_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function update_profile_client_DAO($db, $value) {
        
        $language = $value['language'];
        $lang="";
        foreach ((array)$language as $indice) {
            $lang .= $indice.":";
                
        }
// Textos completos     id_user     name    passwd  email   avatar  type    token   date    activate    last_name   birthday    exp_dni     language    phone   community   province    city
        //print_r($value);
        //exit;
        $sql = "UPDATE ".$value['table']." SET name = '".$value['name']."', last_name = '".$value['last_name']."', birthday = '".$value['birthday']."', exp_dni = '".$value['exp_dni']."', avatar = '".$value['avatar']."', email = '".$value['email']."', phone = '".$value['phone']."', community = '".$value['community']."', province = '".$value['province']."', city = '".$value['city']."', language = '".$lang."' WHERE id_user = '".$value['id']."' ";
            
        return $db->ejecutar($sql);
        
    }

    public function create_profile_DAO($db, $arrArgument) {

        if ($arrArgument['type']=="Client") {
            $lang="";

            $id_user = $arrArgument['id'];
            $dni = $arrArgument['dni'];
            $name = $arrArgument['name'];
            $last_name = $arrArgument['last_name'];
            $birthday = $arrArgument['birthday'];
            $exp_dni = $arrArgument['exp_dni'];
            $language = $arrArgument['language'];
            $email = $arrArgument['email'];
            $phone = $arrArgument['phone'];
            $avatar = $arrArgument['avatar'];
            $community = $arrArgument['community'];
            $province = $arrArgument['province'];
            $city = $arrArgument['city'];

            foreach ($language as $indice) {
                $lang .= $indice.":";
                
            }
            //$sql = "UPDATE user_register SET avatar='".$avatar."' WHERE id_user='".$id_user."'";
            //$db->ejecutar($sql);
            //print_r($id_user);
            //exit;
            //     dni     name    last_name   fech_nac    idioma  avatar  email   phone   country     province    city
            $sql = "INSERT INTO cliente ( id_user, dni, name, last_name, fech_nac, expiration_dni, idioma, avatar, email, phone, community, province, city  )" 
                    ."VALUES ('$id_user', '$dni', '$name', '$last_name','$birthday','$exp_dni', '$lang', '$avatar', '$email','$phone', '$community','$province','$city')";
            
            return $db->ejecutar($sql);
            
        }else{
            $lang="";
            $matter="";

            $id_user = $arrArgument['id'];
            $dni = $arrArgument['dni'];
            $name = $arrArgument['name'];
            $last_name = $arrArgument['last_name'];
            $birthday = $arrArgument['birthday'];
            $date_up = $arrArgument['date_up'];
            $language = $arrArgument['language'];
            $category = $arrArgument['category'];
            $matters = $arrArgument['matters'];
            $specialist = $arrArgument['specialist'];
            $proyect = $arrArgument['proyect'];
            $email = $arrArgument['email'];
            $phone = $arrArgument['phone'];
            $price_h = $arrArgument['price_h'];
            $avatar = $arrArgument['avatar'];
            $community = $arrArgument['community'];
            $province = $arrArgument['province'];
            $city = $arrArgument['city'];

            foreach ($language as $indice) {
                $lang .= $indice.":";
                
            }
            foreach ($matters as $indice) {
                $matter .= $indice.":";
                
            }
           
            $sql = "INSERT INTO autonomo ( id_user, dni, name, last_name, birthday, date_up, idioma, categ_prof, especialista, materia, proyecto, avatar, email, phone, community, province, city, price_h )" 
                    ."VALUES ('$id_user', '$dni', '$name', '$last_name','$birthday', '$date_up', '$lang', '$category', '$specialist','$matter','$proyect','$avatar','$email','$phone','$community','$province','$city','$price_h')";
            
            return $db->ejecutar($sql);

        }
        
    }

    public function get_community_DAO($url){
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $file_contents = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $accepted_response = array(200, 301, 302);
        if(!in_array($httpcode, $accepted_response)){
            return FALSE;
        }else{
            return ($file_contents) ? $file_contents : FALSE;
        }
    }

    public function get_provinces_DAO($id){
        //print_r("dao: ".$id);
        //exit;
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, 'http://apiv1.geoapi.es/provincias?CCOM='.$id.'&type=JSON&key=d6fecb835906853c9987261e9ecf51594cb6f43500b134d83e26cb9bd2dc11f3&sandbox=1');
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $file_contents = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $accepted_response = array(200, 301, 302);
        if(!in_array($httpcode, $accepted_response)){
            return FALSE;
        }else{
            return ($file_contents) ? $file_contents : FALSE;
        }

    }

    public function get_cities_DAO($id_prov){
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, 'http://apiv1.geoapi.es/municipios?CPRO='.$id_prov.'&type=JSON&key=d6fecb835906853c9987261e9ecf51594cb6f43500b134d83e26cb9bd2dc11f3&sandbox=1');
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $file_contents = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $accepted_response = array(200, 301, 302);
        if(!in_array($httpcode, $accepted_response)){
            return FALSE;
        }else{
            return ($file_contents) ? $file_contents : FALSE;
        }
    }

    public function insert_coordenadas_DAO($db, $value) {

        $sql = "INSERT INTO city_coordenadas ( dni, city, lat, lng ) VALUES ('$value[dni]', '$value[city]', '$value[lat]', '$value[lng]')";
            
        return $db->ejecutar($sql);
        
    }

    public function count_user_DAO($db, $value) {
        //echo json_encode($value);
        //exit;
        $sql = "SELECT count(*) as count FROM ".$value['table']." WHERE id_user = '".$value['id']."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function get_avatar_DAO($db, $value) {
        //echo json_encode($value);
        //exit;
        $sql = "SELECT avatar FROM user_register WHERE id_user = '".$value."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function get_data_user_DAO($db, $value) {
        //echo json_encode($value);
        //exit;
        $sql = "SELECT * FROM ".$value['table']." WHERE id_user = '".$value['id']."'";
            
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function get_likes_DAO($db) {

        $sql = "SELECT * FROM likes";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    

}
