<?php
    class db {
        private $servidor;
        private $usu;
        private $pass;
        private $bd;
        private $link;
        private $stmt;
        private $array;
        static $_instance;
    
        private function __construct() {
            $this->setConexion();
            $this->conectar();
        }
    
        private function setConexion() {
            $conf = conf::getInstance();
           
            $this->servidor = $conf->hostdb;
            $this->bd = $conf->db;
            $this->usu = $conf->userdb;
            $this->pass = $conf->passdb;
        }
    
        private function __clone() {
            
        }
    
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
    
        private function conectar() {
            $this->link = new mysqli($this->servidor, $this->usu, $this->pass);
            $this->link->select_db($this->bd);
        }
    
        public function ejecutar($sql) {
            //echo '<script language="javascript">console.log("Entra en ejecutar")</script>';
            $this->stmt = $this->link->query($sql);
            return $this->stmt;
        }

        public function listar($stmt) {
            $this->array = array();
            while ($row = $stmt->fetch_array(MYSQLI_ASSOC)) {
                array_push($this->array, $row);
            }
            return $this->array;
        }
    
    
    }