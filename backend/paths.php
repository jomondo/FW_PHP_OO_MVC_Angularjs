<?php
//SITE_ROOT
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'].'/freelancer/backend/');

//SITE_PATH
define('SITE_PATH', 'https://' . $_SERVER['HTTP_HOST'] . '/freelancer/backend/');

//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/js/');

define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . 'model/');

//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');

//modules
define('MODULE_PATH', SITE_ROOT . 'module/');


//resources
define('RESOURCES', SITE_ROOT . 'resources/');

//media
define('MEDIA_PATH', SITE_PATH . 'media/');
define('MEDIA_GRAVATAR', SITE_PATH . 'media/gravatar/');

//utils
define('UTILS', SITE_ROOT .'module/util/');
define('UTILS_PATH', SITE_ROOT .'utils/');
define('UTILS_HOME', SITE_ROOT .'module/home/utils/');

//lib
define('LIB_PATH', SITE_PATH .'view/lib/');

//module home
define('MODEL_PATH_HOME', SITE_ROOT . 'module/home/model/');
define('DAO_HOME', SITE_ROOT . 'module/home/model/DAO/');
define('BLL_HOME', SITE_ROOT . 'module/home/model/BLL/');
define('MODEL_HOME', SITE_ROOT . 'module/home/model/Model/');
define('HOME_JS_PATH', SITE_PATH . 'module/home/view/js/');
define('HOME_CSS_PATH', SITE_PATH . 'module/home/view/css/');

//module profile
define('UTILS_PROFILE', SITE_ROOT . 'module/profile/utils/');
define('PROFILE_JS_LIB_PATH', SITE_PATH . 'module/profile/view/lib/');
define('PROFILE_JS_PATH', SITE_PATH . 'module/profile/view/js/');
define('MODEL_PATH_PROFILE', SITE_ROOT . 'module/profile/model/');
define('DAO_PROFILE', SITE_ROOT . 'module/profile/model/DAO/');
define('BLL_PROFILE', SITE_ROOT . 'module/profile/model/BLL/');
define('MODEL_PROFILE', SITE_ROOT . 'module/profile/model/Model/');

//module login
define('MODEL_LOGIN', SITE_ROOT . 'module/login/model/Model/');
define('LOGIN_JS_PATH', SITE_PATH . 'module/login/view/js/');
define('LOGIN_CSS_PATH', SITE_PATH . 'module/login/view/css/');

//module admin
define('MODEL_ADMIN', SITE_ROOT . 'module/admin/model/Model/');

//module forum
define('MODEL_FORUM', SITE_ROOT . 'module/forum/model/Model/');

//module contact
define('CONTACT_JS_PATH', SITE_PATH . 'module/contact/view/js/');

//start web
define('START_WEB', SITE_ROOT . 'home/');

//path
define('PATHS', SITE_ROOT . 'paths.php');

//amigables
define('URL_AMIGABLES', TRUE);