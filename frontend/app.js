// module
var freelancer = angular.module('freelancer', ['ngRoute','ngAnimate','ui.bootstrap','infinite-scroll','ngMaterial', 'ngCookies']);

// conf rutes
freelancer.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl : 'frontend/module/home/view/home.html',
        controller  : 'home_controller',
        resolve: {
            data: function (services) {
                return services.get('home', 'nameCategory');
            }
        }

        }).when('/freelancer', {
            templateUrl : 'frontend/module/home/view/listUser.html',
            controller  : 'list_controller',
            resolve: {
                data: function (services) {
                    return services.get('home', 'list_freelancer');

                },
                //obtain values filters
                categ: function (filter_services) {
                    return filter_services.obtain_category_combo();

                },
                especialist: function (filter_services) {
                    return filter_services.obtain_especialist_combo();

                },
                province: function ( filter_services) {
                    return filter_services.obtain_province_combo();

                }
            }

        }).when('/list_categories/:cat', {
            templateUrl : 'frontend/module/home/view/list_categories.html',
            controller  : 'list_categories',
            resolve: {
                data: function (services,$route) {
                    return services.get('home', 'list_freelancer');

                },
                //obtain values filters
                categ: function (filter_services) {
                    return filter_services.obtain_category_combo();

                },
                especialist: function (filter_services) {
                    return filter_services.obtain_especialist_combo();

                },
                province: function (filter_services) {
                    return filter_services.obtain_province_combo();

                }
            }

        }).when('/list_typeahead/:esp', {
            templateUrl : 'frontend/module/home/view/list_typeahead.html',
            controller  : 'list_typeahead',
            resolve: {
                data: function (services,$route) {
                    return services.get('home', 'list_freelancer');

                },
                //obtain values filters
                categ: function (filter_services) {
                    return filter_services.obtain_category_combo();

                },
                especialist: function (filter_services) {
                    return filter_services.obtain_especialist_combo();

                },
                province: function (filter_services) {
                    return filter_services.obtain_province_combo();

                }
            }

        }).when('/freelancer/:dni', {
            templateUrl : 'frontend/module/home/view/detailsUser.html',
            controller  : 'details_controller',
            resolve: {
                data: function (services,$route) {
                    console.log($route.current.params.dni);
                    return services.get('home', 'details_list', $route.current.params.dni);

                },
                likes: function (services,$route, details_services) {
                    return details_services.get_likes_details($route.current.params.dni);

                },
                // return services.get('home', 'get_votaciones', $route.current.params.dni);
                num_votaciones: function (services,$route, details_services) {
                    var data = {"dni":$route.current.params.dni}
                    var data_users_JSON = JSON.stringify(data);
                    return services.post('home', 'get_votaciones', data_users_JSON);

                },

            }

        }).when('/contact', {
            templateUrl : 'frontend/module/contact/view/contact.html',
            controller  : 'contactController'

        }).when('/profile', {
            templateUrl : 'frontend/module/profile/view/profile.view.html',
            controller  : 'profile_controller',
            resolve: {
                data: function (services,$route,cookiesService) {
                    var user = cookiesService.getUser();
                    var data = {"token":user.token}
                    var data_users_JSON = JSON.stringify(data);
                    return services.post('login', 'select_user_token', data_users_JSON);

                }
            }

        }).when("/user/activar/:token", {
            templateUrl: "frontend/module/home/view/home.html",
            controller: "verify_controller"

        }).when("/user/changePassword/:token", {
            templateUrl: "frontend/module/login/view/change_passwd.html",
            controller: "change_passwd_controller"

        }).when("/admin", {
            templateUrl: "frontend/module/admin/view/admin.view.html",
            controller: "admin_controller",
            resolve: {
                freelancer: function (services,admin_services ) {
                    return admin_services.get_freelancer();

                },
                clients: function (services,admin_services ) {
                    return admin_services.get_clients();

                },
                visitors: function (services,admin_services ) {
                    return admin_services.get_total_visitors();

                },
                visitors_last7days: function (services,admin_services ) {
                    return admin_services.get_visitors_last_7days();

                }
                
            }

        }).when("/admin/control_user", {
            templateUrl: "frontend/module/admin/view/admin_control_users.view.html",
            controller: "admin_control_user_controller",
            resolve: {
                users: function (services,admin_services ) {
                    return admin_services.get_users_register();

                }
                
            }


        }).when("/profile/my_favourites", {
            templateUrl: "frontend/module/profile/view/profile_favourites.view.html",
            controller: "favourites_controller",
            resolve: {
                data: function (services) {
                    return services.get('profile', 'get_like');

                },
                freelancer: function (services) {
                    return services.get('home', 'list_freelancer');

                }
            }

        }).when("/profile/my_rank", {
            templateUrl: "frontend/module/profile/view/profile_rank.view.html",
            controller: "rank_controller",
            resolve: {
                comments: function (services) {
                    return services.get('forum', 'get_comments');

                },
                created_items: function (services) {
                    return services.get('forum', 'comments_grupBy');

                },
                likes: function (services) {
                    return services.get('profile', 'get_like');

                },
                votados: function (services) {
                    return services.post('home', 'get_votaciones_by_user');

                },
                
            }
            

        }).when("/forum", {
            templateUrl: "frontend/module/forum/view/forum.view.html",
            controller: "forum_controller",
            resolve: {
                comments: function (services) {
                    return services.get('forum', 'get_comments');

                },
                commentsGrupBy: function (services) {
                    return services.get('forum', 'comments_grupBy');

                }
                
            }


        }).when("/top_users", {
            templateUrl: "frontend/module/home/view/top_users.view.html",
            controller: "top_users_controller",
            resolve: {
                users: function (services) {
                    return services.get('home', 'get_users_register');

                }
                
            }


        }).otherwise({
            redirectTo: '/'

        });

}]);

freelancer.config(function ($mdThemingProvider) {
    //modal
    $mdThemingProvider.theme('red')
      .primaryPalette('red');

    $mdThemingProvider.theme('blue')
      .primaryPalette('blue');
        
});
