freelancer.factory('rank_services', function (services) {
	var service = {};
    service.get_karma = get_karma;
    return service;

    function get_karma($scope, id_user){
        var num_commens = $scope.num_comments.length;
        var num_items = $scope.num_items.length;
        var num_likes = $scope.num_likes.length;
        // alert(num_commens);
        // alert(num_items);
        // alert(num_likes);
        
        if ( num_commens <= 1 ) {
            $scope.nothing_comment = true;
            $scope.total_points = 0;

        }

        if ( num_items <1 ) {
            $scope.nothing_item = true;
            $scope.total_points = 0;

        }

        if ( num_likes <1 ) {
            $scope.nothing_likes = true;
            $scope.total_points = 0;

        }
        
        //// comments /////////
        if ( num_commens >1 && num_commens <=2 || num_commens >= 3 && num_commens <= 10 || num_commens >= 11 && num_commens <= 50) {
            $scope.rank_brounce = true;

        }if( num_commens >= 3 && num_commens <= 10 || num_commens >= 11 && num_commens <= 50 ){
            $scope.rank_plata = true;

        }if( num_commens >= 11 && num_commens <= 50 ){
            $scope.rank_gold = true;

        }

        ///// items ////////
        if (num_items>=1 && num_items <=2 || num_items >= 3 && num_items <= 10 || num_items >= 11 && num_items <= 50 ) {
            $scope.rank_brounce_items = true;

        }if ( num_items >=3 && num_items <=10 || num_items >= 11 && num_items <= 50 ) {
            $scope.rank_plata_items = true;

        }if( num_items >= 11 && num_items <= 50 ){
            $scope.rank_gold_items = true;

        }

        //// likes /////////
        if ( num_likes >=1 && num_likes <=2 || num_likes >= 3 && num_likes <= 10 || num_likes >= 11 && num_likes <= 50) {
            $scope.rank_brounce_likes = true;

        }if( num_likes >= 3 && num_likes <= 10 || num_likes >= 11 && num_likes <= 50 ){
            $scope.rank_plata_likes = true;

        }if( num_likes >= 11 && num_likes <= 50 ){
            $scope.rank_gold_likes = true;

        }

        $scope.total_points = ( num_commens + num_items + num_likes );
        //update points to table user_register
        var data = { "id_user":id_user,"points":$scope.total_points};
        var data_points_JSON = JSON.stringify(data);
        console.log(data_points_JSON);
        
        services.post("home","update_points_user",data_points_JSON).then(function(response){
            console.log(response);

        });
        
    }

});
