freelancer.factory('load_selects_services', function ($q, services, CommonService) {
	var service = {};
    service.load_community = load_community;
    service.load_province = load_province;
    service.load_city = load_city;
    return service;

    function load_community() {
        var deferred = $q.defer();
        services.get("profile", "load_community", true).then(function (data) {
            //console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_community" });
            } else {
                deferred.resolve({ success: true, datas: data });
                
            }
        });
        return deferred.promise;
    };
    
    function load_province(community) {
        var deferred = $q.defer();

        services.post("profile", "load_provinces", JSON.stringify(community)).then(function (data) {
            console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_province" });
            } else {
                deferred.resolve({ success: true, datas: data });
                
            }
        });
        return deferred.promise;
    };
    
    function load_city(province) {
        var deferred = $q.defer();
        services.post("profile", "load_cities", JSON.stringify(province)).then(function (data) {
            //console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_cities" });
            } else {
                deferred.resolve({ success: true, datas: data });
                
            }
        });
        return deferred.promise;
    };
});