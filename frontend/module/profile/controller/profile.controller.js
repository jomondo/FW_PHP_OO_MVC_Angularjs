freelancer.controller('profile_controller', function ($rootScope, $filter, $scope, data, load_selects_services, $timeout, services, CommonService, $location, cookiesService, UserService, $mdDateLocale) {
    console.log(data.user[0]);
    
    CommonService.breadcrumb(true, "Profile", "profile", "My Profile");
    CommonService.underlined_menu("profile");

    $scope.lang1 = "Español";
    $scope.lang2 = "Ingles";
    $scope.lang3 = "Frances";
    $scope.lang4 = "Italiano";
    
    //this.myDate = new Date();
    this.isOpen = false;
    $scope.format_date_birthday = function (data) {
        $scope.birthday = $filter('date')(new Date(data),'MM/dd/yyyy');
        console.log($scope.birthday);

    };
    
    $scope.format_date_expDni = function (data) {
        $scope.exp_dni = $filter('date')(new Date(data),'MM/dd/yyyy');
        console.log($scope.exp_dni);
        
    };
     
    if (data.user[0].language !== null) {
        var value = data.user[0].language
        var f = value.split(":");

        for(var i=0;i<f.length;i++){
            //alert(f[i]);
            if (f[i]=="Ingles") {
                $scope.checked2 = true;

            }
            if (f[i]=="Español") {
                $scope.checked1 = true;

            }
            if (f[i]=="Frances") {
                $scope.checked3 = true;

            }
            if (f[i]=="Italiano") {
                $scope.checked4 = true;

            }

        }
                        
    }
    console.log(String(data.user[0].exp_dni));
    $scope.token = data.user[0].token;
    $scope.val = {};
    $scope.name = data.user[0].id_user;
    $scope.last_name = data.user[0].last_name;
    $scope.birthday = data.user[0].birthday;
    $scope.exp_dni = data.user[0].exp_dni;
    $scope.language = data.user[0].language;
    $scope.phone = data.user[0].phone;
    $scope.community = data.user[0].community;
    $scope.avatar = data.user[0].avatar;
    $scope.email = data.user[0].email;
    

    load_selects_services.load_community().then(function (response) {
        console.log(response);
    
        $scope.community = response.datas.data;
        if (data.user[0].community!==null) {
            $scope.id_community = data.user[0].community;
            var id_com = data.user[0].community;
            /* la api requiere 1 segundo por cada peticion */
            $timeout(function () {
                load_selects_services.load_province(id_com).then(function (response) {
                    $scope.province = response.datas.data;
                    $scope.id_province = data.user[0].province;

                });

            }, 2000);

            $timeout(function () {
                load_selects_services.load_city($scope.id_province).then(function (response) {
                    console.log(response);
                    $scope.city = response.datas.data;
                    $scope.id_city = data.user[0].city;

                });

            }, 4000);
            
        }

    });

    $scope.get_province = function (data) {
        $scope.id_community = data;
        load_selects_services.load_province(data).then(function (response) {
            console.log(response);
            $scope.province = response.datas.data;

        });
    };

    $scope.get_city = function (data) {
        $scope.id_province = data;
        load_selects_services.load_city(data).then(function (response) {
            console.log(response);
            $scope.city = response.datas.data;

        });
    };

    $scope.obtain_city = function (data){
        $scope.id_city = data;
    };

    $scope.obtain_check = function (data){
        $scope.check_language = data;
    };
    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=profile&function=upload',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
                console.log(file);
                console.log(response);
                if (file.status=="success") {
                    $('.msg').text('');
                    $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente');

                }else{
                    $('.msg').text('');
                    $('.msg').text('Ha ocurrido algun error con el archivo' + file.name);

                }
                
            },
            'removedfile': function (file, serverFileName) {
                console.log(file);
                
            }
    }};


    $scope.submit_update = function () {
        let language = [];
        var inputElements = document.getElementsByClassName('checkbox');
        var j = 0;
        for (var i = 0; i < inputElements.length; i++) {
            if (inputElements[i].checked) {
                language[j] = inputElements[i].value;
                j++;
            }
        }

        var data = { "id":$scope.name, "name":$scope.inputName, "last_name":$scope.inputLast_name, "birthday":$scope.birthday,"exp_dni":$scope.exp_dni, "community":$scope.id_community,"province":$scope.id_province,"city":$scope.id_city, "language":language, "email":$scope.inputEmail, "phone":$scope.inputPhone };
        var data_profile_JSON = JSON.stringify(data);
        console.log(data_profile_JSON);

        services.post("profile", "update_profile_client", data_profile_JSON).then(function (response){
            console.log(response);

            if (response.success) {
                CommonService.banner(response.msg,"");
                cookiesService.setUser($scope.token);
                UserService.login();

            }else{
                if (response.error) {
                    CommonService.banner(response.error,"Err");

                }else{
                    CommonService.banner(response.msg,"Err");

                }

            }

        });

    };

});

/////// my favourites freelancer ////////
freelancer.controller('favourites_controller', function ($route, data, freelancer, $scope, services, CommonService, filter_services, token_services) {
    console.log(data);
    console.log(freelancer);
    $scope.freelancer = freelancer;
    $scope.array = [];
    $scope.filtered = [];
    $scope.currentPage = 1;


    token_services.get_info().then(function(response){
        console.log(response);
        var id_user = response.response.user[0].id_user;
        if (response.success) {
            filter_services.get_likes(data, id_user, $scope);
            if ($scope.array.length==0) {
                alert("no tens favoritos");

            }else{
                console.log($scope.array);
                
                $scope.val = $scope.array;
                $scope.filtered = $scope.array.slice(0, 2);

                $scope.page_change = function() {
                    var startPos = ($scope.currentPage - 1) * 2;
                    $scope.filtered = $scope.array.slice(startPos, startPos + 2);
                    console.log($scope.filtered);
                };
                
            }

        }else{
            CommonService.banner("Tienes que estar registrado","Err");

        }

    });


    
});
///////// my rank //////////
freelancer.controller('rank_controller', function (votados, rank_services, likes, comments, created_items, $scope, services, CommonService, token_services) {
    console.log(comments);
    console.log(votados);
    $scope.comments = comments;
    $scope.votados = votados;
    $scope.likes = likes;
    $scope.created_items = created_items;
    $scope.num_comments = [];
    $scope.name_votados = [];
    $scope.num_items = [];
    $scope.num_likes = [];

    $scope.rank_brounce = false;
    $scope.rank_plata = false;
    $scope.rank_gold = false;
    $scope.nothing = false;
    $scope.rank_brounce_items = false;
    $scope.rank_plata_items = false;
    $scope.rank_gold_items = false;

    $scope.rank_brounce_likes = false;
    $scope.rank_plata_likes = false;
    $scope.rank_gold_likes = false;

    

    token_services.get_info().then(function(response){
        console.log(response);
        var id_user = response.response.user[0].id_user;
        $scope.avatar = response.response.user[0].avatar;

        if (response.success) {
            angular.forEach($scope.votados, function(val, key) {
                if (val.id_valorador == id_user) {
                    $scope.name_votados.push(val);

                }

            });
            console.log($scope.name_votados);
            angular.forEach($scope.comments, function(val, key) {
                if (val.id_user == id_user) {
                    $scope.num_comments.push(val);

                }

            });

            angular.forEach($scope.created_items, function(val, key) {
                if (val.id_user == id_user) {
                    $scope.num_items.push(val);

                }

            });

            angular.forEach($scope.likes, function(val, key) {
                if (val.id_user == id_user) {
                    $scope.num_likes.push(val);

                }

            });
            // alert($scope.num_items.length);
            // alert($scope.num_comments.length);
            rank_services.get_karma($scope, id_user);
            
            
        }else{
            CommonService.banner("Tienes que estar registrado","Err");

        }

    });


    
});