freelancer.controller('admin_controller', function (freelancer,clients, visitors,visitors_last7days, $scope, $rootScope, services, admin_services, CommonService) {
	console.log(visitors_last7days);
	CommonService.underlined_menu("admin");
	CommonService.breadcrumb(true, "admin", "admin", "Dashboard Admin");

	$scope.total_visitors = visitors.response[0].count_visitors;
	$scope.total_freelancer = freelancer.response[0].count_freelancer;
	$scope.total_clients = clients.response[0].count_clients;
		
	var day0 = visitors_last7days.response[0].visitors_last7days;
	var day1 = visitors_last7days.response[1].visitors_last7days;
	var day2 = visitors_last7days.response[2].visitors_last7days;
	var day3 = visitors_last7days.response[3].visitors_last7days;
	var day4 = visitors_last7days.response[4].visitors_last7days;
	var day5 = visitors_last7days.response[5].visitors_last7days;
	var day6 = visitors_last7days.response[6].visitors_last7days;
	
	admin_services.visitors_week_graph(day0,day1,day2,day3,day4,day5,day6);
	

});

freelancer.controller('admin_control_user_controller', function (users, $scope, $rootScope, services, CommonService, admin_services) {
	CommonService.underlined_menu("admin");
	$scope.users = users.response;

	$scope.submit_lock = function(data){
		console.log(data);
		var data = { "id_user":data };
        var data_JSON = JSON.stringify(data);

		services.post("admin","lock_user",data_JSON).then(function(response){
			console.log(response);
			if (response.success) {
				CommonService.banner("Has bloqueado a un usuario","");
				admin_services.get_users_register().then(function(response){
					console.log(response);
					$scope.users = response.response;

				});

			}else{
				CommonService.banner("No has podido bloquear al usuario","Err");

			}
		});

	};

	$scope.submit_unlock = function(data){
		console.log(data);
		var data = { "id_user":data };
        var data_JSON = JSON.stringify(data);

		services.post("admin","unlock_user",data_JSON).then(function(response){
			console.log(response);
			if (response.success) {
				CommonService.banner("Has desbloqueado a un usuario","");
				admin_services.get_users_register().then(function(response){
					$scope.users = response.response;
					
				});

			}else{
				CommonService.banner("No has podido desbloquear al usuario","Err");

			}
		});

	};

	$scope.submit_delete = function(data){
		console.log(data);
		var data = { "id_user":data };
        var data_JSON = JSON.stringify(data);

		services.post("admin","delete_user",data_JSON).then(function(response){
			console.log(response);
			if (response.success) {
				CommonService.banner("Has eliminado a un usuario","");
				admin_services.get_users_register().then(function(response){
					$scope.users = response.response;
					
				});

			}else{
				CommonService.banner("No has podido eliminar al usuario","Err");

			}
		});

	};

});