freelancer.factory('admin_services', function ($q, services) {
	var service = {};
    service.get_freelancer = get_freelancer;
    service.get_clients = get_clients;
    service.get_total_visitors = get_total_visitors;
    service.get_users_register = get_users_register;
    service.get_visitors_last_7days = get_visitors_last_7days;
    service.visitors_week_graph = visitors_week_graph;
    return service;

    function get_freelancer() {
        var deferred = $q.defer();

        services.get("admin","get_total_freelancer").then(function(response){
			console.log(response);
			
			deferred.resolve({ success: true, response });

		}).catch(function(error) {
            console.log( 'Error al obtener datos freelancer - error 503 - ' + error );

        });

        return deferred.promise;
    };

    function get_clients() {
        var deferred = $q.defer();

        services.get("admin","get_total_clients").then(function(response){
			console.log(response);
			
			deferred.resolve({ success: true, response });

		}).catch(function(error) {
            console.log( 'Error al obtener datos clients - error 503 - ' + error );

        });

        return deferred.promise;
    };

    function get_total_visitors() {
        var deferred = $q.defer();

        services.get("admin","get_total_visitors").then(function(response){
			console.log(response);
			
			deferred.resolve({ success: true, response });

		}).catch(function(error) {
            console.log( 'Error al obtener datos visitors - error 503 - ' + error );

        });

        return deferred.promise;
    };

    function get_users_register() {
        var deferred = $q.defer();

        services.get("admin","get_users_register").then(function(response){
			console.log(response);
			
			deferred.resolve({ success: true, response });

		}).catch(function(error) {
            console.log( 'Error al obtener datos users_register - error 503 - ' + error );

        });

        return deferred.promise;
    };

    function get_visitors_last_7days() {
        var deferred = $q.defer();

        services.get("admin","get_visitors_last_7days").then(function(response){
            console.log(response);
            
            deferred.resolve({ success: true, response });

        }).catch(function(error) {
            console.log( 'Error al obtener datos users_register - error 503 - ' + error );

        });

        return deferred.promise;
    };

    function visitors_week_graph(day0,day1,day2,day3,day4,day5,day6) {
        var data, options;

        data = {
            labels: ['Day1', 'Day2', 'Day3', 'Day4', 'Day5', 'Day6', 'Day7'],
            series: [ [ day0, day1, day2, day3, day4, day5, day6 ] ]
        };

        options = {
            height: 400,
            width: 1000,
            showArea: true,
            showLine: true,
            showPoint: true,
            fullWidth: true,
            axisX: {
                showGrid: true
            },
            lineSmooth: true,
        };

        new Chartist.Line('#headline-chart', data, options);
        
    };



});