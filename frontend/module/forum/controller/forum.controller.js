freelancer.controller('forum_controller', function ( forum_services, forum_karma_services, update_karma_services, commentsGrupBy, comments, $scope, $rootScope, CommonService, services, token_services ) {
	//console.log(comments);
    CommonService.breadcrumb(true, "forum", "forum", "Your Forum");
    CommonService.underlined_menu("forum");
    
    $scope.filtered_items = [];
    var id_user = [];
    $scope.currentPage = 1;
    $scope.page_items = true;
    $scope.page_comments = false;
	$scope.comments = comments;
    $scope.commentsGrupBy = commentsGrupBy;
    $scope.delete_comment = false;

    ////// pagination items ///////
    $scope.filtered_items = $scope.commentsGrupBy.slice(0, 3);
    $scope.page_changed = function() {
        var startPos = ($scope.currentPage - 1) * 3;
        $scope.filtered_items = $scope.commentsGrupBy.slice(startPos, startPos + 3);
      
    };
    
    ///// show form addItem /////
    $scope.submit_item = function(){
    	$scope.addItem = true;

    };

    $scope.submit_like = function(id_item, name_item){
        console.log(id_item);
        /*

        1. check user
        2. check if exist it on table db
        3. if exist = error, already exit
        4. if not, insert into on table

        */
        token_services.get_info().then(function(response){
            if (response.success) {
                var id_user = response.response.user[0].id_user;
                var data = { "id_item":id_item,"id_user":id_user };
                var data_forum_JSON = JSON.stringify(data);
                console.log(data_forum_JSON);

                services.post("forum","check_like",data_forum_JSON).then(function(response){
                    console.log(response);
                    if (response.success) {
                        CommonService.banner("Has dado me gusta al comentario","");
                        services.get('forum', 'get_comments').then(function(reset){
                            $scope.comments = reset;
                            $scope.answers = [];
                            /*angular.forEach($scope.comments, function(val, key) {
                                if (val.item == name_item) {
                                    $scope.answers.push(val);

                                }

                            });*/

                            $scope.comments.map(function (val) {
                                if (val.item == name_item) {
                                    $scope.answers.push(val);

                                }
                                
                            });
                            
                            // console.log($scope.answers);
                            $scope.filtered_comments = $scope.answers.slice(0, 3);

                        });  

                    }else{
                        CommonService.banner("Ya has dado me gusta al comentario","Err");

                    }

                });

            }else{
                CommonService.banner("Tienes que registrarte para poder dar me gusta al comentario","Err");

            }

        });

    };
     /* show delete button admin */
    forum_services.check_admin($scope);

    ///// delete comment ADMIN /////
    $scope.submit_delete_comment = function(id_item, item){
        console.log(id_item);
        var data = { "id_item":id_item };
        var data_forum_JSON = JSON.stringify(data);
        console.log(data_forum_JSON);

        services.post("forum", "delete_comment", data_forum_JSON).then(function (response) {
            console.log(response);
            if (response.success) {
                CommonService.banner("Has eliminado un comentario","");

                services.get('forum', 'get_comments').then(function(reset){
                    $scope.comments = reset;
                    $scope.answers = [];
                   /* angular.forEach($scope.comments, function(val, key) {
                        if (val.item == item) {
                            $scope.answers.push(val);

                        }

                    });*/

                    $scope.comments.map(function (val) {
                        if (val.item == item) {
                            $scope.answers.push(val);

                        }
                        
                    });
                    // console.log($scope.answers);
                    $scope.filtered_comments = $scope.answers.slice(0, 3);

                });  

            }else{
                CommonService.banner("Ha habido algun error al borrar el comentario","Err");

            }

        });



    };

    $scope.submit_comment = function(){
    	token_services.get_info().then(function(response){
    		console.log(response);
    		if (response.success) {
                var id_user = response.response.user[0].id_user;
    			var data = { "id_user":response.response.user[0].id_user,"name":response.response.user[0].name, "avatar":response.response.user[0].avatar, "item":$scope.input_item, "comment":$scope.input_comment };
		        var data_forum_JSON = JSON.stringify(data);
		        console.log(data_forum_JSON);

		        services.post("forum", "submit_comment", data_forum_JSON).then(function (response) {
		            console.log(response);
		            if (response.success) {
		                CommonService.banner("Su comentario ha sido enviado","");
		                services.get('forum', 'comments_grupBy').then(function(response){
		                	//console.log(response);
		                	$scope.commentsGrupBy = response;
		                });
                        
                        //////// get_karma ///////////
                        update_karma_services.update_karma($scope, id_user).then(function(response){
                            if (response.success) {
                                services.get('forum', 'get_comments').then(function(reset){
                                    $scope.comments = reset;
                                    $scope.filtered_items = $scope.commentsGrupBy.slice(0, 3);

                                });        
                            }

                        });
                            
		                $scope.addItem = false;

		            }else{
		                CommonService.banner("Ha habido algun error al enviar su comentario, vuelvalo a intentar mas tarde","Err");

		            }

		        });

    		}else{
    			CommonService.banner("Tienes que registrarte para poder comentar en el foro","Err");

    		}

    	});
    
    };

    $scope.show_comment = function(item){
    	//console.log(id_item);
        
        $scope.filtered_comments = [];
        $scope.answers = [];
        $scope.num_comments = [];
        $scope.page_items = false;
        $scope.page_comments = true;
        $scope.forum = false;
        $scope.forum_comments = true;
        
        /*angular.forEach($scope.comments, function(val, key) {
            if (val.item == item) {
                $scope.answers.push(val);

            }

        });*/

        $scope.comments.map(function (val) {
            if (val.item == item) {
                $scope.answers.push(val);

            }
                        
        });
        
        console.log($scope.answers);

        // pagination
        $scope.filtered_comments = $scope.answers.slice(0, 3);
        $scope.page_changed_comments = function() {
            var startPos = ($scope.currentPage - 1) * 3;
            $scope.filtered_comments = $scope.answers.slice(startPos, startPos + 3);
          
        };

    };

    $scope.show_form_answer = function(item){
        $scope.item = "";
        $scope.addItems = true;
        $scope.item = item;

    };

    $scope.submit_answer = function(answer){
        //console.log(answer.$modelValue);
        token_services.get_info().then(function(response){
            console.log(response);
            if (response.success) {
                var id_user = response.response.user[0].id_user;
                var data = { "id_user":response.response.user[0].id_user, "name":response.response.user[0].name, "avatar":response.response.user[0].avatar, "item":$scope.item, "comment":answer.$modelValue };
                var data_forum_JSON = JSON.stringify(data);
                console.log(data_forum_JSON);

                services.post("forum", "submit_answer", data_forum_JSON).then(function (response) {
                    console.log(response);
                    if (response.success) {
                        CommonService.banner("Su comentario ha sido enviado","");
                        services.get('forum', 'get_comments').then(function(response){
                            $scope.answers = [];
                            $scope.comments = response;
                            ///// get karma ////////////
                            update_karma_services.update_karma($scope, id_user).then(function(response){
                                if (response.success) {
                                    services.get('forum', 'get_comments').then(function(reset){
                                        $scope.comments = reset;

                                        /*angular.forEach($scope.comments, function(val, key) {
                                            if (val.item == $scope.item) {
                                                $scope.answers.push(val);

                                            }

                                        });*/

                                        $scope.comments.map(function (val) {
                                            if (val.item == $scope.item) {
                                                $scope.answers.push(val);

                                            }
                                                        
                                        });


                                        //// paginate /////
                                        $scope.filtered_comments = $scope.answers.slice(0, 3);

                                    });        
                                }

                            });

                        });

                        $scope.addItems = false;

                    }else{
                        CommonService.banner("Ha habido algun error al enviar su comentario, vuelvalo a intentar mas tarde","Err");

                    }

                });
            }else{
                CommonService.banner("Tienes que registrarte para poder comentar en el foro","Err");
                
            }

        });

    };

    $scope.return_to_items = function(){
        $scope.addItems = false;
        $scope.forum = true;
        $scope.forum_comments = false;
        $scope.page_items = true;
        $scope.page_comments = false;

    };

});