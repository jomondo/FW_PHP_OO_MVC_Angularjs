freelancer.factory('forum_karma_services', function () {
	var service = {};
    service.get_karma = get_karma;
    return service;

    function get_karma($scope){
        var num_commens = $scope.num_comments.length;
        
        if ( num_commens <= 1) {
            $scope.karma = "miembro";

        }else{
            if ( num_commens <=2 ) {
                $scope.karma = "aprendiz";

            }if( num_commens >= 3 && num_commens <= 10 ){
                $scope.karma = "moderate freelance";

            }if( num_commens >= 11 && num_commens <= 50 ){
                $scope.karma = "expert freelance";

            }

        }

    }

});

freelancer.factory('update_karma_services', function ($q, forum_karma_services, services) {
    var service = {};
    service.update_karma = update_karma;
    return service;

    function update_karma($scope, id_user){
        var deferred = $q.defer();
        $scope.num_comments = [];

        /*angular.forEach($scope.comments, function(val, key) {
            if (val.id_user == id_user) {
                $scope.num_comments.push(val);

            }

        });*/

        $scope.comments.map(function (elemento) {
            if (elemento.id_user == id_user) {
                $scope.num_comments.push(elemento);

            }
        });

        forum_karma_services.get_karma($scope);
        
        var data = { "id_user":id_user,"karma":$scope.karma };
        var data_karma_JSON = JSON.stringify(data);
        console.log(data_karma_JSON);

        services.post("forum", "update_karma", data_karma_JSON).then(function (response) {
            console.log(response);
            deferred.resolve({ success: true });

        });

        return deferred.promise;

    }

});

freelancer.factory('forum_services', function ( token_services, forum_karma_services ) {
    var service = {};
    service.check_admin = check_admin;
    return service;

    function check_admin($scope){

        token_services.get_info().then(function(response){
            //console.log(response.response.user[0].type);
            if (response.success) {
                var type_user = response.response.user[0].type;
                if (type_user == 1) {
                    $scope.delete_comment = true;

                }else{
                    $scope.delete_comment = false;

                }

            }
            
        });

    }

});