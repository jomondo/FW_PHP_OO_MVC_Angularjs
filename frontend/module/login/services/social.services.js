freelancer.factory('init_firebase', function ($q) {
    var service = {};
    service.init = init;
    //service.google = google;
    return service;

    function init(){
        //var promise;
        var deferred = $q.defer();
        var config = {
            apiKey: "AIzaSyB1OO7VM2OQCD_nI6fF853lMJijrs_h7uE",
            authDomain: "freelancerangularjs.firebaseapp.com",
            databaseURL: "https://freelancerangularjs.firebaseio.com",
            projectId: "freelancerangularjs",
            storageBucket: "freelancerangularjs.appspot.com",
            messagingSenderId: "733999342406"
        };

        firebase.initializeApp(config);
            
        var authService = firebase.auth();
        //console.log(authService);
        promise = deferred.resolve(authService);
        return deferred.promise;
        
    };
    
});

freelancer.factory('google_services', function ($q, init_firebase, data_social_services ) {
    var service = {};
    service.google = google;
    return service;

    function google(){
        //console.log(init_firebase);
        init_firebase.init().then(function(result){
          console.log(result);
          var provider_google = new firebase.auth.GoogleAuthProvider();
          provider_google.addScope('email');
          data_social_services.submit_data(result, provider_google); 
    
        }).catch(function(error) {
            console.log('Error al iniciar session con firebase');

        });
        
    }

    
});

freelancer.factory('facebook_services', function ($q, init_firebase, data_social_services) {
    var service = {};
    service.facebook = facebook;
    return service;

    function facebook(){
        init_firebase.init().then(function(result){
          var provider_facebook = new firebase.auth.FacebookAuthProvider();
          data_social_services.submit_data(result, provider_facebook);  

        }).catch(function(error) {
          console.log('Error al iniciar session con firebase');

        });
    }
    
});

freelancer.factory('twitter_services', function ($q, init_firebase, data_social_services) {
    var service = {};
    service.twitter = twitter;
    return service;

    function twitter(){
      init_firebase.init().then(function(result){
        var provider_twitter = new firebase.auth.TwitterAuthProvider();
        data_social_services.submit_data(result, provider_twitter);  

      }).catch(function(error) {
        console.log('Error al iniciar session con firebase');

      });

    }
    
});

freelancer.factory('data_social_services', function ($q, services, $mdDialog, CommonService, cookiesService, UserService) {
    var service = {};
    service.submit_data = submit_data;
    return service;

    function submit_data(result, provider){
      result.signInWithPopup(provider).then(function(result) {
        console.log('Hemos autenticado al usuario ', result.user);
        var data = {"id":result.user.providerData[0].uid , "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        var data_log = JSON.stringify(data);
        services.post('login', 'social_signup', data_log).then(function (response) {
          console.log(response);
          if (response.success) {
            $mdDialog.cancel();
            CommonService.banner("Te has registrado correctamente. Ya puede iniciar sesion", "");

          }else if(response.error){
            $mdDialog.cancel();
            CommonService.banner("Error, no se ha podido registrar " + response.error, "Err");

          }else{
            $mdDialog.cancel();
            var jwt = cookiesService.create_jwt(response[0].id_user);
            console.log(jwt);
            var data = {"token":jwt, "id":response[0].id_user};
            var data_users_JSON = JSON.stringify(data);

            services.post('login', 'update_token', data_users_JSON).then(function (response) {
              console.log(response);
              if (response.success) {
                console.log(jwt);
                cookiesService.setUser(jwt);
                UserService.login();

              }else {
                console.log("Error al actualizar token " + response.error);
                    
              }

            });


          }

        });
                              
      }).catch(function(error) {
        console.log('Se ha encontrado un error:', error);

      });
    }

    
    
});