freelancer.factory("UserService", ['$location', '$rootScope', 'services', 'cookiesService','token_services',
    function ($location, $rootScope, services, cookiesService, token_services) {
        var service = {};
        service.login = login;
        service.logout = logout;
        return service;

        function login() {
            token_services.get_info().then(function(response){
                console.log(response);
                if (response.success) {
                    console.log($rootScope);
                    $rootScope.login = false;
                    $rootScope.logout = true;
                    $rootScope.prof_avatar = true;
                    $rootScope.prof_name = true;
                    $rootScope.forum = true;
                    $rootScope.top_users = true;
                        
                    $rootScope.avatar = response.response.user[0].avatar;
                    $rootScope.nombre = response.response.user[0].name;
                        
                    if (response.response.user[0].type == 0) {
                        console.log("eres client");
                        $rootScope.profile = true;
                        $rootScope.freelancer = true;
                        $rootScope.prof_name = true;
                            
                    }else if (response.response.user[0].type == 1) {
                        console.log("eres admin");
                        $rootScope.profile = true;
                        $rootScope.freelancer = true;
                        $rootScope.prof_name = true;
                        $rootScope.admin = true;
                           
                    }else {
                        console.log("eres user normall");
                        $rootScope.profile = false;
                        $rootScope.freelancer = false;
                        $rootScope.prof_name = false;
                            
                    }
                          
                }else {
                    console.log("eres user normal");
                    $rootScope.profile = false;
                    $rootScope.freelancer = false;
                    $rootScope.forum = true;
                    $rootScope.top_users = true;
                              
                }

            });
            
        }

        function logout() {
            $rootScope.login = true;
            $rootScope.logout = false;
            $rootScope.freelancer = false;
            cookiesService.clearUser();
            $rootScope.profile = false;
            $rootScope.prof_name = false;
            $rootScope.admin = false;
            
            //$location.path('/');
            window.location.href="http://localhost/freelancer";

        }
        
}]);
