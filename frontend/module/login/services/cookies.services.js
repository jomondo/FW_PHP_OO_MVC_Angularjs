freelancer.factory("cookiesService", function ($cookies) {
        var service = {};
        service.getBase64_encode = getBase64_encode;
        service.getBase64_decode = getBase64_decode;
        service.do_encode = do_encode;
        service.do_decode = do_decode;
        service.create_jwt = create_jwt;
        service.setUser = setUser;
        service.getUser = getUser;
        service.clearUser = clearUser;
        return service;

        function do_encode(data){
            return getBase64_encode(JSON.stringify(data));
        }

        function do_decode(data){
            return getBase64_decode(data);
        }

        function getBase64_encode(data){
            var dataArr = CryptoJS.enc.Utf8.parse(data);
            var rdo = CryptoJS.enc.Base64.stringify(dataArr);

            return rdo;
        }

        function getBase64_decode(data){
            var dataArr = CryptoJS.enc.Base64.parse(data);
            var rdo = dataArr.toString(CryptoJS.enc.Utf8);

            return rdo;
        }

        function create_jwt(payload){
            var header = { "typ":"JWT","alg":"HS256"};
            var random = Math.random(0,100);

            var secret = "password"+random;

            var base64_header = getBase64_encode(JSON.stringify(header));
            //console.log(base64_header);
            var base64_payload = getBase64_encode(JSON.stringify(payload));

            var signature = CryptoJS.HmacSHA256(base64_header + "." + base64_payload, secret);
            var base64_sign = CryptoJS.enc.Base64.stringify(signature);

            var jwt = base64_header + "." + base64_payload + "." + base64_sign;
            console.log(jwt);
            return jwt;
        }

        function setUser(token_user){
            $cookies.putObject("session", 
            {token: token_user}, 
            {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});

        }

        function getUser() {
            var token_user = $cookies.getObject("session");
            if (token_user) {
                console.log(token_user);
                
            }
            return token_user;
        }

        function clearUser() {
            $cookies.remove("session");
        }


    });
