freelancer.factory("token_services", function ($q, $rootScope, services, cookiesService) {
	var service = {};
    service.get_info = get_info;
    return service;

    function get_info() {
    	var deferred = $q.defer();
    	var user = cookiesService.getUser();
    	console.log(user);
    	
    	if (user) {
    		var data = {"token":user.token}
	        var data_users_JSON = JSON.stringify(data);

	        services.post('login', 'select_user_token', data_users_JSON).then(function (response) {
	        	deferred.resolve({ success: true, response });

	        }).catch(function(error) {
	            console.log( 'Error al obtener datos token - error 503 - ' + error );
	            
	        });

	        return deferred.promise;

    	}else{
            deferred.resolve({ success: false });
            return deferred.promise;
            
    	}
    	
    }

});