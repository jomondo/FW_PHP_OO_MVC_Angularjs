//modal
freelancer.controller('modal_controller', function($scope, $rootScope, $mdDialog, $interval, services, CommonService, cookiesService, UserService, google_services, facebook_services, twitter_services ) {
  $scope.theme = 'red';
  $rootScope.login = true;
  $rootScope.logout = false;

  var isThemeRed = true;

  $interval(function () {
    $scope.theme = isThemeRed ? 'blue' : 'red';

    isThemeRed = !isThemeRed;
  }, 2000);

  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'frontend/module/login/view/modal_login.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
  };

  $scope.log_out = function() {
      UserService.logout();
    };

  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };

  }

  /////// buttons login social network ///////
  $scope.log_google = function() {
    //console.log(google_services);
    google_services.google();

  };

  $scope.log_fb = function() {
    facebook_services.facebook();

  };

  $scope.log_tw = function() {
    twitter_services.twitter();

  };

});

  ///////////// signup manual ////////////////
  freelancer.controller('signup_controller', function ($scope, services, $location, $timeout, CommonService, UserService, cookiesService, $mdDialog) {
    $scope.regist = false;
    $scope.btn_regist = true;
    $scope.login = true;
    $scope.recovery = false;
    $scope.btn_recovery = true;
    $scope.exist = false;

    $scope.SubmitSignUp = function () {
      var data = {"id": $scope.signup.inputUser, "email": $scope.signup.inputEmail,"password": $scope.signup.inputPass, "re_passwd": $scope.signup.inputPass2};
      var data_users_JSON = JSON.stringify(data);
      console.log(data_users_JSON);
      services.post('login', 'alta_signup', data_users_JSON).then(function (response) {
        console.log(response);
        if (response.success) {
          console.log("usuario dado de alta, activar = 0");
          $mdDialog.cancel();
          $timeout(function () {    
            CommonService.banner("El usuario se ha dado de alta correctamente, revisa su correo para activarlo", "");
          }, 2000);
          $location.path('/');

        }else {
          console.log(response.error);
          $scope.exist = true;
              
        }

      });

    };

    ///// hide form and buttons /////
    $scope.showRegist = function () {
      $scope.regist = true;
      $scope.login = false;
      $scope.btn_regist = false;
      $scope.recovery = false;
      $scope.btn_recovery = false;

    };

    $scope.showChangePasswd = function () {
      $scope.recovery = true;
      $scope.login = false;
      $scope.btn_recovery = false;

    };

    ///////////// login manual /////////////
    $scope.SubmitLogin = function () {
      $scope.error_passwd=false;
      $scope.errorpass = "";
      $scope.error_name=false;
      $scope.errorname = "";

      var data = {"id":$scope.log.user,"password":$scope.log.pass};
      var data_users_JSON = JSON.stringify(data);
      console.log(data_users_JSON);

      services.post('login', 'login', data_users_JSON).then(function (response) {
        console.log(response);

        if (response.success) {
          $mdDialog.cancel();
          //console.log(response.user[0].id_user);
          var jwt = cookiesService.create_jwt(response.user[0].id_user);
          console.log(jwt);
          var data = {"token":jwt, "id":response.user[0].id_user};
          var data_users_JSON = JSON.stringify(data);

          services.post('login', 'update_token', data_users_JSON).then(function (response) {
            console.log(response);
            if (response.success) {
              console.log(jwt);
              cookiesService.setUser(jwt);
              UserService.login();

            }else {
              console.log("Error al actualizar token " + response.error);
                  
            }

          });
        }else {
          if (response.error.password) {
            $scope.error_passwd=true;
            $scope.errorpass = response.error.password;
          }else{
            $scope.error_name=true;
            $scope.errorname = response.error.id;
          }
              
        }

      });

    };
    //////// submit change passwd with token to email ////////
    $scope.SubmitChange = function () {
      $scope.error_email=false;
      $scope.error_email = "";

      var data = {"email":$scope.change.email};
      var data_users_JSON = JSON.stringify(data);
      console.log(data_users_JSON);
      services.post('login', 'check_email', data_users_JSON).then(function (response) {
        console.log(response);
        if (response.success) {
          $mdDialog.cancel();
          CommonService.banner("Le hemos enviado un correo. Por favor, verifique su email para poder cambiar su password", "");

        }else {
          if (response.msg) {
            $scope.error_email=true;
            $scope.erroremail = response.msg;
          }else{
            CommonService.banner("Ha ocurrido un error " + response.error, "Err");

          }
              
        }

      });

    };
    
});

/////////////// submit new passwd to db ////////////////
freelancer.controller('change_passwd_controller', function ($route, $scope, services, $location, CommonService, $timeout) {
    $scope.token = $route.current.params.token;

    $scope.SubmitChangePass = function () {
        var data = {"password": $scope.changepass.inputPassword, "token": $scope.token};
        var passwd = JSON.stringify(data);
        console.log(passwd);

        services.post('login', 'change_passwd', passwd).then(function (response) {
          console.log(response);
          if (response.success) {
            $timeout(function () {    
              CommonService.banner("Su password ha sido actualizado correctamente", "");
            }, 2000);
            $location.path('/');

          } else {
            CommonService.banner("Error al actualizar password", "Err");

          }

        });
    };
});