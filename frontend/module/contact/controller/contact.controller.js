freelancer.controller('contactController', function ($scope, services, CommonService, $rootScope) {
    CommonService.breadcrumb(true, "Contact", "contact", "Enviar consulta");
    CommonService.underlined_menu("contact");

    $scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };

    $scope.SubmitContact = function () {
        var data = {"inputName": $scope.contact.inputName, "inputEmail": $scope.contact.inputEmail, 
        "inputSubject": $scope.contact.inputSubject, "inputMessage": $scope.contact.inputMessage,"token":'contact_form'};
        var contact_form = JSON.stringify(data);
        console.log(contact_form);
        services.post('contact', 'process_consulting', contact_form).then(function (response) {
            console.log(response);
            if (response=="TRUE") {
                $scope.contact = null;
                CommonService.banner("Ha enviado correctamente su consulta", "");

            }else{
                CommonService.banner("Error, no se ha podido entregar su consulta", "Err");

            }

        });
    };
});