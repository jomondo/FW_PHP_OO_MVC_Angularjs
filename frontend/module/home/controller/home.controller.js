freelancer.controller('mainController', function ($scope, $rootScope, UserService, services) {
    UserService.login();
    console.log($rootScope);
    services.get("admin","control_visitas").then(function(response){
        //console.log(response);

    });

});

freelancer.controller('banner_controller', function ($scope, $rootScope) {
    $rootScope.bannerV = false;
    $rootScope.bannerText = "";

});

freelancer.controller('rating_controller', function ($scope, token_services, CommonService, services, $rootScope) {
    $scope.get_stars = function(){
        console.log($scope.test);
        token_services.get_info().then(function(response){
            if (response.success) {
                if ($scope.test==undefined) {
                    $scope.test=2;

                }
                var data = { "id_valorador":response.response.user[0].id_user,"dni_valorado":$rootScope.dni, "stars":$scope.test };
                var data_rating_JSON = JSON.stringify(data);
                console.log(data_rating_JSON);

                services.post("home", "submitValueFreelancer", data_rating_JSON).then(function (response) {
                    console.log(response);
                    if (response.success) {
                        CommonService.banner("Su valoracion ha sido satisfactoria","");

                    }else{
                        CommonService.banner("No puedes votar dos veces al mismo usuario","");

                    }

                });

            }else{
                CommonService.banner("Tienes que estar registrado para poder valorar","Err");

            }

        }).catch(function(error) {
            console.log( 'Error al obtener info del token ' + error );

        });

    };

});

freelancer.controller('verify_controller', function (UserService, $location, CommonService, $route, services, cookiesService, $timeout) {
    var token = $route.current.params.token;
    if (token.substring(0, 3) !== 'Ver') {
        $timeout(function () {
          CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
        }, 1000);
        $location.path('/');

    }
    services.get("login", "verify", token).then(function (response) {
        if (response.success) {
            $timeout(function () {
                CommonService.banner("Cuenta verificada correctamente, ya puede iniciar sesion", "");
            }, 1000);
            $location.path('/');

        }else {
            CommonService.banner("Su cuenta no ha sido verificada " + response.error, "Err");
            
        }

    });
});

//scroll
freelancer.controller("home_controller", function($scope, data, services, $rootScope, CommonService){
    console.log(data[0]);

    CommonService.breadcrumb(false, "", "", "");
    CommonService.underlined_menu("home");

    $scope.category = data[0];
    $scope.items = [data[0].categ_prof,data[1].categ_prof];
    
    var i = 2;
    $scope.more = function(){
        if (i<5) {
            for(line in data[i]){
                var newItem = data[i].categ_prof;
                console.log(newItem);
                $scope.items.push(newItem);
            }
            i++;
        }
    
    };


    //enviar contador para sacar las 4 categorias mas visitadas
    $scope.submit_cont = function(data){
        //console.log(data);
        var name_categ = data;
        var dat = {"categ_prof":name_categ}
        var data_JSON = JSON.stringify(dat);
        services.post('home', 'valCateg', data_JSON).then(function (data) {
            console.log("cont: "+data.value[0].value);
            var update_categ = parseInt(data.value[0].value) + parseInt(1);
            var data = {"val_categ":update_categ,"categ_prof":name_categ}
            var data_json = JSON.stringify(data);
            services.post('home', 'updateValCateg', data_json).then(function (data) {
                console.log(data);
            });

        });

    };
  
});

freelancer.controller('list_controller', function ($scope, categ, especialist, province, data, services, list_map, $rootScope, CommonService, filter_services) {
    console.log(province.response);
    console.log(categ);
    console.log(especialist);
    
    CommonService.breadcrumb(true, "home", "", "List Freelancer");
    CommonService.underlined_menu("freelancer");

    /* llenar combos */
    $scope.categ = categ.response;
    $scope.especialist = especialist.response;
    $scope.provinces = province.response;

    $scope.user = data;
    $scope.filtered_freelancer = [];
    $scope.currentPage = 1;
    $scope.data = data;
    $scope.array = [];
    $scope.markers = [];

    list_map.cargarmap($scope.data, $scope);
    $scope.filtered_freelancer = $scope.data.slice(0, 3);

    $scope.pageChanged = function() {
	  var startPos = ($scope.currentPage - 1) * 3;
	  $scope.filtered_freelancer = $scope.data.slice(startPos, startPos + 3);
	  
	};

    ///////////// filters /////////////////
    $scope.category = function(data) {
        $scope.array = [];
        $rootScope.name_categ = data;

    };

    $scope.especialista = function(data) {
        $scope.array = [];
        $rootScope.name_especial = data;

    };

    $scope.province = function(data) {
        $scope.array = [];
        $rootScope.name_province = data;

    };

    $scope.submit_filter = function() {
        filter_services.more_filter($scope);
        
        if ($scope.array.length == 0) {
            CommonService.banner("No hay resultados, lo sentimos","Err");

        }else{
            $scope.user = $scope.array;
            list_map.cargarmap($scope.array, $scope);

            $scope.filtered_freelancer = $scope.array.slice(0, 3);

            $scope.pageChanged = function() {
                var startPos = ($scope.currentPage - 1) * 3;
                $scope.filtered_freelancer = $scope.array.slice(startPos, startPos + 3);
                
            };

        }
        
    };

});

freelancer.controller('details_controller', function (likes, num_votaciones, $scope, data, list_map, $rootScope, CommonService, token_services, services, details_services) {
    // angular.element(document.querySelector(".list-group-item")).css("min-height","0px");
    CommonService.breadcrumb(true, "home", "", "Details");
    console.log(num_votaciones.value[0].media);
    console.log(num_votaciones.value[0].count_votaciones);

    $scope.num_votaciones = num_votaciones.value[0].count_votaciones;
    $scope.media = num_votaciones.value[0].media;
    $scope.user = data.val[0];
    $rootScope.dni = data.val[0].dni;
    $scope.likes = likes.response.value[0].count;
    $scope.markers = [];

    list_map.cargarmap($scope.user, $scope);



    $scope.submit_like = function(dni_like) {
        console.log(dni_like);

        token_services.get_info().then(function(response){
            console.log(response);
            if (response.success) {

                var data = { "id_user":response.response.user[0].id_user,"dni_like":$rootScope.dni };
                var data_like_JSON = JSON.stringify(data);
                console.log(data_like_JSON);

                services.post("home", "submit_like", data_like_JSON).then(function(response){
                    console.log(response);
                    if (response.success) {
                        CommonService.banner("Te gusta este freelancer","");
                        details_services.get_likes_reset(dni_like, $scope);

                    }else{
                        CommonService.banner(response.msg,"Err");

                    }

                });

            }else{
                CommonService.banner("Tienes que registrarte para ponerlo como favoritos", "Err");

            }

        });

    };

    $scope.submit_stop_like = function(dni_like) {
        console.log(dni_like);

        token_services.get_info().then(function(response){
            console.log(response);
            if (response.success) {

                var data = { "id_user":response.response.user[0].id_user,"dni_like":$rootScope.dni };
                var data_like_JSON = JSON.stringify(data);
                console.log(data_like_JSON);

                services.post("home", "stop_like", data_like_JSON).then(function(response){
                    console.log(response);
                    if (response.success) {
                        details_services.get_likes_reset(dni_like, $scope);

                    }

                });

            }else{
                CommonService.banner("Tienes que registrarte antes", "Err");

            }

        });

    };

    
});

freelancer.controller('list_categories', function ($scope,categ, especialist, province, data, $route, list_map, filter_services, $rootScope, CommonService) {
    console.log(data);

    CommonService.breadcrumb(true, "home", "", "List Categories");
    CommonService.underlined_menu("freelancer");

    /* llenar combos */
    $scope.categ = categ.response;
    $scope.especialist = especialist.response;
    $scope.provinces = province.response;

    $scope.category = $route.current.params.cat;
    $scope.filtered = [];
    $scope.currentPage = 1;
    $scope.array = [];
    $scope.markers = [];
    $scope.data = data;

    filter_services.load("categ_prof", $scope.category, $scope);

    $scope.val = $scope.array;
    list_map.cargarmap($scope.val, $scope);
    $scope.filtered = $scope.array.slice(0, 3);
    
    console.log($scope.filtered);
    $scope.pageChange = function() {
        var startPos = ($scope.currentPage - 1) * 3;
        $scope.filtered = $scope.array.slice(startPos, startPos + 3);
        console.log($scope.filtered);
    };


    ///////////// filters /////////////////
    $scope.category = function(data) {
        $scope.array = [];
        $rootScope.name_categ = data;

    };

    $scope.especialista = function(data) {
        $scope.array = [];
        $rootScope.name_especial = data;

    };

    $scope.province = function(data) {
        $scope.array = [];
        $rootScope.name_province = data;

    };

    $scope.submit_filter = function() {
        filter_services.more_filter($scope);
        
        if ($scope.array.length == 0) {
            CommonService.banner("No hay resultados, lo sentimos","Err");

        }else{
            $scope.val = $scope.array;
            list_map.cargarmap($scope.array, $scope);

            $scope.filtered = $scope.array.slice(0, 3);

            $scope.pageChanged = function() {
                var startPos = ($scope.currentPage - 1) * 3;
                $scope.filtered = $scope.array.slice(startPos, startPos + 3);
                
            };

        }
        
    };

});

freelancer.controller('list_typeahead', function ($scope,categ, especialist, province, data, $route, CommonService, list_map, filter_services, $rootScope) {
    console.log(data);

    CommonService.breadcrumb(true, "home", "", "List Typeahead");
    CommonService.underlined_menu("freelancer");

    /* llenar combos */
    $scope.categ = categ.response;
    $scope.especialist = especialist.response;
    $scope.provinces = province.response;

    var especialista;
    $scope.especialista = $route.current.params.esp;
    $scope.filtered = [];
    $scope.currentPage = 1;
    $scope.array = [];
    $scope.markers = [];
    $scope.data = data;
    
    filter_services.load("especialista", $scope.especialista, $scope);
    
    $scope.val = $scope.array;
    list_map.cargarmap($scope.val, $scope);
    $scope.filtered = $scope.array.slice(0, 3);
    
    $scope.data = $scope.array;
    $scope.pageChange = function() {
        var startPos = ($scope.currentPage - 1) * 3;
        $scope.filtered = $scope.array.slice(startPos, startPos + 3);
        
    };

    ///////////// filters /////////////////
    $scope.category = function(data) {
        $scope.array = [];
        $rootScope.name_categ = data;

    };

    $scope.especialista = function(data) {
        $scope.array = [];
        $rootScope.name_especial = data;

    };

    $scope.province = function(data) {
        $scope.array = [];
        $rootScope.name_province = data;

    };

    $scope.submit_filter = function() {
        //console.log($scope.data1);
        filter_services.more_filter($scope);
        
        if ($scope.array.length == 0) {
            CommonService.banner("No hay resultados, lo sentimos","Err");

        }else{
            $scope.val = $scope.array;
            list_map.cargarmap($scope.array, $scope);

            $scope.filtered = $scope.array.slice(0, 3);

            $scope.pageChanged = function() {
                var startPos = ($scope.currentPage - 1) * 3;
                $scope.filtered = $scope.array.slice(startPos, startPos + 3);
                
            };

        }
        
    };
    
});

freelancer.controller('CarouselDemoCtrl', function ($scope) {
    var cont = 0;
    $scope.myInterval = 5000;
    $scope.noWrapSlides = false;
    $scope.active = 0;
    var slides = $scope.slides = [];
    var currIndex = 0;
    //'//unsplash.it/' + newWidth + '/300'
    
    $scope.addSlide = function() {
        cont++;
        //var newWidth = 1700 + slides.length + 1;
        slides.push({
            image: '/freelancer/frontend/assets/media/banner'+cont+'.jpg',
            id: currIndex++
        });
    };

    for (var i = 0; i <= 4; i++) {
        $scope.addSlide();
    }

});

freelancer.controller('best_freelancer_controller', function ($scope, services) {
    var cont = 0;
    $scope.myInterval = 5000;
    $scope.noWrapSlides = false;
    $scope.active = 0;
    var slides = $scope.slides = [];
    var currIndex = 0;

    services.get('home', 'get_best_freelancer').then(function(response){
        console.log(response);
        //$scope.slides = response;
        for (var i = 0; i <= 4; i++) {
            slides.push({
                image: response[i].avatar,
                id: currIndex++,
                name: response[i].name,
                category: response[i].categ_prof,
                dni: response[i].dni
            });
        }

    });

});


freelancer.controller('TypeaheadCtrl', function($scope, services, CommonService) {

    $scope.states = [];
    services.get('home', 'obtainEspecialist').then(function (data) {
        //console.log(data);    
        /*data.forEach(function(elemento){
            $scope.states.push(elemento.especialista);

        });*/

        data.map(function (elemento) {
            console.log("element: " + elemento.especialista);
            $scope.states.push(elemento.especialista);
        });
        
    });
    
    $scope.getVal = function() {
        //console.log($scope.selected);
        if ($scope.selected==undefined) {
            CommonService.banner("Tiene que escribir almenos una letra", "Err");

        }else{
            window.location.href="#/list_typeahead/"+$scope.selected;

        }

    };

});

freelancer.controller('top_users_controller', function(users, $scope, services, CommonService) {
    console.log(users);
    CommonService.breadcrumb(true, "home", "top users", "Top Users");
    CommonService.underlined_menu("top_users");
    $scope.users = users;

});


