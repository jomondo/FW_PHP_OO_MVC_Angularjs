freelancer.factory('details_services', function (services,$q) {
	var service = {};
    service.get_likes_details = get_likes_details;
    service.get_likes_reset = get_likes_reset;
    return service;

    function get_likes_details(dni) {
        var deferred = $q.defer();
        var data = {"dni":dni};
    
        var data_user_JSON = JSON.stringify(data);
        // return services.post('home', 'get_likes_details', data_user_JSON);

        services.post('home', 'get_likes_details', data_user_JSON).then(function(response){
            console.log(response);
            deferred.resolve({ success: true, response });
            
        }).catch(function(error) {
            console.log( 'Error al obtener datos likes - error 503 - ' + error );
            

        });

        return deferred.promise;
            
    }

    function get_likes_reset(dni, $scope) {
        var data = {"dni":dni};
    
        var data_user_JSON = JSON.stringify(data);
        // return services.post('home', 'get_likes_details', data_user_JSON);

        services.post('home', 'get_likes_details', data_user_JSON).then(function(response){
            console.log(response);
            $scope.likes = response.value[0].count;
            
            
        }).catch(function(error) {
            console.log( 'Error al obtener datos likes - error 503 - ' + error );
            

        });
      
    }

});