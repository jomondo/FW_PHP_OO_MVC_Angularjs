freelancer.factory("filter_services", function ($q, $rootScope, CommonService, services) {
	var service = {};
    service.load = load;
    service.more_filter = more_filter;
    service.obtain_province_combo = obtain_province_combo;
    service.obtain_category_combo = obtain_category_combo;
    service.obtain_especialist_combo = obtain_especialist_combo;
    service.get_likes = get_likes;
    return service;

    function load(val1, $val2, $rootScope) {
        for (var i = 0; i < $rootScope.data.length; i++) {
           	if (val1=="especialista") {
            	if($rootScope.data[i].especialista.toLowerCase().indexOf($val2.toLowerCase()) != -1){
			        $rootScope.array.push($rootScope.data[i]);

			    }
        	}else if(val1=="categ_prof"){
        		if($rootScope.data[i].categ_prof.toLowerCase().indexOf($val2.toLowerCase()) != -1){
			        $rootScope.array.push($rootScope.data[i]);

			    }
        	}
		        
		}

		if ($rootScope.array.length===0) {
		    CommonService.banner("No hay ningun resultado", "Err");
		             
		}
            
    }

    function more_filter($scope) {
        if ( $rootScope.name_categ !== undefined && $rootScope.name_especial !== undefined && $rootScope.name_province !== undefined ) {
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].categ_prof.toLowerCase() == $rootScope.name_categ.toLowerCase() && $scope.data[i].especialista.toLowerCase() == $rootScope.name_especial.toLowerCase() && $scope.data[i].province.toLowerCase() == $rootScope.name_province.toLowerCase()) {
                    $scope.array.push($scope.data[i]);

                }

            }

        }else if ($rootScope.name_categ !== undefined && $rootScope.name_especial !== undefined) {
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].categ_prof.toLowerCase() == $rootScope.name_categ.toLowerCase() && $scope.data[i].especialista.toLowerCase() == $rootScope.name_especial.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }

            }

        }else if ($rootScope.name_categ !== undefined && $rootScope.name_province !== undefined) {
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].categ_prof.toLowerCase() == $rootScope.name_categ.toLowerCase() && $scope.data[i].province.toLowerCase() == $rootScope.name_province.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }

            }

        }else if ($rootScope.name_province !== undefined && $rootScope.name_especial !== undefined) {
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].province.toLowerCase() == $rootScope.name_province.toLowerCase() && $scope.data[i].especialista.toLowerCase() == $rootScope.name_especial.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }

            }

        }else if ($rootScope.name_categ !== undefined) {
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].categ_prof.toLowerCase() == $rootScope.name_categ.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }
            }

    	}else if($rootScope.name_especial !== undefined){
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].especialista.toLowerCase() == $rootScope.name_especial.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }
            }

    	}else if($rootScope.name_province !== undefined){
            for (var i = 0; i < $scope.data.length; i++) {

                if ( $scope.data[i].province.toLowerCase() == $rootScope.name_province.toLowerCase() ) {
                    $scope.array.push($scope.data[i]);

                }
            }

    	}

    }

    function obtain_province_combo() {
        var deferred = $q.defer();

        services.get('home', 'obtainProvince').then(function(response){
            console.log(response);
            deferred.resolve({ success: true, response });
            
        }).catch(function(error) {
            console.log( 'Error al obtener datos provincias - error 503 - ' + error );
            //pillar de json

        });

        return deferred.promise;
            
    }

    function obtain_category_combo() {
        var deferred = $q.defer();

        services.get('home', 'obtainCateg').then(function(response){
            console.log(response);
            deferred.resolve({ success: true, response });
            
        }).catch(function(error) {
            console.log( 'Error al obtener datos categorias - error 503 - ' + error );

        });

        return deferred.promise;
            
    }

    function obtain_especialist_combo() {
        var deferred = $q.defer();

        services.get('home', 'obtainEspecialist').then(function(response){
            console.log(response);
            deferred.resolve({ success: true, response });
            
        }).catch(function(error) {
            console.log( 'Error al obtener datos especialista - error 503 - ' + error );

        });

        return deferred.promise;
            
    }

    function get_likes(data, id_user, $scope){
        //console.log(data);
        angular.forEach(data, function(value, key) {
            if (value.id_user==id_user) {
                /*angular.forEach($scope.freelancer, function(val, key) {
                    if (value.dni_freelancer == val.dni) {
                        $scope.array.push(val);

                    }
                });*/

                $scope.freelancer.map(function (elemento) {
                    if (value.dni_freelancer == elemento.dni) {
                        $scope.array.push(elemento);

                    }
                    
                });

            }
        });
        
    }
    
});