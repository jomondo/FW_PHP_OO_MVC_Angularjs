function do_encode(data){
	return getBase64_encode(JSON.stringify(data));
}

function do_decode(data){
	return getBase64_decode(data);
}

function getBase64_encode(data){
	var dataArr = CryptoJS.enc.Utf8.parse(data);
	var rdo = CryptoJS.enc.Base64.stringify(dataArr);

	return rdo;
}

function getBase64_decode(data){
	var dataArr = CryptoJS.enc.Base64.parse(data);
	var rdo = dataArr.toString(CryptoJS.enc.Utf8);

	return rdo;
}

function create_jwt(payload){
	var header = { "typ":"JWT","alg":"HS256"};
	var secret = "password1";

	var base64_header = getBase64_encode(JSON.stringify(header));
	//console.log(base64_header);
	var base64_payload = getBase64_encode(JSON.stringify(payload));

	var signature = CryptoJS.HmacSHA256(base64_header + "." + base64_payload, secret);
	var base64_sign = CryptoJS.enc.Base64.stringify(signature);

	var jwt = base64_header + "." + base64_payload + "." + base64_sign;
	console.log(jwt);
	return jwt;
}

