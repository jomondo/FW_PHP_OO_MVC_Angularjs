
function load_data_ajax(url,type,value,name_label,make){
	/*
	url = go to controller
	type = (POST or GET);
	make = 1 -> redirect
	*/
	//alert("entra en load_data_ajax");
	
	if (type=="POST" && make == 1) {
		$.ajax({
			url:url,
			type:type,
			data: {data:value},
			success:function(data){
				//alert("asd2:"+data);
				var json = JSON.parse(data);
				if (json.success==true) {
					//alert("entra en redirectyy");
					localStorage.setItem("redirect",json.ref);
					var aut = localStorage.getItem("auto_list");
					var auts = localStorage.getItem("page_home");

					if (aut=="auto_listar" && auts=="home") {
						window.location.href = json.redirect;
						return false;
						
					}else if(aut=="auto_listar"){
						//alert("entra especial");
						localStorage.removeItem("auto_list");
						window.location.href = "../listUser/";
						return false;

					}
					
					window.location.href = json.redirect;

				}

			},
			error:function(xhr){
				alert(xhr.responseText);

			}

		});

	}if (type== "GET" && make == "scroll") {
		$.ajax({
			url:url,
			type:type,
			success:function(data){
				//alert(data);
				var json = JSON.parse(data);
				if (json.error) {
					getJsonCategory();

				}else{
					paintCategories(0, 1, json);
					json.forEach(function(elemento){
			            cont_db++;

			        });
			        scroll(json, cont_db);
			        
				}
			},
			error:function(xhr){
				//alert(xhr.status);
				if (xhr.status==503) {
					var json = JSON.parse(xhr.responseText);
					alert(json.error);//redirigir al modul 503

				}
			}
		});

	}if (type== "POST" && make == "details") {
		//alert("entra en details");
		$.ajax({
            url: url,
            type: type,
            data:{id:value},
            success:function (response) {
                //alert(response);
                var json = JSON.parse(response);
                //alert(json.redirect);
                if (json.redirect) {
                    window.location.href = json.redirect;
                }
                    
            },
            error:function(xhr){
                alert(xhr.responseText);
            }

        });
        
	}
	if (type=="POST" && make == "list_page") {
		$.ajax({
		    url: url,
		    type: type,
		    data:{num:value},
		    success:function (response) {
		        //alert(response);
		        var json = JSON.parse(response);
		        //alert(json.data.length);
		        if (json.data.length==1) {
		        	//alert("entra en 1");
		        	var dni = json.data[0].dni;
		        	var data = {"dni":dni};
		        	var dni_JSON = JSON.stringify(data);
		        	load_data_ajax("../../home/details_list/","POST",dni_JSON,"","details");
		        	return false;

		        }
		        
		        geolocation ("../../home/get_markets/",json.data, "list_maps");
		        paintListFreelancer(json,name_label);

		    },
		    error:function(xhr){
		        alert(xhr.responseText);
		    }

		});
	}

	if (type=="GET" && make == "paintHome") {
		$.ajax({
			url: url,
			type: type,
			success:function (response) {
				//alert(response);
				var json = JSON.parse(response);
				if (value == "paintbestFreelancer") {
					//alert(response);
					paintListHome(json);
					return false;

				}

				if (value == "paintStatisticHome") {
					paintStat_Home(json[0].count,name_label);
					return false;

				}

				if (value == "paintBestClientHome") {
					//alert("entra en paintBestClient");
					paintBestClient(json);
					return false;

				}
		                        
			},
			error:function(xhr){
				alert(xhr.responseText);
			}

		});
	}
	// sumar las veces que entramos en categorias
	if (type=="POST" && make == 2) {
		$.ajax({
		    url: url,
		    type: type,
		    data:{data:value},
		    success:function (response) {
		        //alert("aci: "+response);
		        var json = JSON.parse(response);
		        
		        var val_categ = json.value[0].value;
		        var name_categ = json.value[0].name_catg;
		        
		        //alert(val_categ);
		        if (val_categ=="") {
		        	val_categ = 0;

		        }

		        var update_categ = parseInt(val_categ) + parseInt(1);
		        //alert(update_categ);
		        var data = {"val_categ":update_categ,"categ_prof":name_categ}
				var data_json = JSON.stringify(data);
				console.log(data_json);
				localStorage.setItem("data_categ",data_json);
		        //load_data_ajax("../home/updateValCateg/","POST",data_json,"",3);

		    },
		    error:function(xhr){
		        alert(xhr.responseText);
		    }

		});
	}
	//resultado cont categorias home
	if (type=="POST" && make == 3) {
		$.ajax({
		    url: url,
		    type: type,
		    data:{data:value},
		    success:function (response) {
		        //alert(response);
		        console.log(response);
		        localStorage.removeItem("data_categ");
		        

		    },
		    error:function(xhr){
		        alert(xhr.responseText);
		    }

		});
	}

	if (type=="GET" && make == "filters") {
		$.ajax({
		    url: url,
		    type: type,
		    success:function (response) {
		        //alert(response);
		        var json = JSON.parse(response);
		        if (json[0].name_catg) {
		        	$.each(json, function (i, valor) {
						//alert(valor.name_catg);
				    	$("#filter_especial").append("<option id='"+valor.name_catg+"'>" + valor.name_catg + "</option>");

				    });
				    return false;
		        }else if(json[0].especialista){
		        	$.each(json, function (i, valor) {
						//alert(valor.name_catg);
				    	$("#filter_especialista").append("<option id='"+valor.especialista+"'>" + valor.especialista + "</option>");

				    });
				    return false;
		        }else if(json[0].province){
		        	$.each(json, function (i, valor) {
						//alert(valor.name_catg);
				    	$("#filter_province").append("<option id='"+valor.province+"'>" + valor.province + "</option>");

				    });
				    return false;
		        }else if(json[0].price_h){
		        	$.each(json, function (i, valor) {
						//alert(valor.name_catg);
				    	$("#filter_price_h").append("<option id='"+valor.price_h+"'>" + valor.price_h + "</option>");

				    });
				    return false;
		        }
		        
		        
		        

		    },
		    error:function(xhr){
		        alert(xhr.responseText);
		    }

		});
	}

	if (type=="POST" && make == "obtain_filters") {
		$.ajax({
		    url: url,
		    type: type,
		    data:{data:value},
		    success:function (response) {
		        //alert(response);
		        localStorage.removeItem("slider_price_min");
		        localStorage.removeItem("slider_price_max");
		        var json = JSON.parse(response);

		        var url1 = "ListFilter";
			    var url2 = "paintListFilter";
			    var name_list = "listar";
			    var name_pag = "pag_filter";

			    pagination(url1, url2, name_list, name_pag);
		        

		    },
		    error:function(xhr){
		        alert(xhr.responseText);
		    }

		});
	}if (type=="POST" && make == "update_token_manual") {
		$.ajax({
            url: amigable(url),
            type:type,
            data:{data:value},
            success:function(response){
                alert(response);
                var json = JSON.parse(response);
                if (json.success) {
                    alert("user updated");
                    var tokenInit = {"token":name_label, "log_man":true};
                    var tokenToInit = JSON.stringify(tokenInit);
                    localStorage.setItem("datos_user",tokenToInit);
                    window.location.href = amigable("?module=home");

                }
                return false;     
            },
            error:function(xhr){
                alert(xhr.responseText);
            },

        });
	}if (type=="POST" && make == "update_token_social") {
		$.ajax({
         	url: amigable(url),
          	type: type,
          	data:{data:value},
          	success:function(response){
            	alert(response);
            	var json = JSON.parse(response);
            	if (json.success) {
              		alert("user updated");
              		var tokenInit = {"token":name_label};
              		var tokenToInit = JSON.stringify(tokenInit);
              		localStorage.setItem("datos_user",tokenToInit);
              		window.location.href = amigable("?module=home");

            	}
            	return false;     
          	},
          	error:function(xhr){
            	alert(xhr.responseText);
          	},

        });
	}if (type=="POST" && make == "value_web") {
		$.post(amigable(url),{data:value},function (response){
            alert(response);
            var json = JSON.parse(response);
            //alert(json.success);
            console.log("success: "+json.success);
            if (json.success) {
                $("#value_stars").val('');
                $("#message_stars").val('');
                
                amaran("Gracias por valorar nuestra web. Te estamos muy agradecidos.");
                
            }else{
            	amaran(json.mgs);

            }
                
        }).fail(function(xhr){
            alert(xhr.responseText);    

        });
          
	}if (type=="POST" && make == "displayProfile_social") {
		$.ajax({
	      url: amigable(url),
	      type: type,
	      data:{data:value},
	      success:function (response) {
	        alert(response);
	        localStorage.removeItem('access_controller');
	        var json = JSON.parse(response);
	        
	        if (json.success) {
	          localStorage.setItem("datos_user","registrado");
	          window.location.href = amigable("?module=home");
	          return false;

	        }
	        //update token login
	        var payload = {"id":json[0].id_user};
	        //var datos_social = do_encode(data);
	        var jwt = create_jwt(payload);
	        var data = {"id":json[0].id_user, "token":jwt};
	        var token = JSON.stringify(data);
	        
	        load_data_ajax("?module=login&function=update_token","POST",token,jwt,"update_token_social");

	      },
	      error:function(xhr){
	        alert(xhr.responseText);
	      }

    	});
	}if (type=="POST" && make == "select_user_token") {
		$.ajax({
          url: amigable(url),
          type: type,
          data:{data:value},
          success:function(response){
            alert(response);
            var json = JSON.parse(response);
            if (json.success) {
                if (json.user[0].type==0) {
                    //alert("eres client");
                    //console.log(json);
                    if (json.user[0].passwd=="") {
                    	$("#btn-logout").css("display","block");

                    }
                    
                    $("#btn-signup").css("display","none");
                    $('#profile').append('<a href="' + amigable("?module=profile") + '">Profile</a>');
                    $('li#avatar').append('<img src="'+json.user[0].avatar+'" alt="" height="60" width="60" class="img-circle img-responsive"><p align="center">'+json.user[0].name+'</p>');
                }else if(json.user[0].type==1){
                    if (json.user[0].passwd=="") {
                    	$("#btn-logout").css("display","block");

                    }
                  
                    $("#btn-signup").css("display","none");
                    $('li#avatar').append('<img src="'+json.user[0].avatar+'" alt="" height="60" width="60"><p>'+json.user[0].name+'</p>');
                    $('#profile').append('<a href="' + amigable("?module=profile") + '">Create</a>');
                    
                }else{
                    alert("eres usuari normal");
                    $('div.test_consultant').remove();
                    
                }
            }
               
          },
          error:function(xhr){
            alert(xhr.responseText);
          },

        });
	}if (type=="POST" && make == "select_user_rating") {
		$.ajax({
          url: amigable(url),
          type: type,
          data:{data:value},
          success:function(response){
            //alert(response);
            var json = JSON.parse(response);
            var json_rat = JSON.parse(name_label);
            if (json.success) {
            	var data = { "name":json.user[0].name,"avatar":json.user[0].avatar, "stars":json_rat.stars, "mgs":json_rat.mgs};
                var data_val_JSON = JSON.stringify(data);
                console.log("json debug val_web: " + data_val_JSON);

                //../home/data_val_web/
                load_data_ajax("?module=home&function=data_val_web","POST",data_val_JSON,"","value_web");
                
            }
               
          },
          error:function(xhr){
            alert(xhr.responseText);
          },

        });
	}

	if (type=="POST" && make == "rating_stars_free") {
		$.ajax({
          url: amigable(url),
          type: type,
          data:{data:value},
          success:function(response){
            //alert(response);
            var json = JSON.parse(response);
            var json_rat = JSON.parse(name_label);
            if (json.success) {
            	var data = { "id_valorador":json.user[0].id_user,"stars":json_rat.stars, "dni_valorado":json_rat.dni_valorado};
                var data_val_JSON = JSON.stringify(data);
                console.log("json debug val_web: " + data_val_JSON);

                //../home/data_val_web/
                load_data_ajax("?module=home&function=submitValueFreelancer","POST",data_val_JSON,"","value_web");
                
            }
               
          },
          error:function(xhr){
            alert(xhr.responseText);
          },

        });
	}

	if (type=="POST" && make == "get_data_user") {
		$.ajax({
          url: amigable(url),
          type: type,
          data:{data:value},
          success:function(response){
            alert(response);
            var json = JSON.parse(response);
            if (json.success) {
            	//pintar dades
            	if (!json.value[0].categ_prof) {
            		$("#formClient_update").css('display','block');
            		$("div#avatar").append("<img src='"+json.value[0].avatar+"' class='img-circle img-responsive' height='200' width='200'>");
	            	$("#dni_update").val(json.value[0].dni);
	            	$("#name_update").val(json.value[0].name);
	            	$("#last_name_update").val(json.value[0].last_name);
	            	$("#birthday_update").val(json.value[0].fech_nac);
	            	$("#expirationDni_update").val(json.value[0].expiration_dni);
	            	$("#email_update").val(json.value[0].email);
	            	$("#phone_update").val(json.value[0].phone);
	            	$("#community_update").append("<option id='"+json.value[0].community+"'>"+json.value[0].community+"</option>");
	            	$("#province_update").append("<option id='"+json.value[0].province+"'>"+json.value[0].province+"</option>");
	            	$("#city_update").append("<option id='"+json.value[0].city+"'>"+json.value[0].city+"</option>");
	            	//prova();
	            	json.value.forEach(function(elemento){
		        		var f = elemento.idioma.split(":");
		        		for(var i=0;i<f.length;i++){
		        			//alert(f[i]);
						    if (f[i]=="Ingles") {
						    	elemento1 = $("input:checkbox#Ingles");
								elemento1.prop("checked", true);
						    }if (f[i]=="Español") {
						    	elemento = $("input:checkbox#Español");
								elemento.prop("checked", true);
						    }if (f[i]=="Frances") {
						    	elemento = $("input:checkbox#Frances");
								elemento.prop("checked", true);
						    }if (f[i]=="Italiano") {
						    	elemento = $("input:checkbox#Italiano");
								elemento.prop("checked", true);
						    }
						    
						}
		        		
		        	});
            	}else{
            		//alert("autnomo");
            		
            		$("#formAutonomo_update").css('display','block');
            		$("div#avatar_free").append("<img src='"+json.value[0].avatar+"' class='img-circle img-responsive' height='200' width='200'>");
            		$("#dni_free_update").val(json.value[0].dni);
            		$("#name_free_update").val(json.value[0].name);
            		$("#last_name_free_update").val(json.value[0].last_name);
            		$("#birthday_free_update").val(json.value[0].birthday);
            		$("#date_up_free_update").val(json.value[0].date_up);
            		$("#email_free_update").val(json.value[0].email);
            		$("#phone_free_update").val(json.value[0].phone);
            		$("#price_fre_updatee").val(json.value[0].price_h);
            		$("#community_free_update").append("<option id='"+json.value[0].community+"'>"+json.value[0].community+"</option>");
	            	$("#province_free_update").append("<option id='"+json.value[0].province+"'>"+json.value[0].province+"</option>");
	            	$("#city_free_update").append("<option id='"+json.value[0].city+"'>"+json.value[0].city+"</option>");

	            	json.value.forEach(function(elemento){
		        		var f = elemento.idioma.split(":");
		        		for(var i=0;i<f.length;i++){
		        			alert(f[i]);
						    if (f[i]=="Ingles") {
						    	elemento1 = $("input:checkbox[id=Ingles]");
								elemento1.prop("checked", true);
						    }if (f[i]=="Español") {
						    	elemento = $("input:checkbox[id=Español]");
								elemento.prop("checked", true);
						    }if (f[i]=="Frances") {
						    	elemento = $("input:checkbox[id=Frances]");
								elemento.prop("checked", true);
						    }if (f[i]=="Italiano") {
						    	elemento = $("input:checkbox[id=Italiano]");
								elemento.prop("checked", true);
						    }
						    
						}
		        		
		        	});
		        	

			        ////////// load combos ////////
			        /*var loadCat_prof = function (){
			            //$("#gamma").empty();
			            //$("#cat_prof").append('<select id="categories"></select>');
			            $.each(category, function (i, valor) {
			                $("#cat_prof").append("<option id='"+valor.name_catg+"' value='" + valor.categoryId + "'>" + valor.name_catg + "</option>");
			            });
			        };*/
			        
			       $.getJSON("https://localhost/freelancer/resources/categories.json", function (data){
			            category = data;

			        });
		        	
		        	var aux;
			        $.each(category, function (i, valor) {
		        		if (valor.name_catg==json.value[0].categ_prof) {
		        			aux = "selected";

		        		}else{
		        			aux = "";

		        		}
		                $("#cat_prof_update").append("<option id='"+valor.name_catg+"' value='" + valor.categoryId + "' "+aux+">" + valor.name_catg + "</option>");

		            });
			        var val_catg = $("#cat_prof_update").val();
		            //pillar valor seleccionat en cat_prof_update

		            inputsDependents_update(json.value);


		        	


		        	

            	}
            	
            	
            	
            	
                
            }
               
          },
          error:function(xhr){
            alert(xhr.responseText);
          },

        });
	}
	
}


