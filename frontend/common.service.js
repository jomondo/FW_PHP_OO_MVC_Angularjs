freelancer.factory("CommonService", ['$rootScope','$timeout', function ($rootScope, $timeout) {
        var service = {};
        service.banner = banner;
        service.breadcrumb = breadcrumb;
        service.underlined_menu = underlined_menu;
        return service;

        function banner(message, type) {
            $rootScope.bannerText = message;
            $rootScope.bannerClass = 'alertbanner' + type;
            $rootScope.bannerV = true;

            $timeout(function () {
                $rootScope.bannerV = false;
                $rootScope.bannerText = "";
            }, 8000);
        }

        function breadcrumb(show, module, menu, functions) {
            $rootScope.breadcrumb = show;
            $rootScope.breadcrumb_function = functions;
            $rootScope.breadcrumb_href = "https://localhost/freelancer/#/" + menu;
            $rootScope.breadcrumb_module = module;

        }

        function underlined_menu(active){
            if (active == "home") {
                $rootScope.active_home = "active";

            }else{
                $rootScope.active_home = "";

            }
            
            if (active == "profile") {
                $rootScope.active_profile = "active";

            }else{
                $rootScope.active_profile = "";

            }
            
            if (active == "freelancer") {
                $rootScope.active_freelancer = "active";

            }else{
                $rootScope.active_freelancer = "";

            }
            
            if (active == "contact") {
                $rootScope.active_contact = "active";

            }else{
                $rootScope.active_contact = "";

            }

            if (active == "admin") {
                $rootScope.active_admin = "active";

            }else{
                $rootScope.active_admin = "";

            }

            if (active == "forum") {
                $rootScope.active_forum = "active";

            }else{
                $rootScope.active_forum = "";

            }

            if (active == "top_users") {
                $rootScope.active_top_users = "active";

            }else{
                $rootScope.active_top_users = "";

            }
            

        }
        
    }]);