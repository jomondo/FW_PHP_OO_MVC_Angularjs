Proyecto Freelancer
===================
Proyecto final entorno web 1º DAW 2018 desarrollado por Joan Montés Doria.

Dicho proyecto está enfocado en la búsqueda de personas autónomas (freelancer) alrededor de toda España. Podríamos decir que esta plataforma se ha creado exclusivamente 
para aquellos freelancer que pueden trabajar online y tener el privilegio de trabajar desde cualquier sitio.

En el home podemos ver los 5 freelancer mejor valorados en los rating stars de details por usuarios registrados y muestra las 4 categeorías más visitadas.

Un usuario, ya sea cliente o autónomo, puede encontrar al freelancer que está buscando para proyectos nuevos o colaborativos mediante autocomplete que se situa en el home, el scroll que lista categorias o clicando en el menú llamado freelancer. 

Podemos encontrar a profesionales como: developers web, developers mobile, abogados, diseñadores y 
marketing. Este listado de autónomos que se genera, podemos paginarlos de 3 en 3, ver sus detalles, donde se encuentran situados gracias a la geolocalización de google maps (si clicamos en el marker podremos ver un infoview con algunos detalles), votar al freelancer si es un usuario registrado, y darle like. Para cada una de estas listas generadas hay filtros para poder ayudar al usuario a encontrar su búsqueda.

Para esta plataforma freelancer se ha creado un foro para que los usuarios puedan discutir los temas que tengan dudas. Los usuarios pueden crear diversos items y pueden ser contestados por otros usuarios. Estos comentarios pueden ser votados con un like y solo el administrador puede borrarlos.

Cuando el usuario se registra e inicia sessión, tiene derecho a tener su menú de profile, poder ver sus datos y actualizarlos si quiere. También puede ver los freelancer favoritos, paginarlos y ver las medallas y puntos obtenidos. Estos, son obtenidos por numero de comentarios del foro, items creados y likes del freelancer. El total de los puntos serán los que posicionarán a los usuarios. Lo que se pretende aquí es intentar fidelizar al cliente para que tenga una actividad y uso constante de nuestra web, por eso hay una classificación de nuestros usuarios en la sección "top users".

El administrador tiene un panel de control para controlar el numero de visitas totales, freelancer y clientes. Tiene una gráfica que representa el total de las visitas de cada día de los últimos 7 días. También tiene un control para los usuarios, donde puede ver el estado del usuario, puede bloquear al usuario, desbloquearlo e iliminarlo.

Hay una sección de contacto donde se puede enviar un mensaje de cualquier duda o sugerimientos que pueda tener el usuario.

```
- Mejoras:
	1. breadcrumb ( general app service )
	2. combos dependents ( api geoapi )
	3. enviar contador para obtener las 4 categorías más visitadas en home
	4. JWT ( guardar token en cookies y update token cada vez que un usuario inicia sessión )
	5. underlined menu ( general app service )
	6. filtros en todas las vistas de listar ( filter.services )
	7. rating stars freelancer en details
	8. mostrar 5 freelancer mejor valorados en home
	9. carousel angularui, infinite-scroll, modal angularjs material, datepicker angularjs material
	10. cambiar formato fecha ( $filter )
	11. añadir a mis favoritos en details, ver mis favoritos y paginarlos en profile (contra DAO)
	12. panel de control administrador: ver total visitantes, total freelancer, clientes, ver estado del usuario, bloquear a un usuario, desbloquearlo, eliminarlo y buscarlo. (modulo admin).
	13. gráfica para poder ver las visitas totales de cada día en los ultimos 7 días, en panel control admin ( service ).
	14. funcionalidad de poder dejar de gustar a un freelancer y ver cantidad actual de me gusta que tiene.
	15. foro ( puedes ver, paginar y crear temas, así también como comentarios, donde todos los usuarios registrados pueden responder a sus mismos temas o de otros usuarios )
	16. obtener tipo karma de cada usuario dependiendo el numero de comentarios en forum ( forma dinámica )
	17. fidelización en profile ( total puntos que tiene un usuario, medallas obtenidas por comentarios, items y votaciones freelancer )
	18. solo el admin puede borrar comentarios en el forum
	19. like en comentarios del forum ( contra DAO )
	20. top users ( depende de los puntos del usuario )
	 
```
Tecnologías usadas
==================

- Bases de datos: SQL
- Almacenamiento y manipulación de datos: JSON y XML

- Backend:
	1. PHP

- Frontend:
	1. angularjs
	2. javascript
	2. jquery

- Diseño web:
	1. HTML5
	2. CSS3
	3. Bootstrap
